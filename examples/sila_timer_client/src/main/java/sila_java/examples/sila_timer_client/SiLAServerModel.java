package sila_java.examples.sila_timer_client;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SiLAServerModel {
    private String name;
    private boolean connected;
}
