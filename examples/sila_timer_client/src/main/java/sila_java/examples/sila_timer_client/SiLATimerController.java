package sila_java.examples.sila_timer_client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.examples.sila_timer_service.client.TimerWebClient;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.command.ObservableCommand;

import java.io.IOException;

@Slf4j
@Controller
@RequestMapping(value = "/")
public class SiLATimerController {

    private static final String TOPIC_SERVER_MESSAGE = "/topic/server-info";
    private static final String TOPIC_TIMER_MESSAGE = "/topic/timer-info";

    CloudConnectedServer timerServiceServer;
    TimerWebClient timerServiceWebClient;
    ObservableCommand timerServiceStartCommand;

    private final SimpMessagingTemplate messagingTemplate;

    public SiLATimerController(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void start() throws IOException {

        timerServiceWebClient = new TimerWebClient() {
            @Override
            public void onServerConnect(CloudConnectedServer server) {
                super.onServerConnect(server);
                timerServiceServer = server;
                serverHasConnected(new SiLAServerModel("The timer Server", true));
            }
        };

        timerServiceWebClient.startClient();
    }

    @GetMapping
    public String home(Model model) {
        return "index";
    }

    @PostMapping("/start-timer")
    public @ResponseBody
    void startTimer() {
        timerServiceStartCommand = timerServiceWebClient.createStartCommand(this::onStartTimerConfirmation, this::onTimerIntermediateResponse);
        timerServiceWebClient.executeCommand(timerServiceStartCommand, timerServiceServer.getObservablecommandExecutionService());
    }

    @PostMapping("/stop-timer")
    public @ResponseBody
    void stopTimer() {
        ObservableCommand stopCommand = timerServiceWebClient.createStopCommand(this::onStopTimerConfirmation);
        timerServiceWebClient.executeCommand(stopCommand, timerServiceServer.getObservablecommandExecutionService());
    }

    @PostMapping("/reset-timer")
    public @ResponseBody
    void resetTimer() {
        ObservableCommand resetCommand = timerServiceWebClient.createResetCommand(this::onResetTimerConfirmation);
        timerServiceWebClient.executeCommand(resetCommand, timerServiceServer.getObservablecommandExecutionService());
    }

    public void serverHasConnected(SiLAServerModel server) {
        messagingTemplate.convertAndSend(TOPIC_SERVER_MESSAGE, server);
    }

    private void onStartTimerConfirmation(SiLAFramework.CommandConfirmation confirmation) {
        timerServiceWebClient.subscribeCommandIntermediateResponse(timerServiceStartCommand, timerServiceServer.getObservablecommandExecutionService());
    }

    private void onTimerIntermediateResponse(SilaTimerServiceOuterClass.StartTimer_Responses response) {
        messagingTemplate.convertAndSend(TOPIC_TIMER_MESSAGE, new SiLATimerModel("", Long.toString(response.getTimerValue().getValue()), false));
    }

    private void onStopTimerConfirmation(SilaTimerServiceOuterClass.StopTimer_Responses response) {
        messagingTemplate.convertAndSend(TOPIC_TIMER_MESSAGE, new SiLATimerModel("", Long.toString(response.getTimerValue().getValue()), false));
    }

    private void onResetTimerConfirmation(SilaTimerServiceOuterClass.ResetTimer_Responses response) {
        messagingTemplate.convertAndSend(TOPIC_TIMER_MESSAGE, new SiLATimerModel("", Long.toString(response.getTimerValue().getValue()), true));
    }

    public void stop() {
        timerServiceWebClient.stopClient();
    }
}
