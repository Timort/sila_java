package sila_java.examples.sila_timer_client;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SiLATimerModel {
    private String name;
    private String value;
    boolean reset;
}
