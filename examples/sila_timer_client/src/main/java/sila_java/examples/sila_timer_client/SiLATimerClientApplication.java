package sila_java.examples.sila_timer_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

@SpringBootApplication
public class SiLATimerClientApplication {

    private final SiLATimerController timerController;

    @Inject
    public SiLATimerClientApplication(SiLATimerController webController) {
        this.timerController = webController;
    }

    public static void main(String[] args) {
        SpringApplication.run(SiLATimerClientApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onStart() throws IOException {
        timerController.start();
    }

    @PreDestroy
    public void onStop() {
        timerController.stop();
    }

}
