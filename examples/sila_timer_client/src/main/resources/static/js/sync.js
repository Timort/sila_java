$(document).ready(function () {

    var stompClient = null;
    var url = window.location;

    function connect() {

        var socket = new SockJS('/timer-service-client');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {

            console.log('Connected: ' + frame);

            stompClient.subscribe('/topic/server-info', function (serverInfo) {
                updateServerInfo(JSON.parse(serverInfo.body));
            });

            stompClient.subscribe('/topic/timer-info', function (timerInfo) {
                updateTimerInfo(JSON.parse(timerInfo.body));
            });

        });
    }

    function disconnect() {
        if (stompClient !== null) {
            stompClient.disconnect();
        }

        console.log("Disconnected");
    }

    $(function () {
        connect();
    });

    function updateServerInfo(serverInfo) {
        if (serverInfo.connected) {
            $("#timer-control").addClass("border-success");
            getElement("btnStartTimer").disabled = false;
            getElement("btnStopTimer").disabled = false;
            getElement("btnResetTimer").disabled = false;
        } else {
            $("#timer-control").removeClass("border-success");
            getElement("btnStartTimer").disabled = true;
            getElement("btnStopTimer").disabled = true;
            getElement("btnResetTimer").disabled = true;
        }
    }

    function updateTimerInfo(timerInfo) {
        if (timerInfo.reset) {
            $("#timer-info").removeClass("border-success");
        } else {
            $("#timer-info").addClass("border-success");
        }
        update(getElement("timer"), timerInfo.value)
    }

    function getButton(item) {
        return document.querySelector("[id='" + item + "'] .btnStopTest");
    }

    function update(element, value) {
        if (element) {
            element.textContent = value;
        }
    }

    function getElement(element) {
        return document.querySelector("#" + element);
    }

});