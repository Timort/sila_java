$(document).ready(function () {

    var url = window.location;

    $("#btnStartTimer").click(function (event) {
        execute("/start-timer");
    });

    $("#btnStopTimer").click(function (event) {
        execute("/stop-timer");
    });

    $("#btnResetTimer").click(function (event) {
        execute("/reset-timer");
    });

    function execute(path) {
        $.post(url + path,
               {},
               function (data, status) {
               });
    }

});