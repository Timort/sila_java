# SiLA Timer Service Web Client
The SiLA Timer Service Web Client demonstrates the usage of the SiLA Cloud Connection Features.
This Client can run anywhere on the internet where it is accessible over HTTP protocol.
A SiLA Timer Service server connects to a SiLA Timer Service Web Client and serve its features to the public internet.

To run the client execute:
```
mvn spring-boot:run
```
open a browser window with the address: http://localhost:8080  
  
### Timer Service Features
- Start Timer
- Stop Timer
- Reset Timer

### Implementation Details
This Web Client is implemented as a Spring Boot Application using the Spring following features:

* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-developing-web-applications)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-spring-mvc-template-engines)
* [WebSocket](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-websockets)

