package sila_java.examples.hello_sila_cloud.client;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.SayHello_Parameters;
import sila_java.library.cloud.client.command.UnobservableCommandImpl;

import java.util.function.Consumer;

import static sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.SayHello_Responses;
import static sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.SayHello_Responses.parser;

@Slf4j
@Getter
public class UnobservableGreetingCommand extends UnobservableCommandImpl<SayHello_Parameters, SayHello_Responses> {

    private static final String COMMAND_ID = "org.silastandard/examples/GreetingProvider/v1/Command/SayHello";

    @Builder
    public UnobservableGreetingCommand(SayHello_Parameters parameters, Consumer<SayHello_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(COMMAND_ID, parameters, resultCallback, errorCallback, parser());
    }

    @Override
    public void onResponse(SayHello_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.info("Error received: {}", error);
    }

}
