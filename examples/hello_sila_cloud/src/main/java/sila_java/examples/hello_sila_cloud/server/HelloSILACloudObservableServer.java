package sila_java.examples.hello_sila_cloud.server;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.server.SILACloudServer;
import sila_java.library.server_base.utils.ArgumentHelper;

import static sila_java.library.cloud.client.SILACloudClient.SILA_CLOUD_DEFAULT_PORT;
import static sila_java.library.core.utils.Utils.blockUntilStop;

@Slf4j
public class HelloSILACloudObservableServer implements AutoCloseable {

    public static final String SERVER_TYPE = "Hello SiLA Cloud Server";
    SILACloudServer cloudServer;

    /**
     * Application Class using command line arguments
     *
     * @param argumentHelper Custom Argument Helper
     */
    public HelloSILACloudObservableServer(@NonNull final ArgumentHelper argumentHelper) {

        log.info("Launching cloud server");
        cloudServer = new SILACloudServer.Builder()
                .withHost("localhost")
                .withPort(argumentHelper.getPort().orElse(SILA_CLOUD_DEFAULT_PORT))
                .withCommandHandler(
                        "org.silastandard/examples/ObservableGreetingProvider/v1/Command/SayHello",
                        new SayHelloCloudObservableCommand())
                .withPropertyHandler(
                        "org.silastandard/examples/ObservableGreetingProvider/v1/Property/StartYear",
                        new YearCloudObservableProperty())
                .withPropertyHandler(
                        "org.silastandard/examples/ObservableGreetingProvider/v1/Property/Timestamp",
                        new TimestampCloudObservableProperty())
                .build();

        cloudServer.start();
        log.info("Server started streaming to cloud");

    }

    @Override
    public void close() {
        this.cloudServer.stop();
    }

    /**
     * Simple main function that starts the server and keeps it alive
     */
    public static void main(final String[] args) throws InterruptedException {

        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);

        try (final HelloSILACloudObservableServer cloudServer = new HelloSILACloudObservableServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(cloudServer::close));
            blockUntilStop();
        }

    }

}
