package sila_java.examples.hello_sila_cloud.client;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Parameters;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.command.ObservableCommandExecutionService;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Slf4j
public class HelloSILACloudObservableClient {

    /**
     * start client providing rRCP cloud service endpoint
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        long stayAliveSeconds = 120;
        log.info("Launching cloud client");

        HelloSILACloudObservableClient helloClient = new HelloSILACloudObservableClient();

        SILACloudClient cloudClient = new SILACloudClient.Builder()
                .withServerConnectedListener(helloClient::serverHasConnected)
                .start();

        log.info("client has been started. Keeping alive for {} seconds", stayAliveSeconds);
        log.info("# No connectedServers on client start: {}", cloudClient.getConnectedServers().size());
        Thread.sleep(stayAliveSeconds * 1000);
        log.info("# No connectedServers before client ends: {}", cloudClient.getConnectedServers().size());
        log.info("{} seconds past. finish", stayAliveSeconds);

        cloudClient.shutdown();
        log.info("Cloud client has been shut down");
    }


    void serverHasConnected(CloudConnectedServer server) {
        log.info("Server has connected. Client can send commands to it now");
        Stream.of(
                "Anton",
                "Diego",
                "Emma",
                "Marlene"
        )
                .forEach(message -> executeGreetCommand(server.getObservablecommandExecutionService(), message));

        TimestampObservableProperty property = TimestampObservableProperty.builder()
                .build();
        server.getObservablePropertyService().subscribeProperty(property);
        server.getObservablePropertyService().cancelSubscription("1");
    }

    void executeGreetCommand(ObservableCommandExecutionService commandExecutionService, String helloMessage) {
        ObservableCommand greetCommand = createGreetCommand(helloMessage, response -> log.info("Got greetingResponse: {}", response));
        commandExecutionService.executeCommand(greetCommand);
    }

    ObservableCommand createGreetCommand(String name, Consumer<ObservableGreetingProviderOuterClass.SayHello_Responses> callback) {

        SayHello_Parameters parameter = SayHello_Parameters
                .newBuilder()
                .setName(SiLAString.from(name))
                .build();

        return ObservableGreetingCommand
                .builder()
                .parameters(parameter)
                .resultCallback(callback)
                .build();
    }

}
