package sila_java.examples.hello_sila_cloud.server;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.Get_StartYear_Parameters;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.Get_StartYear_Responses;
import sila_java.examples.hello_sila.SayHelloCommand;
import sila_java.library.cloud.server.property.UnobservedPropertyHandlerImpl;

@Slf4j
public class YearCloudProperty extends UnobservedPropertyHandlerImpl<Get_StartYear_Parameters, Get_StartYear_Responses> {

    private final GreetingProviderGrpc.GreetingProviderImplBase commandDelegate;

    public YearCloudProperty() {
        super(Get_StartYear_Parameters.parser());
        this.commandDelegate = new SayHelloCommand();
    }

    @Override
    public Get_StartYear_Responses handle(Get_StartYear_Parameters requestParameters) {
        CommandWrapperStreamObserver resultObserver = new CommandWrapperStreamObserver();
        commandDelegate.getStartYear(requestParameters, resultObserver);

        return resultObserver.getResult();

    }

}
