package sila_java.examples.hello_sila_cloud.client;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Responses;
import sila_java.library.cloud.client.property.ObservablePropertyImpl;

import java.util.function.Consumer;

import static sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Parameters;

@Slf4j
public class TimestampObservableProperty extends ObservablePropertyImpl<Subscribe_Timestamp_Parameters, Subscribe_Timestamp_Responses> {

    private static final String PROPERTY_ID = "org.silastandard/examples/ObservableGreetingProvider/v1/Property/Timestamp";

    @Builder
    public TimestampObservableProperty(Subscribe_Timestamp_Parameters parameters, Consumer<Subscribe_Timestamp_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, errorCallback, Subscribe_Timestamp_Responses.parser());
    }

    @Override
    public void onResponse(Subscribe_Timestamp_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.error("Error received: {}", error);
    }
}
