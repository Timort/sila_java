package sila_java.examples.hello_sila_cloud.server;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class GreetingSimulation {
    public static final double UPDATE_INTERVAL = 1;
    private final AtomicInteger currentGreetingNumber = new AtomicInteger(0);
    private final List<GreetingListener> greetingListenerList = new CopyOnWriteArrayList<>();
    private final Thread agentThread;

    public GreetingSimulation() {
        agentThread = new Thread(
                new GreetingAgent(),
                GreetingSimulation.class.getName() + "_" + GreetingAgent.class.getName()
        );
        //terminate the thread with the VM.
        agentThread.setDaemon(true);
        agentThread.start();
    }

    public interface GreetingListener {
        void onChange(int number);
    }

    private class GreetingAgent implements Runnable {
        @Override
        public void run() {
            while (!Thread.interrupted()) {
                final long startTime = System.currentTimeMillis();
                final int number = currentGreetingNumber.get();

                currentGreetingNumber.getAndAdd(1);

                GreetingSimulation.this.greetingListenerList.forEach(l -> l.onChange(number));

                // Best effort to keep the update interval
                final long remainingTime = TimeUnit.SECONDS.toMillis((long) UPDATE_INTERVAL) -
                        (System.currentTimeMillis() - startTime);
                try {
                    if (remainingTime > 0)
                        Thread.sleep(remainingTime);
                } catch (final InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            log.info("End of greeting agent");
        }
    }

    public void addListener(final @NonNull GreetingSimulation.GreetingListener listener) {
        greetingListenerList.add(listener);
    }

    public void removeListener(final @NonNull GreetingSimulation.GreetingListener listener) {
        greetingListenerList.remove(listener);
    }

    public void setCurrentGreetingNumber(final int currentGreetingNumber) {
        this.currentGreetingNumber.set(currentGreetingNumber);
    }

    public int getCurrentGreetingNumber() {
        return this.currentGreetingNumber.get();
    }
}
