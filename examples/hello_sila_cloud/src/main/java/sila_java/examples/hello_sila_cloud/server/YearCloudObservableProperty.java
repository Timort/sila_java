package sila_java.examples.hello_sila_cloud.server;

import com.google.protobuf.ByteString;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.Subscribe_StartYear_Parameters;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.Subscribe_StartYear_Responses;
import sila_java.library.cloud.server.property.ObservedPropertyHandlerImpl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

@Slf4j
public class YearCloudObservableProperty extends ObservedPropertyHandlerImpl<Subscribe_StartYear_Parameters, Subscribe_StartYear_Responses> {

    private final AtomicBoolean running = new AtomicBoolean(false);

    public YearCloudObservableProperty() {
        super(Subscribe_StartYear_Parameters.parser());
    }

    @Override
    public void handleSubscription(Subscribe_StartYear_Parameters parameters, Consumer<ByteString> consumer) {
        running.set(true);

        AtomicLong year = new AtomicLong(2010);
        for (int i = 0; i < 11; i++) {
            if (!running.get()) {
                break;
            }
            Subscribe_StartYear_Responses yearResponse = Subscribe_StartYear_Responses.newBuilder()
                    .setStartYear(SiLAFramework.Integer.newBuilder().setValue(year.incrementAndGet()))
                    .build();
            consumer.accept(yearResponse.toByteString());

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }

    }

    @Override
    public void handleCancelSubscription(String requestUUID) {
        log.warn("handleCancelSubscription {}", requestUUID);
        running.set(false);
    }

}
