package sila_java.examples.hello_sila_cloud.client;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.Subscribe_StartYear_Parameters;
import sila_java.library.cloud.client.property.ObservablePropertyImpl;

import java.util.function.Consumer;

import static sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.Subscribe_StartYear_Responses;

@Slf4j
public class YearObservableProperty extends ObservablePropertyImpl<Subscribe_StartYear_Parameters, Subscribe_StartYear_Responses> {

    private static final String PROPERTY_ID = "org.silastandard/examples/ObservableGreetingProvider/v1/Property/StartYear";

    @Builder
    public YearObservableProperty(Subscribe_StartYear_Parameters parameters, Consumer<Subscribe_StartYear_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, errorCallback, Subscribe_StartYear_Responses.parser());
    }

    @Override
    public void onResponse(Subscribe_StartYear_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.error("Error received: {}", error);
    }
}
