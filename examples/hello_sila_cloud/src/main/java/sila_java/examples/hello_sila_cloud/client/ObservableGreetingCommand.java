package sila_java.examples.hello_sila_cloud.client;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Parameters;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Responses;
import sila_java.library.cloud.client.command.ObservableCommandImpl;

import java.util.function.Consumer;

@Slf4j
@Getter
public class ObservableGreetingCommand extends ObservableCommandImpl<SayHello_Parameters, SayHello_Responses> {

    private static final String COMMAND_ID = "org.silastandard/examples/ObservableGreetingProvider/v1/Command/SayHello";

    @Builder
    public ObservableGreetingCommand(SayHello_Parameters parameters,
                                     Consumer<SiLAFramework.CommandConfirmation> confirmationCallback,
                                     Consumer<SiLAFramework.ExecutionInfo> executionInfoCallback,
                                     Consumer<SayHello_Responses> intermediateResultCallback,
                                     Consumer<SayHello_Responses> resultCallback,
                                     Consumer<SiLAFramework.SiLAError> errorCallback) {

        super(COMMAND_ID, parameters, confirmationCallback, executionInfoCallback, intermediateResultCallback, resultCallback, errorCallback, SayHello_Responses.parser());

    }

    @Override
    public void onCommandConfirmation(SiLAFramework.CommandConfirmation confirmation) {
        log.info("Confirmation received: {}", confirmation);
    }

    @Override
    public void onCommandExecutionInfo(SiLAFramework.ExecutionInfo executionInfo) {
        log.info("Execution info received: {}", executionInfo);
    }

    @Override
    public void onIntermediateResponse(SayHello_Responses responses) {
        log.info("Intermediate response received: {}", responses);
    }

    @Override
    public void onResponse(SayHello_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.info("error received: {}", error);
    }

}
