package sila_java.examples.hello_sila_cloud.client;

import java.io.IOException;
import java.util.function.Consumer;
import java.util.stream.Stream;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.command.CommandExecutionService;
import sila_java.library.cloud.client.command.UnobservableCommand;
import sila_java.library.core.sila.types.SiLAString;

@Slf4j
public class HelloSILACloudClient {

    /**
     * start client providing rRCP cloud service endpoint
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        long stayAliveSeconds = 120;
        log.info("Launching cloud client");

        HelloSILACloudClient helloClient = new HelloSILACloudClient();

        SILACloudClient cloudClient = new SILACloudClient.Builder()
                .withServerConnectedListener(helloClient::serverHasConnected)
                .start();

        log.info("client has been started. Keeping alive for {} seconds", stayAliveSeconds);
        log.info("# No connectedServers on client start: {}", cloudClient.getConnectedServers().size());
        Thread.sleep(stayAliveSeconds * 1000);
        log.info("# No connectedServers before client ends: {}", cloudClient.getConnectedServers().size());
        log.info("{} seconds past. finish", stayAliveSeconds);

        cloudClient.shutdown();
        log.info("Cloud client has been shut down");
    }


    void serverHasConnected(CloudConnectedServer server) {
        log.info("Server has connected. Client can send commands to it now");
        Stream.of(
                "Anton",
                "Diego",
                "Emma",
                "Marlene"
        )
                .forEach(message -> executeGreetCommand(server.getCommandExecutionService(), message));
    }

    void executeGreetCommand(CommandExecutionService commandExecutionService, String helloMessage) {
        UnobservableCommand greetCommand = createGreetCommand(helloMessage, response -> log.info("Got greetingResponse: {}", response));
        commandExecutionService.executeCommand(greetCommand);
    }

    UnobservableCommand createGreetCommand(String name, Consumer<GreetingProviderOuterClass.SayHello_Responses> callback) {

        GreetingProviderOuterClass.SayHello_Parameters parameter = GreetingProviderOuterClass.SayHello_Parameters
                .newBuilder()
                .setName(SiLAString.from(name))
                .build();

        return UnobservableGreetingCommand
                .builder()
                .parameters(parameter)
                .resultCallback(callback)
                .build();

    }

}
