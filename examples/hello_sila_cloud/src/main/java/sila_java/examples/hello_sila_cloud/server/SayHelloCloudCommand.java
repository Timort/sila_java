package sila_java.examples.hello_sila_cloud.server;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.SayHello_Parameters;
import sila_java.examples.hello_sila.SayHelloCommand;
import sila_java.library.cloud.server.command.UnobservedCommandHandlerImpl;
import sila_java.library.core.sila.errors.SiLAErrors;

import static sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass.SayHello_Responses;

@Slf4j
public class SayHelloCloudCommand extends UnobservedCommandHandlerImpl<SayHello_Parameters, SayHello_Responses> {

    private final GreetingProviderGrpc.GreetingProviderImplBase commandDelegate;

    public SayHelloCloudCommand() {
        super(SayHello_Parameters.parser());
        this.commandDelegate = new SayHelloCommand();
    }

    @Override
    public SayHello_Responses handle(SayHello_Parameters requestParameters) {

        if (!requestParameters.hasName()) {
            throw SiLAErrors.generateValidationError("name", "Name required");
        }

        if (StringUtils.isAllUpperCase(requestParameters.getName().getValue())) {
            throw SiLAErrors.generateDefinedExecutionError("NameNotGreetable", "Do not shout, " + requestParameters.getName().getValue());
        }

        CommandWrapperStreamObserver resultObserver = new CommandWrapperStreamObserver();
        commandDelegate.sayHello(requestParameters, resultObserver);

        return resultObserver.getResult();

    }

}
