package sila_java.examples.hello_sila_cloud.server;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Parameters;
import sila_java.library.cloud.server.command.ObservedCommandHandlerImpl;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import static sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Responses;

@Slf4j
public class SayHelloCloudObservableCommand extends ObservedCommandHandlerImpl<SayHello_Parameters, SayHello_Responses> {

    private final GreetingSimulation greetingSimulation = new GreetingSimulation();

    private final ObservableCommandManager<SayHello_Parameters, SayHello_Responses> commandManager = new ObservableCommandManager<>(new ObservableCommandTaskRunner(1, 1),
            this::runCommandTask);

    private SayHello_Responses runCommandTask(ObservableCommandWrapper<SayHello_Parameters, SayHello_Responses> command) {
        SiLAFramework.String name = command.getParameter().getName();

        final CompletableFuture<String> future = new CompletableFuture<>();

        final GreetingSimulation.GreetingListener listener = times -> {

            log.info("times : {}", times);
            if (times == 5) {
                future.complete("Hello " + name.getValue());
            }
        };
        greetingSimulation.addListener(listener);
        try {
            String greeting = future.get();
            return ObservableGreetingProviderOuterClass.SayHello_Responses.newBuilder()
                    .setGreeting(SiLAString.from(greeting))
                    .build();
        } catch (InterruptedException | ExecutionException e) {
            throw SiLAErrors.generateGenericExecutionError(e);
        } finally {
            greetingSimulation.removeListener(listener);
        }
    }

    public SayHelloCloudObservableCommand() {
        super(SayHello_Parameters.parser());
    }

    @SneakyThrows
    @Override
    public SiLAFramework.CommandConfirmation handle(SayHello_Parameters requestParameters) {

        if (!requestParameters.hasName()) {
            throw SiLAErrors.generateValidationError("name", "Name required");
        }

        if (StringUtils.isAllUpperCase(requestParameters.getName().getValue())) {
            log.error("Too loud! {}", requestParameters.getName().getValue());
            throw SiLAErrors.generateDefinedExecutionError("NameNotGreetable", "Do not shout, " + requestParameters.getName().getValue());
        }

        CompletableFuture<SiLAFramework.CommandConfirmation> future = new CompletableFuture<>();
        SayHelloStreamObserver<SiLAFramework.CommandConfirmation> streamObserver = new SayHelloStreamObserver<>(future::complete);
        commandManager.addCommand(requestParameters, streamObserver);
        return future.get();
    }

    @Override
    public void handleInfo(SiLAFramework.CommandExecutionUUID commandExecutionUUID, Consumer<SiLAFramework.ExecutionInfo> infoConsumer) {
        SayHelloStreamObserver<SiLAFramework.ExecutionInfo> streamObserver = new SayHelloStreamObserver<>(infoConsumer);
        ObservableCommandWrapper<SayHello_Parameters, SayHello_Responses> commandWrapper = this.commandManager.get(commandExecutionUUID);
        commandWrapper.addStateObserver(streamObserver);
    }

    @Override
    public void handleIntermediateResult(SiLAFramework.CommandExecutionUUID commandExecutionUUID, Consumer<ByteString> consumer) {
        SayHelloStreamObserver<ByteString> streamObserver = new SayHelloStreamObserver<>(consumer);
        ObservableCommandWrapper<SayHello_Parameters, SayHello_Responses> commandWrapper = this.commandManager.get(commandExecutionUUID);
        commandWrapper.addIntermediateResponseObserver(streamObserver);
    }

    @SneakyThrows
    @Override
    public SayHello_Responses handleResult(SiLAFramework.CommandExecutionUUID parameters) {
        CompletableFuture<SayHello_Responses> future = new CompletableFuture<>();
        SayHelloStreamObserver<SayHello_Responses> streamObserver = new SayHelloStreamObserver<>(future::complete);
        ObservableCommandWrapper<SayHello_Parameters, SayHello_Responses> commandWrapper = commandManager.get(parameters);
        commandWrapper.sendResult(streamObserver);
        return future.get();
    }

    @Override
    public void handleExecutionInfoCancel() {
        // TODO
    }

    @Override
    public void handleIntermediateResponseCancel() {
        // TODO
    }

    class SayHelloStreamObserver<T> implements StreamObserver<T> {

        private final Consumer<T> consumer;

        public SayHelloStreamObserver(Consumer<T> consumer) {
            this.consumer = consumer;
        }

        @Override
        public void onNext(T sayHelloResponses) {
            log.info("SayHelloStreamObserver, onNext {}", sayHelloResponses);
            consumer.accept(sayHelloResponses);
        }

        @Override
        public void onError(Throwable throwable) {
            log.info("SayHelloStreamObserver, onError", throwable);
            consumer.accept(null);
        }

        @Override
        public void onCompleted() {
            log.info("SayHelloStreamObserver, onCompleted");
            consumer.accept(null);
        }
    }

}
