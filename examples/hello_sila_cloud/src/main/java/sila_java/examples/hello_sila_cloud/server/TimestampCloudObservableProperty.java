package sila_java.examples.hello_sila_cloud.server;

import com.google.protobuf.ByteString;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila_java.library.cloud.server.property.ObservedPropertyHandlerImpl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

@Slf4j
public class TimestampCloudObservableProperty extends ObservedPropertyHandlerImpl<ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Parameters, ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Responses> {

    private final AtomicBoolean running;

    public TimestampCloudObservableProperty() {
        super(ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Parameters.parser());
        running = new AtomicBoolean(false);
    }

    @Override
    public void handleSubscription(ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Parameters parameters, Consumer<ByteString> consumer) {
        running.set(true);
        new Thread(new TimestampRunnable(consumer)).start();
    }

    @Override
    public void handleCancelSubscription(String requestUUID) {
        log.info("handleCancelSubscription {}", requestUUID);
        running.set(false);
    }

    @AllArgsConstructor
    private class TimestampRunnable implements Runnable {
        Consumer<ByteString> intermediateConsumer;

        @Override
        public void run() {
            while (running.get()) {
                SiLAFramework.Timestamp timestamp = SiLAFramework.Timestamp.newBuilder()
                        .setYear(2020)
                        .build();
                ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Responses response = ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Responses.newBuilder()
                        .setTimestamp(timestamp).build();
                intermediateConsumer.accept(response.toByteString());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
        }
    }

}
