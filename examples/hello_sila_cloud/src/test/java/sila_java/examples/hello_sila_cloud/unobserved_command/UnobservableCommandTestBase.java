package sila_java.examples.hello_sila_cloud.unobserved_command;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import sila_java.examples.hello_sila_cloud.server.HelloSILACloudServer;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.command.CommandExecutionService;
import sila_java.library.cloud.client.property.UnobservablePropertyService;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.cloud.client.SILACloudClient.SILA_CLOUD_DEFAULT_PORT;

@Slf4j
public abstract class UnobservableCommandTestBase {

    public HelloSILACloudServer cloudServer;
    public SILACloudClient cloudClient;
    public CommandExecutionService clientCommandService;
    public UnobservablePropertyService unobservablePropertyService;

    boolean cloudClientReady;

    @BeforeEach
    void setup() throws IOException {

        log.info("Starting HelloSiLACloudServer...");
        final String[] args = {"-p", "" + SILA_CLOUD_DEFAULT_PORT};

        this.cloudClient = new SILACloudClient
                .Builder()
                .withPort(SILA_CLOUD_DEFAULT_PORT)
                .withServerConnectedListener(this::serverHasConnected)
                .start();

        while (!cloudClientReady) {
            log.info("client not ready!");
            this.cloudServer = new HelloSILACloudServer(new ArgumentHelper(args, HelloSILACloudServer.SERVER_TYPE));
        }
    }

    @AfterEach
    void cleanup() {

        if (cloudServer != null) {
            cloudServer.close();
            cloudServer = null;
        }

        if (cloudClient != null) {
            cloudClient.shutdown();
        }

    }

    private void serverHasConnected(CloudConnectedServer connectedServer) {

        if (null != clientCommandService) {
            return;
        }

        log.info("server has connected!");
        cloudClientReady = true;
        clientCommandService = connectedServer.getCommandExecutionService();
        unobservablePropertyService = connectedServer.getUnobservablePropertyService();

    }

}
