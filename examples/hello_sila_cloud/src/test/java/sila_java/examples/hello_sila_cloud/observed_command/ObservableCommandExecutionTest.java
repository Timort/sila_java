package sila_java.examples.hello_sila_cloud.observed_command;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.plexus.util.StringUtils;
import org.junit.jupiter.api.Test;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila_java.examples.hello_sila_cloud.client.ObservableGreetingCommand;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class ObservableCommandExecutionTest extends ObservableCommandTestBase {


    @Test
    void testHelloInfo() throws InterruptedException, TimeoutException, ExecutionException {

        String name = "test";

        CompletableFuture<ObservableCommand> confirmation = submitCommand(name);


        CompletableFuture<SiLAFramework.ExecutionInfo> infoFuture = submitCommand(confirmation.get());

        SiLAFramework.ExecutionInfo actual = infoFuture.get(5, TimeUnit.SECONDS);
        log.info("got execution info {}", actual);
        assertNotNull(actual.toString());
    }

    private CompletableFuture<ObservableCommand> submitCommand(String name) {
        CompletableFuture<ObservableCommand> commandFuture = new CompletableFuture<>();
        CompletableFuture<SiLAFramework.CommandExecutionUUID> commandExecutionUUIDFuture = new CompletableFuture<>();
        ObservableCommand command = createGreetCommand(name, response -> commandExecutionUUIDFuture.complete(response.getCommandExecutionUUID()));
        commandExecutionUUIDFuture.thenApply(com -> commandFuture.complete(command));
        clientCommandService.executeCommand(command);
        return commandFuture;
    }

    private CompletableFuture<SiLAFramework.ExecutionInfo> submitCommand(ObservableCommand command) {
        CompletableFuture<SiLAFramework.ExecutionInfo> infoFuture = new CompletableFuture<>();
        ObservableCommand greetCommand = createGreetCommandInfo(command.getCommandExecutionUUID(), (response) -> {
            log.info("Got greetingInfoResponse: {}", response);
            infoFuture.complete(response);
        });

        clientCommandService.subscribeCommandInfo(greetCommand);
        return infoFuture;
    }

    ObservableCommand createGreetCommand(String name, Consumer<SiLAFramework.CommandConfirmation> callback) {

        ObservableGreetingProviderOuterClass.SayHello_Parameters.Builder parameter = ObservableGreetingProviderOuterClass.SayHello_Parameters
                .newBuilder();

        if (StringUtils.isNotBlank(name)) {
            parameter.setName(SiLAString.from(name));
        }

        return ObservableGreetingCommand
                .builder()
                .parameters(parameter.build())
                .confirmationCallback(callback)
                .build();
    }

    private ObservableCommand createGreetCommandInfo(SiLAFramework.CommandExecutionUUID executionUuid, Consumer<SiLAFramework.ExecutionInfo> callback) {
        ObservableGreetingCommand greetingCommand = ObservableGreetingCommand
                .builder()
                .executionInfoCallback(callback)
                .build();
        greetingCommand.handleCommandConfirmation(SiLAFramework.CommandConfirmation.newBuilder().setCommandExecutionUUID(executionUuid).build());
        return greetingCommand;
    }

}
