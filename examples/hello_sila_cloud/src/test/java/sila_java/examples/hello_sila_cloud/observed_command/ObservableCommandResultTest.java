package sila_java.examples.hello_sila_cloud.observed_command;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.plexus.util.StringUtils;
import org.junit.jupiter.api.Test;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Responses;
import sila_java.examples.hello_sila_cloud.client.ObservableGreetingCommand;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class ObservableCommandResultTest extends ObservableCommandTestBase {

    @Test
    void testHelloResult() throws InterruptedException, TimeoutException, ExecutionException {

        String name = "test";

        CompletableFuture<SiLAFramework.CommandExecutionUUID> future = submitCommand(name);
        SiLAFramework.CommandExecutionUUID executionUUID = future.get(5, TimeUnit.SECONDS);

        assertNotNull(executionUUID);
        assertNotNull(executionUUID.getValue());

        SiLAFramework.CommandConfirmation commandConfirmation = SiLAFramework.CommandConfirmation.newBuilder()
                .setCommandExecutionUUID(executionUUID)
                .build();

        CompletableFuture<SiLAFramework.ExecutionInfo> commandInfoFuture = submitCommand(commandConfirmation);
        SiLAFramework.ExecutionInfo executionInfo = commandInfoFuture.get(15, TimeUnit.SECONDS);

        log.info("execution info: {}", executionInfo);
        assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully, executionInfo.getCommandStatus());

        CompletableFuture<SayHello_Responses> resultFuture = submitCommandResult(commandConfirmation);
        String greeting = resultFuture.get(5, TimeUnit.SECONDS).getGreeting().getValue();

        assertEquals("Hello " + name, greeting);

    }

    private CompletableFuture<SiLAFramework.CommandExecutionUUID> submitCommand(String name) {
        CompletableFuture<SiLAFramework.CommandExecutionUUID> commandExecutionUUIDFuture = new CompletableFuture<>();
        ObservableCommand command = createGreetCommand(name, response -> commandExecutionUUIDFuture.complete(response.getCommandExecutionUUID()));
        clientCommandService.executeCommand(command);
        return commandExecutionUUIDFuture;
    }

    private CompletableFuture<SiLAFramework.ExecutionInfo> submitCommand(SiLAFramework.CommandConfirmation commandConfirmation) {
        CompletableFuture<SiLAFramework.ExecutionInfo> infoFuture = new CompletableFuture<>();
        ObservableCommand greetCommand = createGreetCommandInfo(commandConfirmation, (response) -> {
            log.info("Got greetingInfoResponse: {}", response);

            if (response.getCommandStatus() == SiLAFramework.ExecutionInfo.CommandStatus.running) {
                log.info("still running {}", response.getCommandStatus());
            } else {
                log.info("done {}", response.getCommandStatus());
                infoFuture.complete(response);
            }
        });

        clientCommandService.subscribeCommandInfo(greetCommand);
        return infoFuture;
    }

    private CompletableFuture<SayHello_Responses> submitCommandResult(SiLAFramework.CommandConfirmation commandConfirmation) {

        CompletableFuture<SayHello_Responses> infoFuture = new CompletableFuture<>();
        ObservableCommand greetCommand = createGreetCommandResult(commandConfirmation, response -> {
            log.info("Got greetingInfoResponse: {}", response);
            infoFuture.complete(response);
        });

        clientCommandService.executeCommandResult(greetCommand);
        return infoFuture;
    }

    ObservableCommand createGreetCommand(String name, Consumer<SiLAFramework.CommandConfirmation> callback) {
        ObservableGreetingProviderOuterClass.SayHello_Parameters.Builder parameter = ObservableGreetingProviderOuterClass.SayHello_Parameters
                .newBuilder();

        if (StringUtils.isNotBlank(name)) {
            parameter.setName(SiLAString.from(name));
        }

        return ObservableGreetingCommand
                .builder()
                .parameters(parameter.build())
                .confirmationCallback(callback)
                .build();
    }

    private ObservableCommand createGreetCommandInfo(SiLAFramework.CommandConfirmation confirmation, Consumer<SiLAFramework.ExecutionInfo> callback) {
        ObservableGreetingCommand command = ObservableGreetingCommand
                .builder()
                .executionInfoCallback(callback)
                .build();
        command.handleCommandConfirmation(confirmation);
        return command;
    }

    private ObservableCommand createGreetCommandResult(SiLAFramework.CommandConfirmation confirmation, Consumer<SayHello_Responses> callback) {
        ObservableGreetingCommand command = ObservableGreetingCommand
                .builder()
                .resultCallback(callback)
                .build();
        command.handleCommandConfirmation(confirmation);
        return command;
    }

}
