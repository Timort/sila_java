package sila_java.examples.hello_sila_cloud.observed_command;

import org.codehaus.plexus.util.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila_java.examples.hello_sila_cloud.client.ObservableGreetingCommand;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class ObservableCommandInitiationTest extends ObservableCommandTestBase {

    @Test
    void testHelloConfirmation() throws InterruptedException, TimeoutException, ExecutionException {

        String name = "test";

        CompletableFuture<SiLAFramework.CommandExecutionUUID> future = submitCommand(name);
        SiLAFramework.CommandExecutionUUID executionUUID = future.get(5, TimeUnit.SECONDS);

        assertNotNull(executionUUID);
        assertNotNull(executionUUID.getValue());

    }

    private CompletableFuture<SiLAFramework.CommandExecutionUUID> submitCommand(String name) {
        CompletableFuture<SiLAFramework.CommandExecutionUUID> commandExecutionUUIDFuture = new CompletableFuture<>();
        ObservableCommand command = createGreetCommand(name, response -> commandExecutionUUIDFuture.complete(response.getCommandExecutionUUID()));
        clientCommandService.executeCommand(command);
        return commandExecutionUUIDFuture;
    }

    ObservableCommand createGreetCommand(String name, Consumer<SiLAFramework.CommandConfirmation> callback) {

        ObservableGreetingProviderOuterClass.SayHello_Parameters.Builder parameter = ObservableGreetingProviderOuterClass.SayHello_Parameters
                .newBuilder();

        if (StringUtils.isNotBlank(name)) {
            parameter.setName(SiLAString.from(name));
        }

        return ObservableGreetingCommand
                .builder()
                .parameters(parameter.build())
                .confirmationCallback(callback)
                .build();
    }

}
