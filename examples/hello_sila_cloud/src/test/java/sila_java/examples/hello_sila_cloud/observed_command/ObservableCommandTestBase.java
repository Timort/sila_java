package sila_java.examples.hello_sila_cloud.observed_command;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import sila_java.examples.hello_sila_cloud.server.HelloSILACloudObservableServer;
import sila_java.examples.hello_sila_cloud.server.HelloSILACloudServer;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.command.ObservableCommandExecutionService;
import sila_java.library.cloud.client.property.ObservablePropertyService;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.cloud.client.SILACloudClient.SILA_CLOUD_DEFAULT_PORT;

@Slf4j
public abstract class ObservableCommandTestBase {

    public HelloSILACloudObservableServer cloudServer;
    public SILACloudClient cloudClient;
    public ObservableCommandExecutionService clientCommandService;
    public ObservablePropertyService observablePropertyService;

    boolean cloudClientReady;

    @BeforeEach
    void setup() throws IOException {

        log.info("Starting HelloSiLACloudServer...");
        final String[] args = {"-p", "" + SILA_CLOUD_DEFAULT_PORT};

        this.cloudClient = new SILACloudClient
                .Builder()
                .withPort(SILA_CLOUD_DEFAULT_PORT)
                .withServerConnectedListener(this::serverHasConnected)
                .start();

        while (!cloudClientReady) {
            log.info("client not ready!");
            this.cloudServer = new HelloSILACloudObservableServer(new ArgumentHelper(args, HelloSILACloudServer.SERVER_TYPE));
        }
    }

    @AfterEach
    void cleanup() {

        if (cloudServer != null) {
            cloudServer.close();
            cloudServer = null;
        }

        if (cloudClient != null) {
            cloudClient.shutdown();
        }

        clientCommandService = null;

    }

    private void serverHasConnected(CloudConnectedServer connectedServer) {

        if (null != clientCommandService) {
            return;
        }

        log.info("server has connected!");
        cloudClientReady = true;
        clientCommandService = connectedServer.getObservablecommandExecutionService();
        observablePropertyService = connectedServer.getObservablePropertyService();

    }

}
