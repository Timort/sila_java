package sila_java.examples.hello_sila_cloud.unobserved_command;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.examples.hello_sila_cloud.client.UnobservableGreetingCommand;
import sila_java.library.cloud.client.command.UnobservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UnobservableCommandTest extends UnobservableCommandTestBase {

    @Test
    void testHello() throws InterruptedException {

        String message = "test";


        CountDownLatch countDownLatch = new CountDownLatch(1);
        UnobservableCommand greetCommand = createGreetCommand(message, (response) -> {

            log.info("Got greetingResponse: {}", response);

            assertTrue(response.getGreeting().getValue().contentEquals("Hello " + message));
            countDownLatch.countDown();

        });

        clientCommandService.executeCommand(greetCommand);
        countDownLatch.await(5, TimeUnit.SECONDS);

    }

    UnobservableCommand createGreetCommand(String name, Consumer<GreetingProviderOuterClass.SayHello_Responses> callback) {

        GreetingProviderOuterClass.SayHello_Parameters.Builder builder = GreetingProviderOuterClass.SayHello_Parameters
                .newBuilder();

        if (StringUtils.isNotBlank(name)) {
            builder.setName(SiLAString.from(name));
        }

        return UnobservableGreetingCommand
                .builder()
                .parameters(builder.build())
                .resultCallback(callback)
                .build();

    }
}
