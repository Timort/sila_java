package sila_java.examples.hello_sila_cloud.unobserved_command;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.examples.hello_sila_cloud.client.UnobservableGreetingCommand;
import sila_java.library.cloud.client.command.UnobservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UnobservableCommandErrorTest extends UnobservableCommandTestBase {

    @Test
    void testHelloNoName() throws InterruptedException {

        String name = "";
        AtomicBoolean isSilaError = new AtomicBoolean();
        AtomicBoolean isValidationError = new AtomicBoolean();
        StringBuilder actualMessage = new StringBuilder();


        CountDownLatch countDownLatch = new CountDownLatch(1);
        UnobservableCommand greetCommand = createGreetCommand(name, response -> {

            log.info("Got greetingResponse: {}", response);
            isSilaError.set(response != null);
            isValidationError.set(response.hasValidationError());
            actualMessage.append(response.getValidationError().getMessage());
            countDownLatch.countDown();

        });

        clientCommandService.executeCommand(greetCommand);
        countDownLatch.await(5, TimeUnit.SECONDS);
        assertTrue(isSilaError.get());
        assertTrue(isValidationError.get());
        assertEquals("Name required", actualMessage.toString());

    }

    @Test
    @Disabled
    void testHelloNotAllowedName() throws InterruptedException {

        String name = "MARKUS";
        AtomicBoolean isSilaError = new AtomicBoolean();
        AtomicBoolean isDefinedExecutionError = new AtomicBoolean();
        StringBuilder actualMessage = new StringBuilder();


        CountDownLatch countDownLatch = new CountDownLatch(1);
        UnobservableCommand greetCommand = createGreetCommand(name, response -> {

            log.info("Got greetingResponse: {}", response);
            isSilaError.set(response != null);
            isDefinedExecutionError.set(response.hasDefinedExecutionError());
            actualMessage.append(response.getDefinedExecutionError().getMessage());
            countDownLatch.countDown();

        });

        clientCommandService.executeCommand(greetCommand);
        boolean gotResponse = countDownLatch.await(5, TimeUnit.SECONDS);
        assertTrue(gotResponse, "No response from server - TIMEOUT");
        assertTrue(isSilaError.get());
        assertTrue(isDefinedExecutionError.get());
        assertEquals("Do not shout, " + name, actualMessage.toString());

    }

    UnobservableCommand createGreetCommand(String name, Consumer<SiLAFramework.SiLAError> callback) {

        GreetingProviderOuterClass.SayHello_Parameters.Builder builder = GreetingProviderOuterClass.SayHello_Parameters
                .newBuilder();
        if (StringUtils.isNotBlank(name)) {
            builder.setName(SiLAString.from(name));
        }

        return UnobservableGreetingCommand
                .builder()
                .parameters(builder.build())
                .errorCallback(callback)
                .build();

    }
}
