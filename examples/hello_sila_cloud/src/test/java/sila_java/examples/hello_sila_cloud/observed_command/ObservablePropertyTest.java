package sila_java.examples.hello_sila_cloud.observed_command;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila_java.examples.hello_sila_cloud.client.TimestampObservableProperty;
import sila_java.examples.hello_sila_cloud.client.YearObservableProperty;
import sila_java.library.cloud.client.property.ObservableProperty;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ObservablePropertyTest extends ObservableCommandTestBase {

    AtomicLong numberOfResults = new AtomicLong();

    @Test
    void testYearProperty() throws Exception {
        Long expected = 2020L;

        CompletableFuture<Long> longCompletableFuture = submitYearPropertyRequest();
        Long year = longCompletableFuture.get(10, TimeUnit.SECONDS);

        assertEquals(expected, year);
        assertEquals(10, numberOfResults.get());

    }

    @Test
    @Disabled("TODO: fix timing issue with testYearProperty()")
    void testTimestampProperty() {
        int expected = 5;
        AtomicInteger i = new AtomicInteger();
        CompletableFuture<SiLAFramework.Timestamp> completableFuture = submitTimestampPropertyRequest(t -> {
            int j = i.incrementAndGet();
            log.info("Got timestamp: {}-{}", j, t);
            if (j == expected) {
                observablePropertyService.cancelSubscription("1");
            }

        });
        assertThrows(TimeoutException.class, () -> completableFuture.get(5, TimeUnit.SECONDS));
        assertEquals(expected, i.get());

    }

    private CompletableFuture<SiLAFramework.Timestamp> submitTimestampPropertyRequest(Consumer<SiLAFramework.Timestamp> consumer) {
        CompletableFuture<SiLAFramework.Timestamp> future = new CompletableFuture<>();
        ObservableProperty property = createTimestampPropertyRequest(response -> {
            log.info("submit response {}", response.getTimestamp());
            SiLAFramework.Timestamp value = response.getTimestamp();

            consumer.accept(value);

            // future won't be completed


        });
        observablePropertyService.subscribeProperty(property);
        return future;
    }


    private CompletableFuture<Long> submitYearPropertyRequest() {
        CompletableFuture<Long> yearFuture = new CompletableFuture<>();
        ObservableProperty property = createPropertyRequest(response -> {
            log.info("Got yearPropertyResponse: {}", response);
            numberOfResults.incrementAndGet();

            long value = response.getStartYear().getValue();
            if (value != 2020) {
                log.info("still running {}", value);
            } else {
                log.info("done {}", value);
                yearFuture.complete(value);
            }
        });
        observablePropertyService.subscribeProperty(property);
        return yearFuture;
    }

    ObservableProperty createPropertyRequest(Consumer<ObservableGreetingProviderOuterClass.Subscribe_StartYear_Responses> callback) {
        ObservableGreetingProviderOuterClass.Subscribe_StartYear_Parameters params = ObservableGreetingProviderOuterClass.Subscribe_StartYear_Parameters
                .newBuilder().build();

        return YearObservableProperty.builder()
                .parameters(params)
                .resultCallback(callback)
                .build();

    }

    ObservableProperty createTimestampPropertyRequest(Consumer<ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Responses> callback) {
        ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Parameters params = ObservableGreetingProviderOuterClass.Subscribe_Timestamp_Parameters
                .newBuilder().build();
        return TimestampObservableProperty.builder()
                .parameters(params)
                .resultCallback(callback)
                .build();

    }
}
