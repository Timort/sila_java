package sila_java.examples.hello_sila_cloud.unobserved_command;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.examples.hello_sila_cloud.client.YearProperty;
import sila_java.library.cloud.client.property.UnobservableProperty;

import java.time.LocalDate;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UnobservablePropertyTest extends UnobservableCommandTestBase {

    @Test
    void testStartYear() throws InterruptedException {
        AtomicLong year = new AtomicLong();

        CountDownLatch countDownLatch = new CountDownLatch(1);
        UnobservableProperty propertyRequest = createPropertyRequest(response -> {

            log.info("Got greetingResponse: {}", response);
            year.set(response);
            countDownLatch.countDown();

        });

        unobservablePropertyService.readProperty(propertyRequest);
        countDownLatch.await(5, TimeUnit.SECONDS);
        assertEquals(LocalDate.now().getYear(), year.get());

    }

    UnobservableProperty createPropertyRequest(Consumer<Long> callback) {
        GreetingProviderOuterClass.Get_StartYear_Parameters params = GreetingProviderOuterClass.Get_StartYear_Parameters
                .newBuilder().build();

        return YearProperty.builder()
                .parameters(params)
                .resultCallback(responses -> {
                    log.info("response: {}", responses);
                    callback.accept(responses.getStartYear().getValue());
                })
                .build();
    }
}
