package sila_java.examples.hello_sila_cloud.observed_command;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.plexus.util.StringUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass;
import sila2.org.silastandard.examples.observablegreetingprovider.v1.ObservableGreetingProviderOuterClass.SayHello_Responses;
import sila_java.examples.hello_sila_cloud.client.ObservableGreetingCommand;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.core.sila.types.SiLAString;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
class ObservableCommandIntermediateResultTest extends ObservableCommandTestBase {


    @Disabled("intermediate not implemented in ObservableCommandWrapper")
    @Test
    void testHelloIntermediate() throws InterruptedException, TimeoutException, ExecutionException {

        String name = "test";

        CompletableFuture<SiLAFramework.CommandExecutionUUID> commandExecutionUUIDFuture = submitCommand(name);

        SiLAFramework.CommandExecutionUUID commandExecutionUUID = commandExecutionUUIDFuture.get(5, TimeUnit.SECONDS);
        SiLAFramework.CommandConfirmation confirmation = SiLAFramework.CommandConfirmation.newBuilder()
                .setCommandExecutionUUID(commandExecutionUUID)
                .build();

        CompletableFuture<SayHello_Responses> infoFuture = submitCommand(confirmation);

        String actual = infoFuture.get(5, TimeUnit.SECONDS).getGreeting().getValue();
        log.info("got execution info {}", actual);
        assertNotNull(actual);
    }

    private CompletableFuture<SiLAFramework.CommandExecutionUUID> submitCommand(String name) {
        CompletableFuture<SiLAFramework.CommandExecutionUUID> commandExecutionUUIDFuture = new CompletableFuture<>();
        ObservableCommand command = createGreetCommand(name, response -> commandExecutionUUIDFuture.complete(response.getCommandExecutionUUID()));
        clientCommandService.executeCommand(command);
        return commandExecutionUUIDFuture;
    }

    private CompletableFuture<SayHello_Responses> submitCommand(SiLAFramework.CommandConfirmation commandConfirmation) {
        CompletableFuture<SayHello_Responses> infoFuture = new CompletableFuture<>();
        ObservableCommand greetCommand = createGreetCommandIntermediaResult(commandConfirmation, (response) -> {
            log.info("Got greetingInfoResponse: {}", response);
            infoFuture.complete(response);
        });

        clientCommandService.subscribeCommandIntermediateResult(greetCommand);
        return infoFuture;
    }

    ObservableCommand createGreetCommand(String name, Consumer<SiLAFramework.CommandConfirmation> callback) {

        ObservableGreetingProviderOuterClass.SayHello_Parameters.Builder parameter = ObservableGreetingProviderOuterClass.SayHello_Parameters
                .newBuilder();

        if (StringUtils.isNotBlank(name)) {
            parameter.setName(SiLAString.from(name));
        }

        return ObservableGreetingCommand
                .builder()
                .parameters(parameter.build())
                .confirmationCallback(callback)
                .build();
    }

    private ObservableCommand createGreetCommandIntermediaResult(SiLAFramework.CommandConfirmation confirmation, Consumer<SayHello_Responses> callback) {

        ObservableGreetingCommand command = ObservableGreetingCommand
                .builder()
                .intermediateResultCallback(callback)
                .build();
        command.handleCommandConfirmation(confirmation);
        return command;
    }

}
