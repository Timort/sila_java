package sila_java.examples.sila_timer_service.server;

import com.google.protobuf.ByteString;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.server.command.ObservedCommandHandlerImpl;

import java.util.function.Consumer;

public class ResetTimerCloudCommand extends ObservedCommandHandlerImpl<SilaTimerServiceOuterClass.ResetTimer_Parameters, SilaTimerServiceOuterClass.ResetTimer_Responses> {

    private final TimerService timerService;

    public ResetTimerCloudCommand(TimerService timerService) {
        super(SilaTimerServiceOuterClass.ResetTimer_Parameters.parser());
        this.timerService = timerService;
    }

    @Override
    public SiLAFramework.CommandConfirmation handle(SilaTimerServiceOuterClass.ResetTimer_Parameters requestParameters) {
        timerService.resetTimer();
        return SiLAFramework.CommandConfirmation.newBuilder()
                .build();

    }

    @Override
    public void handleInfo(SiLAFramework.CommandExecutionUUID executionUUID, Consumer<SiLAFramework.ExecutionInfo> infoConsumer) {

    }

    @Override
    public void handleIntermediateResult(SiLAFramework.CommandExecutionUUID executionUUID, Consumer<ByteString> intermediateResultConsumer) {

    }

    @Override
    public SilaTimerServiceOuterClass.ResetTimer_Responses handleResult(SiLAFramework.CommandExecutionUUID executionUUID) {
        long timerValue = timerService.getTimerValue();
        return SilaTimerServiceOuterClass.ResetTimer_Responses.newBuilder()
                .setTimerValue(SiLAFramework.Integer.newBuilder().setValue(timerValue))
                .build();
    }

    @Override
    public void handleExecutionInfoCancel() {

    }

    @Override
    public void handleIntermediateResponseCancel() {

    }


}
