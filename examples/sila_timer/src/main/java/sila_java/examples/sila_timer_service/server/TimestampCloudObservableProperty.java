package sila_java.examples.sila_timer_service.server;

import com.google.protobuf.ByteString;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.server.property.ObservedPropertyHandlerImpl;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

@Slf4j
public class TimestampCloudObservableProperty extends ObservedPropertyHandlerImpl<SilaTimerServiceOuterClass.Subscribe_Value_Parameters, SilaTimerServiceOuterClass.Subscribe_Value_Responses> {

    private final TimerService timerService;
    private final AtomicBoolean propertySubscription = new AtomicBoolean();


    public TimestampCloudObservableProperty(TimerService timerService) {
        super(SilaTimerServiceOuterClass.Subscribe_Value_Parameters.parser());
        this.timerService = timerService;
    }

    @Override
    public void handleSubscription(SilaTimerServiceOuterClass.Subscribe_Value_Parameters parameters, Consumer<ByteString> consumer) {
        log.info("handlePropertySubscription");
        propertySubscription.set(true);
        new Thread(() -> {
            while (propertySubscription.get()) {
                long timerValue = timerService.getTimerValue();
                SilaTimerServiceOuterClass.Subscribe_Value_Responses response = SilaTimerServiceOuterClass.Subscribe_Value_Responses.newBuilder()
                        .setValue(SiLAFramework.Integer.newBuilder().setValue(timerValue))
                        .build();
                consumer.accept(response.toByteString());
                try {
                    Thread.sleep(1_000);
                } catch (InterruptedException e) {
                }
            }
        }).start();

    }

    @Override
    public void handleCancelSubscription(String requestUUID) {
        propertySubscription.set(false);
    }
}
