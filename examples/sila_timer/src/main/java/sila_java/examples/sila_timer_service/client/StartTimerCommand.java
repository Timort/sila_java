package sila_java.examples.sila_timer_service.client;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.command.ObservableCommandImpl;

import java.util.function.Consumer;

@Slf4j
public class StartTimerCommand extends ObservableCommandImpl<SilaTimerServiceOuterClass.StartTimer_Parameters, SilaTimerServiceOuterClass.StartTimer_Responses> {

    private static final String COMMAND_ID = "org.silastandard/examples/SilaTimerService/v1/Command/StartTimer";

    @Builder
    public StartTimerCommand(SilaTimerServiceOuterClass.StartTimer_Parameters parameters, Consumer<SiLAFramework.CommandConfirmation> confirmationCallback, Consumer<SiLAFramework.ExecutionInfo> executionInfoCallback, Consumer<SilaTimerServiceOuterClass.StartTimer_Responses> intermediateResultCallback, Consumer<SilaTimerServiceOuterClass.StartTimer_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(COMMAND_ID, parameters, confirmationCallback, executionInfoCallback, intermediateResultCallback, resultCallback, errorCallback, SilaTimerServiceOuterClass.StartTimer_Responses.parser());
    }

    @Override
    public void onCommandConfirmation(SiLAFramework.CommandConfirmation confirmation) {
        log.info("onCommandConfirmation {}", confirmation);
    }

    @Override
    public void onCommandExecutionInfo(SiLAFramework.ExecutionInfo executionInfo) {
        log.info("onCommandExecutionInfo {}", executionInfo);
    }

    @Override
    public void onIntermediateResponse(SilaTimerServiceOuterClass.StartTimer_Responses responses) {
        log.info("onIntermediateResponse {}", responses.getTimerValue());
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.StartTimer_Responses responses) {
        log.info("onResponse {}", responses.getTimerValue());
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.info("onError {}", error);
    }

}
