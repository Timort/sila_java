package sila_java.examples.sila_timer_service.server;

import com.google.protobuf.ByteString;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.server.command.ObservedCommandHandlerImpl;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

@Slf4j
public class StartTimerCloudCommand extends ObservedCommandHandlerImpl<SilaTimerServiceOuterClass.StartTimer_Parameters, SilaTimerServiceOuterClass.StartTimer_Responses> {

    private final TimerService timerService;
    private final AtomicBoolean executionInfoRunning;
    private final AtomicBoolean intermediateResponseRunning;

    @Builder
    public StartTimerCloudCommand(TimerService timerService) {
        super(SilaTimerServiceOuterClass.StartTimer_Parameters.parser());
        this.timerService = timerService;
        this.executionInfoRunning = new AtomicBoolean();
        this.intermediateResponseRunning = new AtomicBoolean();
    }

    @Override
    public SiLAFramework.CommandConfirmation handle(SilaTimerServiceOuterClass.StartTimer_Parameters requestParameters) {
        timerService.startTimer();
        String uuid = UUID.randomUUID().toString();
        return SiLAFramework.CommandConfirmation.newBuilder()
                .setCommandExecutionUUID(SiLAFramework.CommandExecutionUUID.newBuilder().setValue(uuid))
                .build();
    }

    @Override
    public void handleInfo(SiLAFramework.CommandExecutionUUID executionUUID, Consumer<SiLAFramework.ExecutionInfo> infoConsumer) {
        log.info("handleInfo, executionUUID {}", executionUUID);
        executionInfoRunning.set(true);
        runInThread(() -> {
            while (executionInfoRunning.get()) {

                SiLAFramework.ExecutionInfo.CommandStatus status;
                if (timerService.isRunning()) {
                    status = SiLAFramework.ExecutionInfo.CommandStatus.running;
                } else {
                    status = SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully;
                    handleExecutionInfoCancel();
                }
                SiLAFramework.ExecutionInfo executionInfo = SiLAFramework.ExecutionInfo.newBuilder()
                        .setCommandStatus(status)
                        .build();
                infoConsumer.accept(executionInfo);
                log.info("handleInfo {}", executionInfo);
                try {
                    Thread.sleep(5_000);
                } catch (InterruptedException e) {
                }
            }
        });
    }

    @Override
    public void handleIntermediateResult(SiLAFramework.CommandExecutionUUID executionUUID, Consumer<ByteString> intermediateResultConsumer) {
        log.info("handleIntermediateResult, executionUUID {}", executionUUID);
        intermediateResponseRunning.set(true);
        runInThread(() -> {
            while (intermediateResponseRunning.get()) {
                if (!timerService.isRunning()) {
                    handleIntermediateResponseCancel();
                }
                long timerValue = timerService.getTimerValue();
                SilaTimerServiceOuterClass.StartTimer_Responses response = SilaTimerServiceOuterClass.StartTimer_Responses.newBuilder()
                        .setTimerValue(SiLAFramework.Integer.newBuilder().setValue(timerValue))
                        .build();
                intermediateResultConsumer.accept(response.toByteString());
                log.info("timer is running for {}", timerValue);
                try {
                    Thread.sleep(1_000);
                } catch (InterruptedException e) {
                }
            }
        });
        log.info("timer is done");
    }

    @Override
    public SilaTimerServiceOuterClass.StartTimer_Responses handleResult(SiLAFramework.CommandExecutionUUID executionUUID) {
        long timerValue = timerService.getTimerValue();
        return SilaTimerServiceOuterClass.StartTimer_Responses.newBuilder()
                .setTimerValue(SiLAFramework.Integer.newBuilder().setValue(timerValue))
                .build();
    }

    @Override
    public void handleExecutionInfoCancel() {
        log.info("handleExecutionInfoCancel");
        executionInfoRunning.set(false);
    }

    @Override
    public void handleIntermediateResponseCancel() {
        log.info("handleIntermediateResponseCancel");
        intermediateResponseRunning.set(false);
    }

    public void runInThread(Runnable runnable) {
        new Thread(runnable).start();
    }

}
