package sila_java.examples.sila_timer_service.client;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.property.ObservablePropertyImpl;

import java.util.function.Consumer;


@Slf4j
public class TimerValueObservableProperty extends ObservablePropertyImpl<SilaTimerServiceOuterClass.Subscribe_Value_Parameters, SilaTimerServiceOuterClass.Subscribe_Value_Responses> {

    private static final String PROPERTY_ID = "org.silastandard/examples/SilaTimerService/v1/Property/Value";

    @Builder
    public TimerValueObservableProperty(SilaTimerServiceOuterClass.Subscribe_Value_Parameters parameters, Consumer<SilaTimerServiceOuterClass.Subscribe_Value_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, errorCallback, SilaTimerServiceOuterClass.Subscribe_Value_Responses.parser());
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.Subscribe_Value_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError error) {
        log.error("Error received: {}", error);
    }
}
