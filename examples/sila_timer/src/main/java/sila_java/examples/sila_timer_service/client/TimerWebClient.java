package sila_java.examples.sila_timer_service.client;

import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.client.CloudConnectedServer;

@Slf4j
public class TimerWebClient extends TimerCloudObservableClient {

    @Override
    public void onServerConnect(CloudConnectedServer server) {
        log.info("some server connected! {}", server);
    }

}
