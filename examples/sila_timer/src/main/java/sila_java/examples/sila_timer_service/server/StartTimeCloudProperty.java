package sila_java.examples.sila_timer_service.server;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.server.property.UnobservedPropertyHandlerImpl;

@Slf4j
public class StartTimeCloudProperty extends UnobservedPropertyHandlerImpl<SilaTimerServiceOuterClass.Get_StartTime_Parameters, SilaTimerServiceOuterClass.Get_StartTime_Responses> {

    private final TimerService timerService;

    public StartTimeCloudProperty(TimerService timerService) {
        super(SilaTimerServiceOuterClass.Get_StartTime_Parameters.parser());
        this.timerService = timerService;
    }

    @Override
    public SilaTimerServiceOuterClass.Get_StartTime_Responses handle(SilaTimerServiceOuterClass.Get_StartTime_Parameters requestParameters) {
        String startTime = timerService.getStartTime();
        return SilaTimerServiceOuterClass.Get_StartTime_Responses.newBuilder()
                .setStartTime(SiLAFramework.String.newBuilder().setValue(startTime))
                .build();

    }

}
