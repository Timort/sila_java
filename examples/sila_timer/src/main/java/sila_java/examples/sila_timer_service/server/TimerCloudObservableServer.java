package sila_java.examples.sila_timer_service.server;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.cloud.server.SILACloudServer;
import sila_java.library.server_base.utils.ArgumentHelper;

import static sila_java.library.cloud.client.SILACloudClient.SILA_CLOUD_DEFAULT_PORT;
import static sila_java.library.core.utils.Utils.blockUntilStop;

@Slf4j
public class TimerCloudObservableServer implements AutoCloseable {

    public static final String SERVER_TYPE = "Timer SiLA Cloud Server";
    private static final String DEFAULT_LOCALHOST = "localhost";
    SILACloudServer cloudServer;

    /**
     * Application Class using command line arguments
     *
     * @param argumentHelper Custom Argument Helper
     */
    public TimerCloudObservableServer(@NonNull final ArgumentHelper argumentHelper) {

        String host = argumentHelper.getHost().orElse(DEFAULT_LOCALHOST);
        TimerService timerService = new TimerService();
        log.info("Launching cloud server, connecting to host at {}", host);
        cloudServer = new SILACloudServer.Builder()
                //.withHost("sila.eu-central-1.elasticbeanstalk.com")
                .withHost(host)
                .withPort(argumentHelper.getPort().orElse(SILA_CLOUD_DEFAULT_PORT))
                .withCommandHandler(
                        "org.silastandard/examples/SilaTimerService/v1/Command/StartTimer",
                        new StartTimerCloudCommand(timerService))
                .withCommandHandler(
                        "org.silastandard/examples/SilaTimerService/v1/Command/StopTimer",
                        new StopTimerCloudCommand(timerService))
                .withCommandHandler(
                        "org.silastandard/examples/SilaTimerService/v1/Command/ResetTimer",
                        new ResetTimerCloudCommand(timerService))
                .withPropertyHandler(
                        "org.silastandard/examples/SilaTimerService/v1/Property/Value",
                        new TimestampCloudObservableProperty(timerService))
                .withPropertyHandler(
                        "org.silastandard/examples/SilaTimerService/v1/Property/StartTime",
                        new StartTimeCloudProperty(timerService))
                .build();

        cloudServer.start();
        log.info("Server started streaming to cloud");

    }

    @Override
    public void close() {
        this.cloudServer.stop();
    }

    /**
     * Simple main function that starts the server and keeps it alive
     */
    public static void main(final String[] args) {

        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);

        try (final TimerCloudObservableServer cloudServer = new TimerCloudObservableServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(cloudServer::close));
            blockUntilStop();
        }

    }

}
