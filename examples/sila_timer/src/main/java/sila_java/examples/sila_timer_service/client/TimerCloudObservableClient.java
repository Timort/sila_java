package sila_java.examples.sila_timer_service.client;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass.ResetTimer_Parameters;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass.StartTimer_Parameters;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass.StopTimer_Parameters;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.client.command.ObservableCommandExecutionService;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.cloud.client.property.ObservablePropertyService;
import sila_java.library.cloud.client.property.ObservableProperty;
import sila_java.library.cloud.client.property.UnobservablePropertyService;
import sila_java.library.cloud.client.property.UnobservableProperty;

import java.io.IOException;
import java.util.function.Consumer;

@Slf4j
public abstract class TimerCloudObservableClient {

    protected SILACloudClient cloudClient;

    public abstract void onServerConnect(CloudConnectedServer server);

    public void startClient() throws IOException {
        cloudClient = new SILACloudClient.Builder()
                .withServerConnectedListener(this::onServerConnect)
                .start();
    }

    public void stopClient() {
        cloudClient.shutdown();
        log.info("Cloud client has been shut down");
    }

    public void executeCommand(ObservableCommand command, ObservableCommandExecutionService commandExecutionService) {
        if (command != null) {
            commandExecutionService.executeCommand(command);
        }
    }

    public void subscribeCommandIntermediateResponse(ObservableCommand command, ObservableCommandExecutionService commandExecutionService) {
        if (command != null) {
            commandExecutionService.subscribeCommandIntermediateResult(command);
        }
    }

    void subscribeCommandExecutionInfo(ObservableCommand command, ObservableCommandExecutionService commandExecutionService) {
        if (command != null) {
            commandExecutionService.subscribeCommandInfo(command);
        }
    }

    void cancelCommandIntermediateResponseSubscription(ObservableCommand command, ObservableCommandExecutionService commandExecutionService) {
        if (command.getIntermediateResponseSubscriptionId() == null) {
            log.info("cancelCommandIntermediateResponseSubscription requires subscription");
            return;
        }
        commandExecutionService.cancelIntermediateResponseSubscription(command.getIntermediateResponseSubscriptionId());
    }

    void cancelCommandExecutionInfoSubscription(ObservableCommand command, ObservableCommandExecutionService commandExecutionService) {
        if (command.getExecutionInfoSubscriptionId() == null) {
            log.info("cancelCommandExecutionInfoSubscription requires subscription");
            return;
        }
        commandExecutionService.cancelExecutionInfoSubscription(command.getExecutionInfoSubscriptionId());
    }

    public ObservableCommand createStartCommand(Consumer<SilaTimerServiceOuterClass.StartTimer_Responses> resultCallback) {

        StartTimer_Parameters parameter = StartTimer_Parameters
                .newBuilder().build();

        return StartTimerCommand
                .builder()
                .parameters(parameter)
                .executionInfoCallback(response -> log.info("execution info {}", response))
                .resultCallback(resultCallback)
                .build();
    }

    public ObservableCommand createStartCommand(
            Consumer<SiLAFramework.CommandConfirmation> confirmationCallback,
            Consumer<SilaTimerServiceOuterClass.StartTimer_Responses> intermediateResultCallback) {

        StartTimer_Parameters parameter = StartTimer_Parameters
                .newBuilder().build();

        return StartTimerCommand
                .builder()
                .parameters(parameter)
                .confirmationCallback(confirmationCallback)
                .executionInfoCallback(response -> log.info("execution info {}", response))
                .intermediateResultCallback(intermediateResultCallback)
                .build();
    }

    public ObservableCommand createStopCommand(Consumer<SilaTimerServiceOuterClass.StopTimer_Responses> callback) {

        StopTimer_Parameters parameter = StopTimer_Parameters
                .newBuilder().build();

        return StopTimerCommand
                .builder()
                .parameters(parameter)
                .resultCallback(callback)
                .build();
    }

    public ObservableCommand createResetCommand(Consumer<SilaTimerServiceOuterClass.ResetTimer_Responses> callback) {

        ResetTimer_Parameters parameter = ResetTimer_Parameters
                .newBuilder().build();

        return ResetTimerCommand
                .builder()
                .parameters(parameter)
                .resultCallback(callback)
                .build();
    }

    public void executeProperty(UnobservableProperty property, UnobservablePropertyService propertyService) {
        if (property != null) {
            propertyService.readProperty(property);
        }
    }

    public void subscribeToProperty(ObservableProperty property, ObservablePropertyService observablePropertyService) {
        if (property != null) {
            observablePropertyService.subscribeProperty(property);
        }
    }

    public void cancelPropertySubscription(ObservableProperty property, ObservablePropertyService observablePropertyService) {
        if (property != null) {
            observablePropertyService.cancelSubscription(property.getRequestUUID());
        }
    }

    UnobservableProperty createStartTimeProperty(Consumer<SilaTimerServiceOuterClass.Get_StartTime_Responses> callback) {

        return StartTimeProperty.builder()
                .errorCallback(e -> log.warn("oh oh {}", e))
                .resultCallback(callback)
                .build();

    }

    ObservableProperty createTimeValueProperty(Consumer<SilaTimerServiceOuterClass.Subscribe_Value_Responses> callback) {
        return TimerValueObservableProperty.builder()
                .errorCallback(e -> log.warn("oh oh {}", e))
                .resultCallback(callback)
                .build();
    }

}
