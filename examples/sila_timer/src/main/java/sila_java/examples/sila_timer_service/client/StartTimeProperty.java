package sila_java.examples.sila_timer_service.client;

import com.google.protobuf.ByteString;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.property.UnobservablePropertyImpl;

import java.util.function.Consumer;

@Slf4j
@Getter
public class StartTimeProperty extends UnobservablePropertyImpl<SilaTimerServiceOuterClass.Get_StartTime_Parameters, SilaTimerServiceOuterClass.Get_StartTime_Responses> {

    private static final String PROPERTY_ID = "org.silastandard/examples/SilaTimerService/v1/Property/StartTime";

    @Builder
    public StartTimeProperty(SilaTimerServiceOuterClass.Get_StartTime_Parameters parameters, Consumer<SilaTimerServiceOuterClass.Get_StartTime_Responses> resultCallback, Consumer<SiLAFramework.SiLAError> errorCallback) {
        super(PROPERTY_ID, parameters, resultCallback, SilaTimerServiceOuterClass.Get_StartTime_Responses.parser(), errorCallback);
    }

    @Override
    public void onResponse(SilaTimerServiceOuterClass.Get_StartTime_Responses responses) {
        log.info("Response received: {}", responses);
    }

    @Override
    public void onError(SiLAFramework.SiLAError silaError) {
        log.warn("error received: {}", silaError);
    }

    @Override
    public String getPropertyId() {
        return PROPERTY_ID;
    }

    @Override
    public ByteString getPayload() {
        SilaTimerServiceOuterClass.Get_StartTime_Responses yearResponses = SilaTimerServiceOuterClass.Get_StartTime_Responses.newBuilder()
                .setStartTime(SiLAFramework.String.newBuilder().setValue("2020"))
                .build();
        return yearResponses.toByteString();
    }

}
