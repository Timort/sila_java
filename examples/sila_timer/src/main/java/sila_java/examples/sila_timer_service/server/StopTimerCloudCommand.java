package sila_java.examples.sila_timer_service.server;

import com.google.protobuf.ByteString;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.server.command.ObservedCommandHandlerImpl;

import java.util.function.Consumer;

@Slf4j
public class StopTimerCloudCommand extends ObservedCommandHandlerImpl<SilaTimerServiceOuterClass.StopTimer_Parameters, SilaTimerServiceOuterClass.StopTimer_Responses> {

    private final TimerService timerService;

    public StopTimerCloudCommand(TimerService timerService) {
        super(SilaTimerServiceOuterClass.StopTimer_Parameters.parser());
        this.timerService = timerService;
    }

    @Override
    public SiLAFramework.CommandConfirmation handle(SilaTimerServiceOuterClass.StopTimer_Parameters requestParameters) {

        timerService.stopTimer();
        long timerValue = timerService.getTimerValue();
        log.info("handle STOP at {} ms. Timer is running: {}", timerValue, timerService.isRunning());
        return SiLAFramework.CommandConfirmation.newBuilder()
                .build();

    }

    @Override
    public void handleInfo(SiLAFramework.CommandExecutionUUID executionUUID, Consumer<SiLAFramework.ExecutionInfo> infoConsumer) {
        log.info("handleInfo");
    }

    @Override
    public void handleIntermediateResult(SiLAFramework.CommandExecutionUUID executionUUID, Consumer<ByteString> intermediateResultConsumer) {
        log.info("handleIntermediateResult");

    }

    @Override
    public SilaTimerServiceOuterClass.StopTimer_Responses handleResult(SiLAFramework.CommandExecutionUUID executionUUID) {
        long timerValue = timerService.getTimerValue();
        log.info("handleResult {}", timerValue);
        return SilaTimerServiceOuterClass.StopTimer_Responses.newBuilder()
                .setTimerValue(SiLAFramework.Integer.newBuilder().setValue(timerValue))
                .build();
    }

    @Override
    public void handleExecutionInfoCancel() {

    }

    @Override
    public void handleIntermediateResponseCancel() {

    }


}
