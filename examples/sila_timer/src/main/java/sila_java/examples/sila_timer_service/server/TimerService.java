package sila_java.examples.sila_timer_service.server;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class TimerService {

    private final StopWatch stopWatch = new StopWatch();
    private LocalDateTime startTime;

    public void startTimer() {
        log.info("startTimer. Currently at {} ms.", stopWatch.getTime());
        if (stopWatch.isStarted()) {
            log.info("timer already started");
            if (stopWatch.isSuspended()) {
                stopWatch.resume();
            }
        } else {
            stopWatch.start();
            startTime = LocalDateTime.now();
        }
    }

    public void stopTimer() {
        log.info("stopTimer. Currently at {} ms.", stopWatch.getTime());
        if (stopWatch.isStarted() && !stopWatch.isSuspended()) {
            stopWatch.suspend();
        }
    }

    public void resetTimer() {
        log.info("resetTimer. Was at {} ms.", stopWatch.getTime());
        stopWatch.reset();
    }

    public long getTimerValue() {
        return stopWatch.getTime();
    }

    public boolean isRunning() {
        return stopWatch.isStarted() && !stopWatch.isSuspended();
    }

    public boolean isStopped() {
        return stopWatch.isStopped();
    }

    public boolean isSuspended() {
        return stopWatch.isSuspended();
    }

    public String getStartTime() {
        if (startTime == null) {
            return "--";
        }
        return startTime.format(DateTimeFormatter.ISO_DATE_TIME);
    }
}

