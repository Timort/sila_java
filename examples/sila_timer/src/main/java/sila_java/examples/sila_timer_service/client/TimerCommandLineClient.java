package sila_java.examples.sila_timer_service.client;

import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.examples.silatimerservice.v1.SilaTimerServiceOuterClass;
import sila_java.library.cloud.client.CloudConnectedServer;
import sila_java.library.cloud.client.command.ObservableCommand;
import sila_java.library.cloud.client.property.ObservableProperty;
import sila_java.library.cloud.client.property.UnobservableProperty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

@Slf4j
public class TimerCommandLineClient extends TimerCloudObservableClient {

    private ObservableCommand startCommand;
    private ObservableProperty timeValueProperty;

    /**
     * start client providing rRCP cloud service endpoint
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        long stayAliveSeconds = 120;
        log.info("Launching cloud client");

        TimerCommandLineClient commandLineClient = new TimerCommandLineClient();
        commandLineClient.startClient();


        log.info("client has been started. Keeping alive for {} seconds", stayAliveSeconds);
        log.info("# No connectedServers on client start: {}", commandLineClient.cloudClient.getConnectedServers().size());
        Thread.sleep(stayAliveSeconds * 10_000);
        log.info("# No connectedServers before client ends: {}", commandLineClient.cloudClient.getConnectedServers().size());
        log.info("{} seconds past. finish", stayAliveSeconds);

        commandLineClient.stopClient();
    }

    @Override
    public void onServerConnect(CloudConnectedServer server) {
        log.info("Server has connected. Client can send commands to it now");

        AtomicBoolean running = new AtomicBoolean(true);

        Thread thread = new Thread(() -> {

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

                while (running.get()) {

                    log.info("usage: start, info, intermediate, stop, reset, cancel-info (ce), cancel-intermediate (ci), kill");
                    // Reading data using readLine
                    String line = reader.readLine();

                    switch (line.toLowerCase()) {
                        case "start":
                            startCommand = createStartCommand(response -> log.info("got start response {}", response));
                            executeCommand(startCommand, server.getObservablecommandExecutionService());
                            break;
                        case "info":
                            subscribeCommandExecutionInfo(startCommand, server.getObservablecommandExecutionService());
                            break;
                        case "intermediate":
                            subscribeCommandIntermediateResponse(startCommand, server.getObservablecommandExecutionService());
                            break;
                        case "ce":
                        case "cancel-info":
                            cancelCommandExecutionInfoSubscription(startCommand, server.getObservablecommandExecutionService());
                            break;
                        case "ci":
                        case "cancel-intermediate":
                            cancelCommandIntermediateResponseSubscription(startCommand, server.getObservablecommandExecutionService());
                            break;
                        case "stop":
                            ObservableCommand stopCommand = createStopCommand(response -> log.info("got stop response {}", response));
                            executeCommand(stopCommand, server.getObservablecommandExecutionService());
                            break;
                        case "reset":
                            ObservableCommand resetCommand = createResetCommand(response -> log.info("got reset response {}", response));
                            executeCommand(resetCommand, server.getObservablecommandExecutionService());
                            break;
                        case "starttime":
                            UnobservableProperty startProperty = createStartTimeProperty(response -> log.info("got property response {}", response));
                            executeProperty(startProperty, server.getUnobservablePropertyService());
                            break;
                         case "time":
                            timeValueProperty = createTimeValueProperty(response -> log.info("got property response {}", response));
                            subscribeToProperty(timeValueProperty, server.getObservablePropertyService());
                            break;
                        case "ct":
                        case "cancel-time":
                            cancelPropertySubscription(timeValueProperty, server.getObservablePropertyService());
                            break;
                        case "kill":
                            running.set(false);
                            log.info("shutdown SILA client");
                            stopClient();
                            break;
                        default:
                            log.info("no such command ''{}''", line);
                    }
                }
            } catch (Exception e) {
                log.warn("error", e);
            }
        });
        thread.start();
    }

    @Override
    public ObservableCommand createStartCommand(Consumer<SilaTimerServiceOuterClass.StartTimer_Responses> callback) {
        if (startCommand == null) {
            startCommand = super.createStartCommand(callback);
        }
        return startCommand;
    }
}
