package sila_java.examples.thermostat;

import io.grpc.Context;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerGrpc;
import sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureControllerOuterClass;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAReal;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;
import sila_java.library.server_base.command.observable.ObservableCommandWrapper;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.blockUntilStop;

/**
 * Example Implementation of a SiLA Server with Observable Commands
 *
 * @implNote A lot of functionality should be provided by the sila_library
 */
@Slf4j
public class ThermostatServer implements AutoCloseable {
    static final String SERVER_TYPE = "Thermostat Server";
    private static final int EXECUTION_LIFETIME = 1000; // [s]
    private static final String featureLocation =
            "/sila_base/feature_definitions/org/silastandard/examples/TemperatureController.sila.xml";
    private final SiLAServer siLAServer;
    private final ThermostatSimulation thermostatSimulation = new ThermostatSimulation();
    private final TemperatureControlImpl temperatureControl = new TemperatureControlImpl();

    public ThermostatServer(@NonNull final ArgumentHelper argumentHelper) {
        val serverInfo = new ServerInformation(
                SERVER_TYPE,
                "Simple Example of a Thermostat",
                "www.sila-standard.org",
                "v0.0"
        );

        try {
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            builder.addFeature(
                    getFileContent(EmptyClass.class.getResourceAsStream(featureLocation)),
                    temperatureControl
            );

            this.siLAServer = builder.start();
        } catch (IOException | SelfSignedCertificate.CertificateGenerationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.temperatureControl.temperatureManager.close();
        this.siLAServer.close();
    }

    public static void main(String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);

        // Start Server
        try (final ThermostatServer server = new ThermostatServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(server::close));
            blockUntilStop();
        }
        System.out.println("termination complete.");
    }

    class TemperatureControlImpl extends TemperatureControllerGrpc.TemperatureControllerImplBase {
        ObservableCommandManager<
                TemperatureControllerOuterClass.ControlTemperature_Parameters,
                TemperatureControllerOuterClass.ControlTemperature_Responses
                > temperatureManager = new ObservableCommandManager<>(
                new ObservableCommandTaskRunner(1, 1),
                this::runCommandTask,
                Duration.ofSeconds(EXECUTION_LIFETIME)
        );

        private TemperatureControllerOuterClass.ControlTemperature_Responses runCommandTask(
                @NonNull final ObservableCommandWrapper<
                        TemperatureControllerOuterClass.ControlTemperature_Parameters,
                        TemperatureControllerOuterClass.ControlTemperature_Responses
                        > command
        ) throws StatusRuntimeException {
            // @TODO: Validation Errors
            final double targetTemperature = command.getParameter().getTargetTemperature().getValue();
            final double startTemperature = thermostatSimulation.getCurrentTemperature();
            if (targetTemperature == startTemperature) {
                return TemperatureControllerOuterClass.ControlTemperature_Responses.newBuilder().build();
            }
            final ThermostatSimulation thermostatSimulation = ThermostatServer.this.thermostatSimulation;
            final CompletableFuture<Double> future = new CompletableFuture<>();
            thermostatSimulation.setTargetTemperature(targetTemperature);

            final ThermostatSimulation.TemperatureListener listener = temperature -> {
                final double absoluteTemperatureDifference = Math.abs(targetTemperature - temperature);
                final double progress = 1.0d - (absoluteTemperatureDifference / Math.abs(targetTemperature - startTemperature));
                final Duration timeLeft = Duration.ofSeconds((long) (absoluteTemperatureDifference / ThermostatSimulation.KELVIN_PER_SECONDS));
                command.setExecutionInfoAndNotify(progress, timeLeft);
                if (absoluteTemperatureDifference <= ThermostatSimulation.KELVIN_ACCURACY) {
                    future.complete(temperature);
                }
            };
            thermostatSimulation.addListener(listener);
            try {
                future.get();
                return TemperatureControllerOuterClass.ControlTemperature_Responses.newBuilder().build();
            } catch (InterruptedException | ExecutionException e) {
                throw SiLAErrors.generateGenericExecutionError(e);
            } finally {
                thermostatSimulation.removeListener(listener);
            }
        }

        @Override
        public void controlTemperature(
                @NonNull final TemperatureControllerOuterClass.ControlTemperature_Parameters parameters,
                @NonNull final StreamObserver<SiLAFramework.CommandConfirmation> commandConfirmationStreamObserver
        ) {
            this.temperatureManager.addCommand(parameters, commandConfirmationStreamObserver);
        }

        @Override
        public void controlTemperatureInfo(
                @NonNull final SiLAFramework.CommandExecutionUUID commandExecutionUUID,
                @NonNull final StreamObserver<SiLAFramework.ExecutionInfo> executionInfoStreamObserver
        ) {
            this.temperatureManager.get(commandExecutionUUID).addStateObserver(executionInfoStreamObserver);
        }

        @Override
        public void controlTemperatureResult(
                @NonNull final SiLAFramework.CommandExecutionUUID commandExecutionUUID,
                @NonNull final StreamObserver<TemperatureControllerOuterClass.ControlTemperature_Responses> responsesStreamObserver
        ) {
            this.temperatureManager.get(commandExecutionUUID).sendResult(responsesStreamObserver);
        }

        @Override
        public void subscribeCurrentTemperature(
                @NonNull final TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Parameters parameters,
                @NonNull final StreamObserver<TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses> responsesStreamObserver
        ) {
            final ThermostatSimulation thermostatSimulation = ThermostatServer.this.thermostatSimulation;

            // Always send back current Temperature
            sendTemperature(responsesStreamObserver, thermostatSimulation.getCurrentTemperature());
            // Subscribe to Changes
            final ThermostatSimulation.TemperatureListener temperatureListener =
                    temperature->sendTemperature(responsesStreamObserver, temperature);
            thermostatSimulation.addListener(temperatureListener);
            log.info("[subscribeCurrentTemperature] Subscription started");

            // Wait until call has been cancelled by client
            while (!Context.current().isCancelled()) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            thermostatSimulation.removeListener(temperatureListener);
            log.info("[subscribeCurrentTemperature] Subscription cancelled");
            responsesStreamObserver.onCompleted();
        }

        // Helper to send back temperature
        private void sendTemperature(
                @NonNull final StreamObserver<TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses> responsesStreamObserver,
                final double temperature
        ) {
            responsesStreamObserver.onNext(
                    TemperatureControllerOuterClass.Subscribe_CurrentTemperature_Responses
                            .newBuilder()
                            .setCurrentTemperature(SiLAReal.from(temperature))
                            .build()
            );
        }
    }
}
