package sila_java.examples.test_server.utils;

import javax.annotation.concurrent.Immutable;

/**
 * Object representation of an interval between a begin and end value
 * Instance is immutable
 */
@Immutable
public class Interval {
    private final long begin;
    private final long end;

    /**
     * Create a new instance of an Interval
     * @param begin The begin interval value
     * @param end The end interval value
     */
    public Interval(final long begin, final long end) {
        if (begin > end)
            throw new IllegalArgumentException("The end of the interval cannot be before its beginning!");
        this.begin = begin;
        this.end = end;
    }

    /**
     * Get the begin interval
     * @return the begin interval
     */
    public long getBegin() {
        return (this.begin);
    }

    /**
     * Get the end interval
     * @return the end interval
     */
    public long getEnd() {
        return (this.end);
    }

    /**
     * Predicate that tell whether or not this interval is not in the specified one
     * @param interval the interval to check
     * @return wether or not this interval is not in the one specified
     */
    public boolean notIn(final Interval interval) {
        return (this.getEnd() < interval.getBegin() || interval.getEnd() < this.getBegin());
    }

    /**
     * Merge this interval and the specified one
     * @param interval the interval to merge
     * @return Return the largest interval that include both this and the specified interval
     */
    public Interval merge(final Interval interval) {
        final long mergedBegin = Math.min(this.getBegin(), interval.getBegin());
        final long mergedEnd = Math.max(this.getEnd(), interval.getEnd());
        return (new Interval(mergedBegin, mergedEnd));
    }
}