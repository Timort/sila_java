package sila_java.examples.test_server;

import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestGrpc;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static sila_java.library.core.sila.clients.ChannelUtils.waitUntilReady;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestBase {
    private static final int SERVER_PORT = 4242;
    private static final int SHUTDOWN_TIMEOUT = 5; // Timeout to wait for Test Server shut down in seconds
    private TestServer testServer;

    private ManagedChannel channel;

    BinaryUploadGrpc.BinaryUploadBlockingStub uploadBlockingStub;
    BinaryUploadGrpc.BinaryUploadStub uploadStreamStub;
    BinaryDownloadGrpc.BinaryDownloadStub downloadStreamStub;
    UnobservableCommandTestGrpc.UnobservableCommandTestBlockingStub unobservableCommandStub;

    @BeforeAll
    void setupTestServer() throws TimeoutException, ExecutionException {
        log.info("Setting up Test server on Port: " + SERVER_PORT);

        testServer = new TestServer(
                new ArgumentHelper(new String[]{"-p", Integer.toString(SERVER_PORT)}, TestServer.SERVER_TYPE)
        );

        channel = ChannelFactory.withEncryption("localhost", SERVER_PORT);

        uploadBlockingStub = BinaryUploadGrpc.newBlockingStub(channel);
        uploadStreamStub = BinaryUploadGrpc.newStub(channel);
        downloadStreamStub = BinaryDownloadGrpc.newStub(channel);
        unobservableCommandStub = UnobservableCommandTestGrpc.newBlockingStub(channel);

        // Make sure the server is up
        waitUntilReady(channel, Duration.ofMinutes(1));
    }

    @AfterAll
    void cleanup() throws InterruptedException {
        channel.shutdown().awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
        testServer.close();
    }
}
