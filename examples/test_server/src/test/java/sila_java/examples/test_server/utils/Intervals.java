package sila_java.examples.test_server.utils;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that encapsulate a list of interval
 * @see Interval
 */
public class Intervals {
    private List<Interval> intervals = new ArrayList<>();

    /**
     * Add a new interval in the list
     * The internal list is simplified if it contains sub-interval
     * @param interval the interval to add
     */
    public void add(@NonNull Interval interval) {
        final List<Interval> merge = new ArrayList<>();

        for (final Interval current : this.intervals) {
            if (current.notIn(interval)) {
                merge.add(current);
            } else {
                interval = current.merge(interval);
            }
        }
        merge.add(interval);
        this.intervals = merge;
    }

    /**
     * Clear the whole list
     */
    public void clear() {
        this.intervals.clear();
    }

    /**
     * Get the intervals
     * @return a copy of the interval list
     */
    public List<Interval> getIntervals() {
        return (new ArrayList<>(this.intervals));
    }
}