package sila_java.examples.test_server;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.examples.test_server.impl.*;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;

import static sila_java.library.core.utils.Utils.blockUntilStop;
import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * SiLA Server for test purposes
 */
@Slf4j
public class TestServer implements AutoCloseable {
    static final String SERVER_TYPE = "Test Server";
    private final SiLAServer siLAServer;
    private final ObservableCommand observableCommand = new ObservableCommand();

    public TestServer(@NonNull final ArgumentHelper argumentHelper) {
        final ServerInformation serverInfo = new ServerInformation(
                SERVER_TYPE,
                "Server for test purposes",
                "www.unitelabs.ch",
                "v0.0"
        );

        try {
            final SiLAServer.Builder builder;
            if (argumentHelper.getConfigFile().isPresent()) {
                builder = SiLAServer.Builder.withConfig(argumentHelper.getConfigFile().get(), serverInfo);
            } else {
                builder = SiLAServer.Builder.withoutConfig(serverInfo);
            }
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withDiscovery);

            if (argumentHelper.useEncryption()) {
                builder.withSelfSignedCertificate();
            }

            final BinaryDatabase binaryDatabase = builder.withBinaryTransferSupport();

            final sila_java.library.server_base.standard_features.ParameterConstraintsProvider ParameterConstraintsProvider =
                    new sila_java.library.server_base.standard_features.ParameterConstraintsProvider();

            builder.addFeature(
                    getResourceContent("features/UnobservablePropertyTest.sila.xml"),
                    new UnobservableProperty()
            ).addFeature(
                    getResourceContent("features/ObservablePropertyTest.sila.xml"),
                    new ObservableProperty()
            ).addFeature(
                    getResourceContent("features/UnobservableCommandTest.sila.xml"),
                    new UnobservableCommand(binaryDatabase)
            ).addFeature(
                    getResourceContent("features/ObservableCommandTest.sila.xml"),
                    observableCommand
            ).addFeature(
                    getResourceContent("features/ParameterConstraintsProviderTest.sila.xml"),
                    new ParameterConstraintsProvider(ParameterConstraintsProvider)
            ).addFeature(
                    getResourceContent("features/ParameterConstraintsTest.sila.xml"),
                    new ParameterConstraints()
            ).addFeature(
                    ParameterConstraintsProvider
            );

            this.siLAServer = builder.start();
        } catch (IOException | SelfSignedCertificate.CertificateGenerationException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.observableCommand.close();
        this.siLAServer.close();
    }

    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        // Start Server
        try (final TestServer server = new TestServer(argumentHelper)) {
            Runtime.getRuntime().addShutdownHook(new Thread(server::close));
            blockUntilStop();
        }
        System.out.println("Server closed.");
    }
}
