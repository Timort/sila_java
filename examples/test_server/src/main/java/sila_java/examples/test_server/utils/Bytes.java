package sila_java.examples.test_server.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class Bytes {
    /**
     * XOR encipher byte array
     * @param data The byte array to encipher
     * @param key The key to encipher the data with
     * @param off The byte array offset
     * @param len The byte array length
     */
    public static void XOREncipherBytes(byte[] data, byte key, int off, int len) {
        for (int i = off; i < len + off; i++) {
            data[i] ^= key;
        }
    }
}
