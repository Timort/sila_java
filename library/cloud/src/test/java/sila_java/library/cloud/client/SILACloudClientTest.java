package sila_java.library.cloud.client;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

@Disabled
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SILACloudClientTest {

    @Test
    void connect() throws IOException {
        SILACloudClient cloudClient = new SILACloudClient.Builder().start();


    }
}