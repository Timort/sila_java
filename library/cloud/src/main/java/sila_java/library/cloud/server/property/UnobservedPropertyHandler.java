package sila_java.library.cloud.server.property;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * The unobserved property handler interface to handle unobserved property requests.
 *
 * @author markus.meser@siobra.de
 */
public interface UnobservedPropertyHandler {

    /**
     * Handles unobserved property requests.
     *
     * @param parameters the property parameters as {@link ByteString}
     * @return the execution result as {@link ByteString}
     * @throws InvalidProtocolBufferException thrown if the parameters could not be parsed
     */
    ByteString handlePropertyRead(ByteString parameters) throws InvalidProtocolBufferException;
}
