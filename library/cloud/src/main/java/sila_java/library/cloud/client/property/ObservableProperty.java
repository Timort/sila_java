package sila_java.library.cloud.client.property;

import com.google.protobuf.InvalidProtocolBufferException;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

public interface ObservableProperty {

    /**
     * Returns this property unique id
     *
     * @return this property unique id
     */
    String getPropertyId();

    String getRequestUUID();

    /**
     * Handles property responses
     *
     * @param requestUUID id used to map response(s) to request
     * @param response    the property response to be handled
     * @throws InvalidProtocolBufferException thrown if incoming response can not be parsed
     */
    void handlePropertyResponse(String requestUUID, SiLACloudConnector.ObservablePropertyValue response) throws InvalidProtocolBufferException;

    /**
     * Handles property error
     *
     * @param error the property error to handled
     */
    void handlePropertyError(SiLAFramework.SiLAError error);
}
