package sila_java.library.cloud.server.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * The unobserved command handler interface to handle unobserved command requests.
 *
 * @author markus.meser@siobra.de
 */
public interface UnobservedCommandHandler {

    /**
     * Handles unobserved command requests.
     *
     * @param parameters the command parameters as {@link ByteString}
     * @return the execution result as {@link ByteString}
     * @throws InvalidProtocolBufferException thrown if the parameters could not be parsed
     */
    ByteString handleCommandRequest(ByteString parameters) throws InvalidProtocolBufferException;
}
