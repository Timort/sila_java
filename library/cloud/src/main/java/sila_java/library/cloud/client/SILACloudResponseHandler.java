package sila_java.library.cloud.client;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.cloud.client.command.CommandExecutionService;
import sila_java.library.cloud.client.command.ObservableCommandExecutionService;
import sila_java.library.cloud.client.property.ObservablePropertyService;
import sila_java.library.cloud.client.property.UnobservablePropertyService;

@Slf4j
public class SILACloudResponseHandler {

    private final CommandExecutionService commandExecutionService;
    private final ObservableCommandExecutionService observableCommandExecutionService;
    private final ObservablePropertyService observablePropertyService;
    private final UnobservablePropertyService unobservablePropertyService;

    @Builder
    public SILACloudResponseHandler(CommandExecutionService commandExecutionService, ObservableCommandExecutionService observableCommandExecutionService, ObservablePropertyService observablePropertyService, UnobservablePropertyService unobservablePropertyService) {
        this.commandExecutionService = commandExecutionService;
        this.observableCommandExecutionService = observableCommandExecutionService;
        this.observablePropertyService = observablePropertyService;
        this.unobservablePropertyService = unobservablePropertyService;
    }


    public void handleResponse(SiLACloudConnector.SILAServerMessage serverMessage) {
        String requestUUID = serverMessage.getRequestUUID();
        switch (serverMessage.getMessageCase()) {
            case COMMANDRESPONSE:
                log.info("COMMANDRESPONSE.");
                commandExecutionService.onCommandResponse(requestUUID, serverMessage.getCommandResponse());
                break;
            case OBSERVABLECOMMANDCONFIRMATION:
                log.info("OBSERVABLECOMMANDCONFIRMATION.");
                observableCommandExecutionService.onCommandConfirmationResponse(requestUUID, serverMessage.getObservableCommandConfirmation());
                break;
            case OBSERVABLECOMMANDEXECUTIONINFO:
                log.info("OBSERVABLECOMMANDEXECUTIONINFO.");
                observableCommandExecutionService.onCommandExecutionInfoResponse(requestUUID, serverMessage.getObservableCommandExecutionInfo());
                break;
            case OBSERVABLECOMMANDINTERMEDIATERESPONSE:
                log.info("OBSERVABLECOMMANDINTERMEDIATERESPONSE.");
                observableCommandExecutionService.onCommandIntermediateResponse(requestUUID, serverMessage.getObservableCommandIntermediateResponse());
                break;
            case OBSERVABLECOMMANDRESPONSE:
                log.info("OBSERVABLECOMMANDRESPONSE.");
                observableCommandExecutionService.onCommandResponse(requestUUID, serverMessage.getObservableCommandResponse());
                break;
            case METADATARESPONSE:
                log.info("METADATARESPONSE");
                break;
            case PROPERTYVALUE:
                log.info("PROPERTYVALUE");
                unobservablePropertyService.onPropertyResponse(requestUUID, serverMessage.getPropertyValue());
                break;
            case OBSERVABLEPROPERTYVALUE:
                log.info("OBSERVABLEPROPERTYVALUE");
                observablePropertyService.onObservablePropertyResponse(requestUUID, serverMessage.getObservablePropertyValue());
                break;
            case COMMANDERROR:
                log.info("COMMANDERROR");
                commandExecutionService.onError(requestUUID, serverMessage.getCommandError());
                observableCommandExecutionService.onError(requestUUID, serverMessage.getCommandError());
                break;
            case PROPERTYERROR:
                log.info("PROPERTYERROR");
                unobservablePropertyService.onError(requestUUID, serverMessage.getPropertyError());
                observablePropertyService.onError(requestUUID, serverMessage.getPropertyError());
                break;
            case MESSAGE_NOT_SET:
                log.info("MESSAGE_NOT_SET");
                break;
            default:
                log.info("default");
                break;
        }
        //OK now we know the message type. next
        //  map the message content to the right payload type!

        log.info("next. Received serverMessage. commandExecutionUUID: {}", requestUUID);
        // do anything. e.g. server connected, so respond with clientMessage
    }
}
