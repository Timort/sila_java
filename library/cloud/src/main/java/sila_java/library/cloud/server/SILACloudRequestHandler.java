package sila_java.library.cloud.server;

import io.grpc.stub.StreamObserver;
import lombok.Builder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.cloud.server.command.CommandExecutionService;
import sila_java.library.cloud.server.command.ObservableCommandExecutionService;
import sila_java.library.cloud.server.property.ObservablePropertyService;
import sila_java.library.cloud.server.property.UnobservablePropertyService;

@Slf4j
public class SILACloudRequestHandler {

    private final StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream;

    private final CommandExecutionService commandExecutionService;
    private final ObservableCommandExecutionService observableCommandExecutionService;
    private final ObservablePropertyService observablePropertyService;
    private final UnobservablePropertyService unobservablePropertyService;

    @Builder
    public SILACloudRequestHandler(StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream, CommandExecutionService commandExecutionService, ObservableCommandExecutionService observableCommandExecutionService, ObservablePropertyService observablePropertyService, UnobservablePropertyService unobservablePropertyService) {
        this.serverMessageStream = serverMessageStream;
        this.commandExecutionService = commandExecutionService;
        this.observableCommandExecutionService = observableCommandExecutionService;
        this.observablePropertyService = observablePropertyService;
        this.unobservablePropertyService = unobservablePropertyService;
    }

    @SneakyThrows
    public void handleResponse(SiLACloudConnector.SILAClientMessage clientMessage) {
        SiLACloudConnector.SILAServerMessage.Builder reply = SiLACloudConnector.SILAServerMessage.newBuilder();
        String requestUUID = clientMessage.getRequestUUID();
        reply.setRequestUUID(requestUUID);
        log.debug("onNext, message {}", clientMessage.getMessageCase());
        switch (clientMessage.getMessageCase()) {
            case COMMANDEXECUTION:
                SiLACloudConnector.CommandExecution commandRequest = clientMessage.getCommandExecution();
                SiLACloudConnector.SILAServerMessage response = commandExecutionService.executeCommand(commandRequest);
                if (response.hasCommandError()) {
                    reply.setCommandError(response.getCommandError());
                } else {
                    reply.setCommandResponse(response.getCommandResponse());
                }
                serverMessageStream.onNext(reply.build());
                break;
            case COMMANDINITIATION:
                SiLACloudConnector.CommandInitiation observableCommandRequest = clientMessage.getCommandInitiation();

                SiLACloudConnector.SILAServerMessage observableCommandResponse = observableCommandExecutionService.executeCommand(observableCommandRequest);
                if (observableCommandResponse.hasCommandError()) {
                    reply.setCommandError(observableCommandResponse.getCommandError());
                } else {
                    reply.setObservableCommandConfirmation(observableCommandResponse.getObservableCommandConfirmation());
                }
                serverMessageStream.onNext(reply.build());
                break;
            case COMMANDEXECUTIONINFOSUBSCRIPTION:
                SiLACloudConnector.CommandExecutionInfoSubscription infoRequest = clientMessage.getCommandExecutionInfoSubscription();
                observableCommandExecutionService.subscribeCommandInfo(infoRequest, requestUUID, serverMessageStream);
                break;
            case COMMANDINTERMEDIATERESPONSESUBSCRIPTION:
                SiLACloudConnector.CommandIntermediateResponseSubscription request = clientMessage.getCommandIntermediateResponseSubscription();
                observableCommandExecutionService.subscribeCommandIntermediateResultRequest(request, requestUUID, serverMessageStream);
                break;
            case COMMANDGETRESPONSE:
                SiLACloudConnector.CommandGetResponse resultRequest = clientMessage.getCommandGetResponse();
                reply.setObservableCommandResponse(observableCommandExecutionService.executeCommandResult(resultRequest));
                serverMessageStream.onNext(reply.build());
                break;
            case METADATAREQUEST:
                SiLACloudConnector.GetFCPAffectedByMetadataRequest metadataRequest = clientMessage.getMetadataRequest();
                reply.setMetadataResponse(commandExecutionService.executeMetadataCommand(metadataRequest));
                break;
            case PROPERTYREAD:
                SiLACloudConnector.PropertyRead propertyRequest = clientMessage.getPropertyRead();
                SiLACloudConnector.SILAServerMessage propertyResponse = unobservablePropertyService.executeProperty(requestUUID, propertyRequest);
                if (propertyResponse.hasPropertyError()) {
                    reply.setPropertyError(propertyResponse.getPropertyError());
                } else {
                    reply.setPropertyValue(propertyResponse.getPropertyValue());
                }
                serverMessageStream.onNext(reply.build());
                break;
            case PROPERTYSUBSCRIPTION:
                SiLACloudConnector.PropertySubscription observablePropertyRequest = clientMessage.getPropertySubscription();
                observablePropertyService.subscribeProperty(requestUUID, observablePropertyRequest, serverMessageStream);
                break;
            case CANCELCOMMANDEXECUTIONINFOSUBSCRIPTION:
                observableCommandExecutionService.cancelExecutionInfoSubscription(requestUUID);
                break;
            case CANCELCOMMANDINTERMEDIATERESPONSESUBSCRIPTION:
                observableCommandExecutionService.cancelIntermediateResponseSubscription(requestUUID);
                break;
            case CANCELPROPERTYSUBSCRIPTION:
                observablePropertyService.cancelPropertySubscription(requestUUID);
                break;
            case MESSAGE_NOT_SET:
                break;
        }
    }

}
