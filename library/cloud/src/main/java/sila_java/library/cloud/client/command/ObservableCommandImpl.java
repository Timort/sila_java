package sila_java.library.cloud.client.command;

import com.google.protobuf.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.SiLAFramework.CommandConfirmation;
import sila2.org.silastandard.SiLAFramework.ExecutionInfo;

import java.util.function.Consumer;

/**
 * The base class for observed commands.
 *
 * @param <ParamType>  the parametrized command parameter type
 * @param <ResultType> the parametrized command result type
 */
@Getter
@RequiredArgsConstructor
@AllArgsConstructor
public abstract class ObservableCommandImpl<ParamType extends GeneratedMessageV3, ResultType extends MessageLite> implements ObservableCommand {

    private final String commandId;
    private final ParamType parameters;
    private CommandConfirmation confirmation;

    private final Consumer<CommandConfirmation> confirmationCallback;
    private final Consumer<ExecutionInfo> executionInfoCallback;
    private final Consumer<ResultType> intermediateResultCallback;
    private final Consumer<ResultType> resultCallback;
    private final Consumer<SiLAFramework.SiLAError> errorCallback;
    private final Parser<ResultType> parser;


    private String executionInfoSubscriptionId;
    private String intermediateResponseSubscriptionId;

    @Override
    public ByteString getPayload() {
        return parameters.toByteString();
    }

    @Override
    public SiLAFramework.CommandExecutionUUID getCommandExecutionUUID() {
        return confirmation.getCommandExecutionUUID();
    }

    @Override
    public void handleCommandConfirmation(CommandConfirmation confirmation) {

        this.confirmation = confirmation;
        onCommandConfirmation(confirmation);

        if (null != confirmationCallback) {
            confirmationCallback.accept(confirmation);
        }
    }

    @Override
    public void handleCommandExecutionInfo(String requestUUID, SiLACloudConnector.ObservableCommandExecutionInfo executionInfo) {
        this.executionInfoSubscriptionId = requestUUID;

        onCommandExecutionInfo(executionInfo.getExecutionInfo());

        if (null != executionInfoCallback) {
            executionInfoCallback.accept(executionInfo.getExecutionInfo());
        }
    }

    @Override
    public void handleCommandIntermediateResponse(String requestUUID, SiLACloudConnector.ObservableCommandIntermediateResponse response) throws InvalidProtocolBufferException {
        this.intermediateResponseSubscriptionId = requestUUID;

        ResultType responses = parser.parseFrom(response.getResult());
        onIntermediateResponse(responses);

        if (null != intermediateResultCallback) {
            intermediateResultCallback.accept(responses);
        }
    }

    @Override
    public void handleCommandResponse(SiLACloudConnector.ObservableCommandResponse response) throws InvalidProtocolBufferException {

        ResultType responses = parser.parseFrom(response.getResult());
        onResponse(responses);

        if (null != resultCallback) {
            resultCallback.accept(responses);
        }

    }

    @Override
    public void handleCommandError(SiLAFramework.SiLAError error) {
        onError(error);

        if (null != errorCallback) {
            errorCallback.accept(error);
        }
    }

    public abstract void onCommandConfirmation(CommandConfirmation confirmation);

    public abstract void onCommandExecutionInfo(ExecutionInfo executionInfo);

    public abstract void onIntermediateResponse(ResultType responses);

    public abstract void onResponse(ResultType responses);

    public abstract void onError(SiLAFramework.SiLAError error);

}
