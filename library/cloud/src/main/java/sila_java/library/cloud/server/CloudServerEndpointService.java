package sila_java.library.cloud.server;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.cloud.server.command.CommandExecutionService;
import sila_java.library.cloud.server.command.ObservableCommandExecutionService;
import sila_java.library.cloud.server.property.ObservablePropertyService;
import sila_java.library.cloud.server.property.UnobservablePropertyService;

@Slf4j
class CloudServerEndpointService {
    private StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream;

    private final CommandExecutionService commandExecutionService;
    private final ObservableCommandExecutionService observableCommandExecutionService;
    private final ObservablePropertyService observablePropertyService;
    private final UnobservablePropertyService unobservablePropertyService;
    private SILACloudRequestHandler responseHandler;

    public CloudServerEndpointService(CommandExecutionService commandExecutionService, ObservableCommandExecutionService observableCommandExecutionService, ObservablePropertyService observablePropertyService, UnobservablePropertyService unobservablePropertyService) {
        this.commandExecutionService = commandExecutionService;
        this.observableCommandExecutionService = observableCommandExecutionService;
        this.observablePropertyService = observablePropertyService;
        this.unobservablePropertyService = unobservablePropertyService;
    }

    void stop() {
        serverMessageStream.onCompleted();
    }

    void start(CloudClientEndpointGrpc.CloudClientEndpointStub cloudConnection) {
        StreamObserver<SiLACloudConnector.SILAClientMessage> responseObserver = new StreamObserver<SiLACloudConnector.SILAClientMessage>() {
            @SneakyThrows
            @Override
            public void onNext(SiLACloudConnector.SILAClientMessage clientMessage) {
                responseHandler.handleResponse(clientMessage);
            }

            @Override
            public void onError(Throwable throwable) {
                log.warn("error", throwable);
                if (throwable instanceof StatusRuntimeException) {
                    // FIXME try reconnect
                }

                SiLAFramework.SiLAError siLAError = SiLAFramework.SiLAError.newBuilder()
                        .setFrameworkError(SiLAFramework.FrameworkError.newBuilder().setMessage(throwable.getMessage()))
                        .build();
                log.warn("onError: {}", siLAError);

                //   cloudConnection.connectSILAServer(responseObserver);

            }

            @Override
            public void onCompleted() {
                log.info("onCompleted");
            }
        };

        this.serverMessageStream = cloudConnection.connectSILAServer(responseObserver);
        this.responseHandler = SILACloudRequestHandler.builder()
                .serverMessageStream(serverMessageStream)
                .commandExecutionService(commandExecutionService)
                .observableCommandExecutionService(observableCommandExecutionService)
                .unobservablePropertyService(unobservablePropertyService)
                .observablePropertyService(observablePropertyService)
                .build();
    }

}
