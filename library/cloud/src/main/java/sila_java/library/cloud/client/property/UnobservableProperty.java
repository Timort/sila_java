package sila_java.library.cloud.client.property;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

public interface UnobservableProperty {

    /**
     * Returns this property unique id
     *
     * @return this property unique id
     */
    String getPropertyId();

    /**
     * Returns this property payload (parameters)
     *
     * @return this property payload
     */
    ByteString getPayload();

    /**
     * Handles property responses
     *
     * @param response the property response to be handled
     * @throws InvalidProtocolBufferException thrown if incoming response can not be parsed
     */
    void handlePropertyResponse(SiLACloudConnector.PropertyValue response) throws InvalidProtocolBufferException;

    /**
     * Handles property error
     *
     * @param error the property error to handled
     */
    void handlePropertyError(SiLAFramework.SiLAError error);



}
