package sila_java.library.cloud.server.property;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.Map;

/**
 * read unobservable properties on server
 */
@Slf4j
public class UnobservablePropertyService {
    final Map<String, UnobservedPropertyHandler> propertyHandlers;

    public UnobservablePropertyService(Map<String, UnobservedPropertyHandler> propertyHandlers) {
        this.propertyHandlers = propertyHandlers;
    }

    public SiLACloudConnector.SILAServerMessage executeProperty(String requestUUID, SiLACloudConnector.PropertyRead propertyRequest) throws InvalidProtocolBufferException {
        log.info("Received clientMessage requestUUID:{}", requestUUID);
        log.info("Received clientMessage FullyQualifiedPropertyId:{}", propertyRequest.getFullyQualifiedPropertyId());

        String commandId = propertyRequest.getFullyQualifiedPropertyId();

        UnobservedPropertyHandler handler = propertyHandlers.get(commandId);
        if (null == handler) {
            throw new IllegalArgumentException("Unsupported property:" + commandId);
        }

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        try {
            ByteString commandResult = handler.handlePropertyRead(ByteString.EMPTY);

            SiLACloudConnector.PropertyValue response = SiLACloudConnector.PropertyValue.newBuilder()
                    .setResult(commandResult)
                    .build();
            return serverMessageBuilder
                    .setRequestUUID(requestUUID)
                    .setPropertyValue(response)
                    .build();
        } catch (StatusRuntimeException e) {
            return serverMessageBuilder
                    .setRequestUUID(requestUUID)
                    .setPropertyError(SiLAErrors.retrieveSiLAError(e).orElse(null))
                    .build();
        }
    }
}
