package sila_java.library.cloud.client.property;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class ObservablePropertyService {
    private final StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream;
    private final ConcurrentHashMap<String, ObservableProperty> subscriptions = new ConcurrentHashMap<>();

    public ObservablePropertyService(StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream) {
        this.clientMessageStream = clientMessageStream;
    }

    public void subscribeProperty(ObservableProperty property) {
        String requestUUID = UUID.randomUUID().toString();

        validatePropertyId(property.getPropertyId());

        SiLACloudConnector.PropertySubscription commandRequest = SiLACloudConnector.PropertySubscription.newBuilder()
                .setFullyQualifiedPropertyId(property.getPropertyId())
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setPropertySubscription(commandRequest)
                .build();

        subscriptions.put(requestUUID, property);
        send(clientMessage);

        log.info("Property subscription request sent. property:{} requestUUID:{} #executions:{}", property.getPropertyId(), requestUUID, subscriptions.size());
    }

    public void onObservablePropertyResponse(String requestUUID, SiLACloudConnector.ObservablePropertyValue response) {
        log.info("observablePropertyResponse. requestUUID: {}", requestUUID);
        ObservableProperty execution = subscriptions.get(requestUUID);
        if (null == execution) {
            log.warn("Received response for a property that is not available: {}", requestUUID);
            return;
        }
        try {
            execution.handlePropertyResponse(requestUUID, response);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }

    private void validatePropertyId(String propertyId) {
        if (StringUtils.isEmpty(propertyId)) throw new IllegalArgumentException("Empty property id: " + propertyId);
    }

    private void send(SiLACloudConnector.SILAClientMessage clientMessage) {
        clientMessageStream.onNext(clientMessage);
    }

    public void cancelSubscription(String subscriptionId) {
        log.info("cancelSubscription {}", subscriptionId);
        SiLACloudConnector.CancelPropertySubscription cancelRequest = SiLACloudConnector.CancelPropertySubscription.newBuilder()
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(subscriptionId)
                .setCancelPropertySubscription(cancelRequest)
                .build();

        subscriptions.remove(subscriptionId);
        send(clientMessage);

        log.info("Property subscription cancel request sent. request:{} #executions:{}", clientMessage, subscriptions.size());
    }

    public void onError(String requestUUID, SiLAFramework.SiLAError propertyError) {
        log.info("onError: {}", propertyError);

        ObservableProperty execution = subscriptions.remove(requestUUID);
        onError(requestUUID, execution, propertyError);
    }
    private void onError(String requestUUID, ObservableProperty property, SiLAFramework.SiLAError error) {

        if (null == property) {
            log.warn("Received response for an property that is not available. requestUUID: {}", requestUUID);
            return;
        }

        property.handlePropertyError(error);

    }
}
