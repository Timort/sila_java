package sila_java.library.cloud.server.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import sila2.org.silastandard.SiLAFramework.CommandConfirmation;
import sila2.org.silastandard.SiLAFramework.CommandExecutionUUID;
import sila2.org.silastandard.SiLAFramework.ExecutionInfo;

import java.util.function.Consumer;

/**
 * The observed command handler interface to handle observed command requests.
 *
 * @author markus.meser@siobra.de
 */
public interface ObservedCommandHandler {

    /**
     * Handles observed command initiation requests.
     *
     * @param parameters the command parameters as {@link ByteString}
     * @return the command confirmation as {@link CommandConfirmation}
     * @throws InvalidProtocolBufferException thrown if the parameters could not be parsed
     */
    CommandConfirmation handleCommandInitiation(ByteString parameters) throws InvalidProtocolBufferException;

    /**
     * Provides command info, see {@link ExecutionInfo}.
     *
     * @param executionUUID the command execution UUID as {@link CommandExecutionUUID}
     * @param infoConsumer  consumes command execution info
     */
    void handleCommandInfo(CommandExecutionUUID executionUUID, Consumer<ExecutionInfo> infoConsumer);

    /**
     * Provides intermediate command result.
     *
     * @param executionUUID              the command execution UUID as {@link CommandExecutionUUID}
     * @param intermediateResultConsumer consumes intermediate command results
     */
    void handleCommandIntermediateResult(CommandExecutionUUID executionUUID, Consumer<ByteString> intermediateResultConsumer);

    /**
     * Provides command result.
     *
     * @param executionUUID the command execution UUID as {@link CommandExecutionUUID}
     * @throws InvalidProtocolBufferException thrown if the result could not be parsed
     */
    ByteString handleCommandResult(CommandExecutionUUID executionUUID) throws InvalidProtocolBufferException;

    /**
     * Cancels subscription to execution info.
     */
    void handleCommandExecutionInfoCancel();

    /**
     * Cancels subscription to intermediate responses.
     */
    void handleCommandIntermediateResponseCancel();

}
