package sila_java.library.cloud.server;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila_java.library.cloud.client.SILACloudClient;
import sila_java.library.cloud.server.command.CommandExecutionService;
import sila_java.library.cloud.server.command.ObservableCommandExecutionService;
import sila_java.library.cloud.server.command.ObservedCommandHandler;
import sila_java.library.cloud.server.command.UnobservedCommandHandler;
import sila_java.library.cloud.server.property.ObservablePropertyService;
import sila_java.library.cloud.server.property.ObservedPropertyHandler;
import sila_java.library.cloud.server.property.UnobservablePropertyService;
import sila_java.library.cloud.server.property.UnobservedPropertyHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * connects the server to a cloud endpoint
 */
public class SILACloudServer {
    final ManagedChannel channel;
    private final CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private final CloudServerEndpointService cloudServerEndpointService;

    public SILACloudServer(Builder builder) {
        this.channel = ManagedChannelBuilder
                .forAddress(builder.host, builder.port)
                //what is that?
                .usePlaintext()
                .enableRetry()
                .build();

        this.clientEndpoint = CloudClientEndpointGrpc.newStub(channel);
        this.cloudServerEndpointService = new CloudServerEndpointService(new CommandExecutionService(builder.commandHandlers), new ObservableCommandExecutionService(builder.observedCommandHandlers), new ObservablePropertyService(builder.observedPropertyHandlers), new UnobservablePropertyService(builder.unobservedPropertyHandlers));

    }

    public void start() {
        cloudServerEndpointService.start(getClientStub());
    }

    public CloudClientEndpointGrpc.CloudClientEndpointStub getClientStub() {
        return clientEndpoint;
    }

    public void stop() {
        cloudServerEndpointService.stop();

    }

    public static class Builder {
        private Integer port = SILACloudClient.SILA_CLOUD_DEFAULT_PORT;

        private String host;
        private final Map<String, UnobservedCommandHandler> commandHandlers = new HashMap<>();
        private final Map<String, ObservedCommandHandler> observedCommandHandlers = new HashMap<>();
        private final Map<String, ObservedPropertyHandler> observedPropertyHandlers = new HashMap<>();
        private final Map<String, UnobservedPropertyHandler> unobservedPropertyHandlers = new HashMap<>();

        /**
         * Define Specific Port for the services
         *
         * @param port Port on which the gRCP service runs.
         */
        public SILACloudServer.Builder withPort(int port) {
            this.port = port;
            return this;
        }

        public SILACloudServer.Builder withHost(String host) {
            this.host = host;
            return this;
        }

        public SILACloudServer.Builder withCommandHandler(String commandId, UnobservedCommandHandler commandHandler) {
            commandHandlers.put(commandId, commandHandler);
            return this;
        }

        public SILACloudServer.Builder withCommandHandler(String commandId, ObservedCommandHandler commandHandler) {
            observedCommandHandlers.put(commandId, commandHandler);
            return this;
        }

        public SILACloudServer.Builder withPropertyHandler(String propertyId, ObservedPropertyHandler propertyHandler) {
            observedPropertyHandlers.put(propertyId, propertyHandler);
            return this;
        }

        public SILACloudServer.Builder withPropertyHandler(String propertyId, UnobservedPropertyHandler propertyHandler) {
            unobservedPropertyHandlers.put(propertyId, propertyHandler);
            return this;
        }

        public SILACloudServer build() {
            return new SILACloudServer(this);
        }

    }
}
