package sila_java.library.cloud.server.property;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.HashMap;
import java.util.Map;

/**
 * handles observable properties on server.
 */
@Slf4j
public class ObservablePropertyService {

    private final Map<String, ObservedPropertyHandler> propertyHandlers;
    private final Map<String, ObservedPropertyHandler> subscriptions;

    public ObservablePropertyService(Map<String, ObservedPropertyHandler> propertyHandlers) {
        this.propertyHandlers = propertyHandlers;
        this.subscriptions = new HashMap<>();
    }

    public void subscribeProperty(String requestUUID, SiLACloudConnector.PropertySubscription request, StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {

        log.info("Received clientMessage requestUUID:{}", requestUUID);
        log.info("Received clientMessage getFullyQualifiedPropertyId:{}", request.getFullyQualifiedPropertyId());
        String commandId = request.getFullyQualifiedPropertyId();

        ObservedPropertyHandler handler = propertyHandlers.get(commandId);
        if (null == handler) {
            throw new IllegalArgumentException("Unsupported property:" + commandId);
        }

        try {
            handler.handlePropertySubscription(request.toByteString(), result -> {
                log.info("handler got propertyResult {}", result);

                SiLACloudConnector.ObservablePropertyValue.Builder response = SiLACloudConnector.ObservablePropertyValue.newBuilder()
                        .setResult(result);

                SiLACloudConnector.SILAServerMessage serverMessage = SiLACloudConnector.SILAServerMessage.newBuilder()
                        .setRequestUUID(requestUUID)
                        .setObservablePropertyValue(response)
                        .build();

                serverMessageStream.onNext(serverMessage);
            });

            subscriptions.put(requestUUID, handler);

        } catch (StatusRuntimeException e) {
            SiLACloudConnector.SILAServerMessage message = SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setRequestUUID(requestUUID)
                    .setPropertyError(SiLAErrors.retrieveSiLAError(e).orElse(null))
                    .build();
            serverMessageStream.onNext(message);
            throw e;
        }
    }

    public void cancelPropertySubscription(String requestUUID) {
        log.info("cancelPropertySubscription {}", requestUUID);
        ObservedPropertyHandler handler = subscriptions.remove(requestUUID);
        handler.cancelSubscription(requestUUID);
    }

}
