package sila_java.library.cloud.client.property;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class UnobservablePropertyService {

    private final StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream;
    private final ConcurrentHashMap<String, UnobservableProperty> properties = new ConcurrentHashMap<>();

    public UnobservablePropertyService(StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream) {
        this.clientMessageStream = clientMessageStream;
    }

    public void readProperty(UnobservableProperty command) {

        String requestUUID = UUID.randomUUID().toString();
        validateCommandId(command.getPropertyId());

        SiLACloudConnector.PropertyRead  propertyRequest = SiLACloudConnector.PropertyRead.newBuilder()
                .setFullyQualifiedPropertyId(command.getPropertyId())
                //.setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder()
                //        .setParameters(command.getPayload()))
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setPropertyRead(propertyRequest)
                .build();

        properties.put(requestUUID, command);
        send(clientMessage);

        log.info("Command request sent. command:{} requestUUID:{} #executions:{}", command.getPropertyId(), requestUUID, properties.size());
    }

    public void onPropertyResponse(String requestUUID, SiLACloudConnector.PropertyValue commandResponse) {

        log.info("commandResponse. requestUUID: {}", requestUUID);

        onResponse(requestUUID, commandResponse);
    }

    private void validateCommandId(String commandId) {
        if (StringUtils.isEmpty(commandId))
            throw new IllegalArgumentException("Empty command id: " + commandId);
    }

    private void send(SiLACloudConnector.SILAClientMessage clientMessage) {
        clientMessageStream.onNext(clientMessage);
    }

    public void onError(String requestUUID, SiLAFramework.SiLAError propertyError) {
        log.info("onError: {}", propertyError);

        UnobservableProperty execution = properties.remove(requestUUID);
        onError(requestUUID, execution, propertyError);
    }

    public void onCompleted() {
        log.info("onCompleted. ");
        properties.clear();
    }

    private void onResponse(String requestUUID, SiLACloudConnector.PropertyValue response) {
        log.info("propertyResponse. requestUUID: {}", requestUUID);
        UnobservableProperty execution = properties.remove(requestUUID);

        if (null == execution) {
            log.warn("Received response for an execution that is not available: {}", requestUUID);
            return;
        }
        try {
            execution.handlePropertyResponse(response);
        } catch (Exception ex) {
            log.error("error handling response {}", ex.getMessage(), ex);
        }

    }

    private void onError(String requestUUID, UnobservableProperty property, SiLAFramework.SiLAError error) {

        if (null == property) {
            log.warn("Received response for an property that is not available. requestUUID: {}", requestUUID);
            return;
        }

        property.handlePropertyError(error);

    }


}
