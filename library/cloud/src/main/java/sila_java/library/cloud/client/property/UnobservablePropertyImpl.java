package sila_java.library.cloud.client.property;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

import java.util.function.Consumer;

@Getter
@AllArgsConstructor
public abstract class UnobservablePropertyImpl<ParamType extends GeneratedMessageV3, ResultType extends MessageLite> implements UnobservableProperty {

    private final String propertyId;
    private final ParamType parameters;
    private final Consumer<ResultType> resultCallback;
    private final Parser<ResultType> parser;
    private final Consumer<SiLAFramework.SiLAError> errorCallback;

    public abstract void onResponse(ResultType responses);

    public abstract void onError(SiLAFramework.SiLAError silaError);

    public void handlePropertyResponse(SiLACloudConnector.PropertyValue response) throws InvalidProtocolBufferException {

        ResultType responses = parser.parseFrom(response.getResult());
        onResponse(responses);

        if (null != resultCallback) {
            resultCallback.accept(responses);
        }
    }

    @Override
    public void handlePropertyError(SiLAFramework.SiLAError error) {
        onError(error);
        if (errorCallback != null) {
            errorCallback.accept(error);
        }
    }
}
