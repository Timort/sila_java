package sila_java.library.cloud.client.property;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

import java.util.function.Consumer;

@Slf4j
@Getter
@RequiredArgsConstructor
public abstract class ObservablePropertyImpl<ParamType extends GeneratedMessageV3, ResultType extends MessageLite> implements ObservableProperty {

    private final String propertyId;
    private final ParamType parameters;
    private final Consumer<ResultType> resultCallback;
    private final Consumer<SiLAFramework.SiLAError> errorCallback;
    private final Parser<ResultType> parser;
    private String requestUUID;


    public abstract void onResponse(ResultType responses);

    public abstract void onError(SiLAFramework.SiLAError error);

    @Override
    public void handlePropertyResponse(String requestUUID, SiLACloudConnector.ObservablePropertyValue response) throws InvalidProtocolBufferException {
        log.info("handlePropertyResponse {}", requestUUID);
        this.requestUUID = requestUUID;

        ResultType responses = parser.parseFrom(response.getResult());
        onResponse(responses);

        if (null != resultCallback) {
            resultCallback.accept(responses);
        }
    }

    @Override
    public void handlePropertyError(SiLAFramework.SiLAError error) {
        onError(error);
        if (errorCallback != null) {
            errorCallback.accept(error);
        }
    }
}
