package sila_java.library.cloud.client.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import sila2.org.silastandard.SiLACloudConnector.CommandResponse;
import sila2.org.silastandard.SiLAFramework;

/**
 * The unobserved command interface
 *
 * @author markus.meser@siobra.de
 */
public interface UnobservableCommand {

    /**
     * Returns this command unique id
     *
     * @return this command unique id
     */
    String getCommandId();

    /**
     * Returns this command payload (parameters)
     *
     * @return this command payload
     */
    ByteString getPayload();

    /**
     * Handles command responses
     *
     * @param response the command response to be handled
     * @throws InvalidProtocolBufferException thrown if incoming response can not be parsed
     */
    void handleCommandResponse(CommandResponse response) throws InvalidProtocolBufferException;

    /**
     * Handles command error
     *
     * @param error the command error to handled
     */
    void handleCommandError(SiLAFramework.SiLAError error);

}
