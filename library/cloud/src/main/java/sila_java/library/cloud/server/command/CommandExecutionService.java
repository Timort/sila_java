package sila_java.library.cloud.server.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.StatusRuntimeException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.Map;

/**
 * executes unobservable commands on server
 */
@Slf4j
public class CommandExecutionService {
    final Map<String, UnobservedCommandHandler> commandHandlers;

    public CommandExecutionService(Map<String, UnobservedCommandHandler> commandHandlers) {
        this.commandHandlers = commandHandlers;
    }

    public SiLACloudConnector.SILAServerMessage executeCommand(SiLACloudConnector.CommandExecution commandExecution) throws InvalidProtocolBufferException {

        log.info("Received clientMessage FullyQualifiedCommandId:{}", commandExecution.getFullyQualifiedCommandId());

        String commandId = commandExecution.getFullyQualifiedCommandId();

        UnobservedCommandHandler handler = commandHandlers.get(commandId);
        if (null == handler) {
            throw new IllegalArgumentException("Unsupported command:" + commandId);
        }

        SiLACloudConnector.SILAServerMessage.Builder serverMessageBuilder = SiLACloudConnector.SILAServerMessage.newBuilder();
        try {
            ByteString commandResult = handler.handleCommandRequest(commandExecution.getCommandParameter().getParameters());

            SiLACloudConnector.CommandResponse response = SiLACloudConnector.CommandResponse.newBuilder()
                    .setResult(commandResult)
                    .build();
            return serverMessageBuilder
                    .setCommandResponse(response)
                    .build();
        } catch (StatusRuntimeException e) {
            return serverMessageBuilder
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null))
                    .build();
        }
    }

    @SneakyThrows
    public SiLACloudConnector.GetFCPAffectedByMetadataResponse executeMetadataCommand(SiLACloudConnector.GetFCPAffectedByMetadataRequest metadataRequest) {
        String fullyQualifiedMetadataId = metadataRequest.getFullyQualifiedMetadataId();
        UnobservedCommandHandler commandHandler = commandHandlers.get(fullyQualifiedMetadataId);
        commandHandler.handleCommandRequest(metadataRequest.toByteString());
        return null;
    }

}
