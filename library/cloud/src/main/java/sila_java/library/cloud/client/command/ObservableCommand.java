package sila_java.library.cloud.client.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLACloudConnector.ObservableCommandIntermediateResponse;
import sila2.org.silastandard.SiLACloudConnector.ObservableCommandResponse;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.SiLAFramework.CommandConfirmation;
import sila2.org.silastandard.SiLAFramework.CommandExecutionUUID;

/**
 * The observed command interface
 *
 * @author markus.meser@siobra.de
 */
public interface ObservableCommand {

    /**
     * Returns this command unique id
     *
     * @return this command unique id
     */
    String getCommandId();

    /**
     * Returns this command execution uuid
     *
     * @return this command execution uuid
     */
    CommandExecutionUUID getCommandExecutionUUID();

    String getExecutionInfoSubscriptionId();

    String getIntermediateResponseSubscriptionId();

    /**
     * Returns this command payload (parameters)
     *
     * @return this command payload
     */
    ByteString getPayload();

    /**
     * Handles command confirmation
     *
     * @param confirmation the command confirmation to be handled
     */
    void handleCommandConfirmation(CommandConfirmation confirmation);

    /**
     * Handles command execution info
     *
     * @param requestUUID   id used to map response(s) to request
     * @param executionInfo the command execution info to be handled
     */
    void handleCommandExecutionInfo(String requestUUID, SiLACloudConnector.ObservableCommandExecutionInfo executionInfo);

    /**
     * Handles command intermediate responses
     *
     * @param requestUUID id used to map response(s) to request
     * @param response    the command intermediate response to be handled
     * @throws InvalidProtocolBufferException thrown if incoming response can not be parsed
     */
    void handleCommandIntermediateResponse(String requestUUID, ObservableCommandIntermediateResponse response) throws InvalidProtocolBufferException;

    /**
     * Handles command responses
     *
     * @param response the command response to be handled
     * @throws InvalidProtocolBufferException thrown if incoming response can not be parsed
     */
    void handleCommandResponse(ObservableCommandResponse response) throws InvalidProtocolBufferException;

    /**
     * Handles command error
     *
     * @param error the command error to be handled
     */
    void handleCommandError(SiLAFramework.SiLAError error);

}
