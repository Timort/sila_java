package sila_java.library.cloud.server.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

/**
 * The base class for unobserved command handlers.
 *
 * @param <ParamType>  the parametrized command parameter type
 * @param <ResultType> the parametrized command result type
 */
@Slf4j
public abstract class UnobservedCommandHandlerImpl<ParamType, ResultType extends MessageLite> implements UnobservedCommandHandler {

    private Parser<ParamType> parser;

    public UnobservedCommandHandlerImpl(Parser<ParamType> parser) {
        this.parser = parser;
    }

    @Override
    public ByteString handleCommandRequest(ByteString parameters) throws InvalidProtocolBufferException {

        ParamType requestParameters = parser.parseFrom(parameters);
        ResultType result = handle(requestParameters);
        return result.toByteString();

    }

    /**
     * To be implemented to handle unobserved command requests
     *
     * @param requestParameters the command request parameters
     * @return the execution result of this handle method
     */
    public abstract ResultType handle(ParamType requestParameters);

    public class CommandWrapperStreamObserver implements StreamObserver<ResultType> {

        ResultType result;

        @Override
        public void onNext(ResultType result) {
            this.result = result;
        }

        @Override
        public void onError(Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        @Override
        public void onCompleted() {

        }

        public ResultType getResult() {
            return result;
        }

    }

}
