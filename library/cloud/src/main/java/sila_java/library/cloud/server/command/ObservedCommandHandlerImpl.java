package sila_java.library.cloud.server.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;
import sila2.org.silastandard.SiLAFramework.CommandConfirmation;
import sila2.org.silastandard.SiLAFramework.CommandExecutionUUID;
import sila2.org.silastandard.SiLAFramework.ExecutionInfo;

import java.util.function.Consumer;

/**
 * The base class for observed command handlers.
 *
 * @param <ParamType>  the parametrized command parameter type
 * @param <ResultType> the parametrized command result type
 */
public abstract class ObservedCommandHandlerImpl<ParamType, ResultType extends MessageLite> implements ObservedCommandHandler {

    private final Parser<ParamType> parser;

    public ObservedCommandHandlerImpl(Parser<ParamType> parser) {
        this.parser = parser;
    }

    @Override
    public CommandConfirmation handleCommandInitiation(ByteString parameters) throws InvalidProtocolBufferException {
        ParamType requestParameters = parser.parseFrom(parameters);
        return handle(requestParameters);
    }

    @Override
    public void handleCommandInfo(CommandExecutionUUID parameters, Consumer<ExecutionInfo> infoConsumer) {
        handleInfo(parameters, infoConsumer);
    }

    @Override
    public void handleCommandIntermediateResult(CommandExecutionUUID parameters, Consumer<ByteString> intermediateConsumer) {
        handleIntermediateResult(parameters, intermediateConsumer);
    }

    @Override
    public ByteString handleCommandResult(CommandExecutionUUID parameters) {
        return handleResult(parameters).toByteString();
    }

    @Override
    public void handleCommandExecutionInfoCancel() {
        handleExecutionInfoCancel();
    }

    @Override
    public void handleCommandIntermediateResponseCancel() {
        handleIntermediateResponseCancel();
    }

    /**
     * Command initiation, must be implemented by command handler.
     *
     * @param parameters the command parameters
     * @return the confirmation as {@link CommandConfirmation}
     */
    public abstract CommandConfirmation handle(ParamType parameters);

    /**
     * Provides command infos, must be implemented by command handler.
     *
     * @param executionUUID the command execution UUID as {@link CommandExecutionUUID}
     * @param infoConsumer  consumes command execution info
     */
    public abstract void handleInfo(CommandExecutionUUID executionUUID, Consumer<ExecutionInfo> infoConsumer);

    /**
     * Provides intermediate command results, must be implemented by command handler.
     *
     * @param executionUUID              the command execution UUID as {@link CommandExecutionUUID}
     * @param intermediateResultConsumer consumes intermediate command results
     */
    public abstract void handleIntermediateResult(CommandExecutionUUID executionUUID, Consumer<ByteString> intermediateResultConsumer);

    /**
     * Provides final command result, must be implemented vy command handler.
     *
     * @param executionUUID the command execution UUID as {@link CommandExecutionUUID}
     */
    public abstract ResultType handleResult(CommandExecutionUUID executionUUID);

    /**
     * Cancels subscription to execution info.
     */
    public abstract void handleExecutionInfoCancel();

    /**
     * Cancels subscription to intermediate responses.
     */
    public abstract void handleIntermediateResponseCancel();
}
