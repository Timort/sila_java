package sila_java.library.cloud.client;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.cloud.client.command.CommandExecutionService;
import sila_java.library.cloud.client.command.ObservableCommandExecutionService;
import sila_java.library.cloud.client.property.ObservablePropertyService;
import sila_java.library.cloud.client.property.UnobservablePropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * gRPC server endpoint on the SILA Client side. Allows SILA servers to connect
 */
@Slf4j
public class CloudClientEndpointService extends CloudClientEndpointGrpc.CloudClientEndpointImplBase {

    List<CloudConnectedServer> connectedServers = new ArrayList<>();
    Consumer<CloudConnectedServer> onServerConnectListener = cloudConnectedServer -> {
        //dummy
    };


    @Override
    public StreamObserver<SiLACloudConnector.SILAServerMessage> connectSILAServer(
            StreamObserver<SiLACloudConnector.SILAClientMessage> responseObserver) {

        log.info("Cloud connection has been established. {}", responseObserver);
        CommandExecutionService commandExecutionService = new CommandExecutionService(responseObserver);
        ObservableCommandExecutionService observableCommandExecutionService = new ObservableCommandExecutionService(responseObserver);
        ObservablePropertyService observablePropertyService = new ObservablePropertyService(responseObserver);
        UnobservablePropertyService unobservablePropertyService = new UnobservablePropertyService(responseObserver);
        CloudConnectedServer newServer = new CloudConnectedServer(commandExecutionService, observableCommandExecutionService, observablePropertyService, unobservablePropertyService);

        SILACloudResponseHandler responseHandler = SILACloudResponseHandler.builder()
                .commandExecutionService(commandExecutionService)
                .observableCommandExecutionService(observableCommandExecutionService)
                .observablePropertyService(observablePropertyService)
                .unobservablePropertyService(unobservablePropertyService)
                .build();
        connectedServers.add(newServer);
        onServerConnectListener.accept(newServer);

        return new StreamObserver<SiLACloudConnector.SILAServerMessage>() {

            @Override
            public void onNext(SiLACloudConnector.SILAServerMessage serverMessage) {
                responseHandler.handleResponse(serverMessage);
            }

            @Override
            public void onError(Throwable throwable) {
                log.error("error. ", throwable);
                SiLAFramework.SiLAError siLAError = SiLAFramework.SiLAError.newBuilder()
                        .setUndefinedExecutionError(SiLAFramework.UndefinedExecutionError.newBuilder().setMessage(throwable.getMessage()))
                        .build();
                commandExecutionService.onError("TODO", siLAError);
                // FIXME error always on commandExecutionService?
            }

            @Override
            public void onCompleted() {
                log.info("completed.");
                commandExecutionService.onCompleted();
                // FIXME complete always on commandExecutionService?
            }
        };
    }

    public void setOnServerConnectListener(Consumer<CloudConnectedServer> onServerConnectListener) {
        this.onServerConnectListener = onServerConnectListener;
    }

    public List<CloudConnectedServer> getConnectedServers() {
        return connectedServers;
    }
}
