package sila_java.library.cloud.client.command;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLACloudConnector.ObservableCommandIntermediateResponse;
import sila2.org.silastandard.SiLACloudConnector.ObservableCommandResponse;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.SiLAFramework.CommandConfirmation;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO javadoc
 */
@Slf4j
public class ObservableCommandExecutionService {

    private final StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream;

    private final ConcurrentHashMap<String, ObservableCommand> executions = new ConcurrentHashMap<>();

    public ObservableCommandExecutionService(StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream) {
        this.clientMessageStream = clientMessageStream;
    }

    public void executeCommand(ObservableCommand command) {

        String requestUUID = UUID.randomUUID().toString();
        validateCommandId(command.getCommandId());

        SiLACloudConnector.CommandInitiation commandRequest = SiLACloudConnector.CommandInitiation.newBuilder()
                .setFullyQualifiedCommandId(command.getCommandId())
                .setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder()
                        .setParameters(command.getPayload()))
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setCommandInitiation(commandRequest)
                .build();

        executions.put(requestUUID, command);
        send(clientMessage);

        log.info("Command request sent. command:{} requestUUID:{} #executions:{}", command.getCommandId(), requestUUID, executions.size());
    }

    public void subscribeCommandIntermediateResult(ObservableCommand command) {

        String requestUUID = UUID.randomUUID().toString();
        validateCommandId(command.getCommandId());

        SiLACloudConnector.CommandIntermediateResponseSubscription commandRequest = SiLACloudConnector.CommandIntermediateResponseSubscription.newBuilder()
                .setExecutionUUID(command.getCommandExecutionUUID())
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setCommandIntermediateResponseSubscription(commandRequest)
                .build();

        executions.put(requestUUID, command);
        send(clientMessage);

        log.info("Command intermediate request sent. command:{} requestUUID:{} #executions:{}", command.getCommandId(), requestUUID, executions.size());

    }

    public void subscribeCommandInfo(ObservableCommand command) {

        String requestUUID = UUID.randomUUID().toString();
        validateCommandId(command.getCommandId());

        SiLACloudConnector.CommandExecutionInfoSubscription commandRequest = SiLACloudConnector.CommandExecutionInfoSubscription.newBuilder()
                .setExecutionUUID(command.getCommandExecutionUUID())
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setCommandExecutionInfoSubscription(commandRequest)
                .build();

        executions.put(requestUUID, command);
        send(clientMessage);

        log.info("Command info request sent. command:{} requestUUID:{} #executions:{}", command.getCommandId(), requestUUID, executions.size());
    }

    public void executeCommandResult(ObservableCommand command) {

        String requestUUID = UUID.randomUUID().toString();
        validateCommandId(command.getCommandId());

        SiLACloudConnector.CommandGetResponse commandRequest = SiLACloudConnector.CommandGetResponse.newBuilder()
                .setExecutionUUID(command.getCommandExecutionUUID())
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setCommandGetResponse(commandRequest)
                .build();

        executions.put(requestUUID, command);
        send(clientMessage);

        log.info("CommandResult request sent. command:{} requestUUID:{} #executions:{}", command.getCommandId(), requestUUID, executions.size());
    }

    public void cancelExecutionInfoSubscription(String subscriptionId) {
        log.info("cancelExecutionInfoSubscription {}", subscriptionId);
        SiLACloudConnector.CancelCommandExecutionInfoSubscription cancel = SiLACloudConnector.CancelCommandExecutionInfoSubscription.newBuilder()
                .build();
        SiLACloudConnector.SILAClientMessage cancelMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(subscriptionId)
                .setCancelCommandExecutionInfoSubscription(cancel)
                .build();
        send(cancelMessage);
    }

    public void cancelIntermediateResponseSubscription(String subscriptionId) {
        log.info("cancelIntermediateResponseSubscription {}", subscriptionId);
        SiLACloudConnector.CancelCommandIntermediateResponseSubscription cancel = SiLACloudConnector.CancelCommandIntermediateResponseSubscription.newBuilder()
                .build();
        SiLACloudConnector.SILAClientMessage cancelMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(subscriptionId)
                .setCancelCommandIntermediateResponseSubscription(cancel)
                .build();
        send(cancelMessage);
    }

    public void onCommandConfirmationResponse(String requestUUID, SiLACloudConnector.ObservableCommandConfirmation commandResponse) {
        log.info("commandConfirmationResponse. requestUUID: {}", requestUUID);

        getExecution(requestUUID).ifPresent(execution -> onCommandConfirmation(execution, commandResponse.getCommandConfirmation()));
    }

    public void onCommandExecutionInfoResponse(String requestUUID, SiLACloudConnector.ObservableCommandExecutionInfo commandResponse) {
        log.info("commandInfoResponse. requestUUID: {}", requestUUID);

        getExecution(requestUUID).ifPresent(execution -> onCommandInfo(requestUUID, execution, commandResponse));
    }

    public void onCommandIntermediateResponse(String requestUUID, ObservableCommandIntermediateResponse commandResponse) {
        log.info("onCommandIntermediateResponse. requestUUID: {}", requestUUID);

        getExecution(requestUUID).ifPresent(execution -> onCommandIntermediateResponse(requestUUID, execution, commandResponse));
    }

    public void onCommandResponse(String requestUUID, ObservableCommandResponse commandResponse) {
        log.info("commandResponse. requestUUID: {}", requestUUID);

        getExecution(requestUUID).ifPresent(execution -> onResponse(execution, commandResponse));
    }

    public void onError(String requestUUID, SiLAFramework.SiLAError commandError) {
        log.info("onError: {}", commandError);

        ObservableCommand execution = executions.remove(requestUUID);
        onError(execution, commandError);
    }

    public void onCompleted() {
        log.info("onCompleted.");
        executions.clear();
    }

    private void onCommandConfirmation(ObservableCommand execution, CommandConfirmation response) {
        try {
            execution.handleCommandConfirmation(response);
        } catch (Exception ex) {
            log.error("error handling confirmation {}", ex.getMessage(), ex);
        }
    }

    private void onCommandInfo(String requestUUID, ObservableCommand execution, SiLACloudConnector.ObservableCommandExecutionInfo executionInfo) {
        try {
            execution.handleCommandExecutionInfo(requestUUID, executionInfo);
        } catch (Exception ex) {
            log.error("error handling execution info {}", ex.getMessage(), ex);
        }
    }

    private void onCommandIntermediateResponse(String requestUUID, ObservableCommand execution, ObservableCommandIntermediateResponse response) {
        try {
            execution.handleCommandIntermediateResponse(requestUUID, response);
        } catch (Exception ex) {
            log.error("error handling intermediate response {}", ex.getMessage(), ex);
        }
    }

    private void onResponse(ObservableCommand execution, ObservableCommandResponse response) {
        try {
            execution.handleCommandResponse(response);
        } catch (Exception ex) {
            log.error("error handling response {}", ex.getMessage(), ex);
        }
    }

    private void onError(ObservableCommand execution, SiLAFramework.SiLAError error) {

        if (null == execution) {
            log.warn("Received error for an execution that is not available: {}", error);
            return;
        }

        execution.handleCommandError(error);

    }

    private void validateCommandId(String commandId) {
        if (StringUtils.isEmpty(commandId))
            throw new IllegalArgumentException("Empty command id: " + commandId);
    }

    private void send(SiLACloudConnector.SILAClientMessage clientMessage) {
        clientMessageStream.onNext(clientMessage);
    }

    private Optional<ObservableCommand> getExecution(String requestUUID) {

        Optional<ObservableCommand> commandExecution = Optional.ofNullable(executions.get(requestUUID));
        if (!commandExecution.isPresent()) {
            log.warn("Received response for an execution that is not available: {}", requestUUID);
        }
        return commandExecution;
    }

}
