package sila_java.library.cloud.server.property;

import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import com.google.protobuf.Parser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;

@Slf4j
public abstract class ObservedPropertyHandlerImpl<ParamType extends MessageLite, ResultType extends MessageLite> implements ObservedPropertyHandler {

    private final Parser<ParamType> parser;

    public ObservedPropertyHandlerImpl(Parser<ParamType> parser) {
        this.parser = parser;
    }

    @SneakyThrows
    @Override
    public void handlePropertySubscription(ByteString parameters, Consumer<ByteString> consumer) {
        ParamType paramType = parser.parseFrom(parameters);
        handleSubscription(paramType, consumer);
    }

    @Override
    public void cancelSubscription(String requestUUID) {
        log.info("cancelSubscription {}", requestUUID);
        handleCancelSubscription(requestUUID);
    }

    /**
     * To be implemented to handle property subscription.
     *
     * @param parameters parameters of property
     * @param consumer   consumer of property values
     */
    public abstract void handleSubscription(ParamType parameters, Consumer<ByteString> consumer);

    /**
     * To be implemented to handle property subscription cancellation.
     *
     * @param requestUUID id of subscription
     */
    public abstract void handleCancelSubscription(String requestUUID);
}
