package sila_java.library.cloud.server.command;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.HashMap;
import java.util.Map;

/**
 * executes unobservable commands on server
 */
@Slf4j
public class ObservableCommandExecutionService {
    final Map<String, ObservedCommandHandler> commandHandlers;
    final Map<SiLAFramework.CommandExecutionUUID, ObservedCommandHandler> executionHandlers;
    final Map<String, ObservedCommandHandler> subscriptionHandlers;

    public ObservableCommandExecutionService(Map<String, ObservedCommandHandler> commandHandlers) {
        this.commandHandlers = commandHandlers;
        this.executionHandlers = new HashMap<>();
        this.subscriptionHandlers = new HashMap<>();
    }

    public SiLACloudConnector.SILAServerMessage executeCommand(SiLACloudConnector.CommandInitiation commandInitiation) throws InvalidProtocolBufferException {

        log.info("Received clientMessage FullyQualifiedCommandId:{}", commandInitiation.getFullyQualifiedCommandId());

        String commandId = commandInitiation.getFullyQualifiedCommandId();

        ObservedCommandHandler handler = commandHandlers.get(commandId);
        if (null == handler) {
            throw new IllegalArgumentException("Unsupported command:" + commandId);
        }
        try {
            SiLAFramework.CommandConfirmation commandResult = handler.handleCommandInitiation(commandInitiation.getCommandParameter().getParameters());
            executionHandlers.put(commandResult.getCommandExecutionUUID(), handler);

            SiLACloudConnector.ObservableCommandConfirmation commandConfirmationResponse = SiLACloudConnector.ObservableCommandConfirmation.newBuilder()
                    .setCommandConfirmation(commandResult)
                    .build();
            return SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setObservableCommandConfirmation(commandConfirmationResponse)
                    .build();
        } catch (StatusRuntimeException e) {
            return SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setCommandError(SiLAErrors.retrieveSiLAError(e).orElse(null))
                    .build();
        }
    }

    public void subscribeCommandInfo(SiLACloudConnector.CommandExecutionInfoSubscription infoSubscription, String requestUUID, StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        SiLAFramework.CommandExecutionUUID executionUUID = infoSubscription.getExecutionUUID();

        ObservedCommandHandler handler = executionHandlers.get(executionUUID);
        if (null == handler) {
            throw new IllegalArgumentException("No executing command handler found for executionUUID: " + executionUUID.getValue());
        }

        handler.handleCommandInfo(executionUUID, (executionInfo -> {

            log.info("handler got executionInfo {}", executionInfo);

            SiLACloudConnector.ObservableCommandExecutionInfo.Builder infoResponse = SiLACloudConnector.ObservableCommandExecutionInfo.newBuilder()
                    .setExecutionInfo(executionInfo);

            SiLACloudConnector.SILAServerMessage serverMessage = SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setRequestUUID(requestUUID)
                    .setObservableCommandExecutionInfo(infoResponse)
                    .build();

            serverMessageStream.onNext(serverMessage);

        }));
        subscriptionHandlers.put(requestUUID, handler);

    }

    public void subscribeCommandIntermediateResultRequest(SiLACloudConnector.CommandIntermediateResponseSubscription request, String requestUUID, StreamObserver<SiLACloudConnector.SILAServerMessage> serverMessageStream) {
        SiLAFramework.CommandExecutionUUID executionUUID = request.getExecutionUUID();

        ObservedCommandHandler handler = executionHandlers.get(executionUUID);
        if (null == handler) {
            throw new IllegalArgumentException("No executing command handler found for executionUUID: " + executionUUID.getValue());
        }
        handler.handleCommandIntermediateResult(executionUUID, (intermediateResult -> {

            log.info("handler got intermediateResult {}", intermediateResult);

            SiLACloudConnector.ObservableCommandIntermediateResponse.Builder response = SiLACloudConnector.ObservableCommandIntermediateResponse.newBuilder()
                    .setResult(intermediateResult);

            SiLACloudConnector.SILAServerMessage serverMessage = SiLACloudConnector.SILAServerMessage.newBuilder()
                    .setRequestUUID(requestUUID)
                    .setObservableCommandIntermediateResponse(response)
                    .build();

            serverMessageStream.onNext(serverMessage);

        }));

        subscriptionHandlers.put(requestUUID, handler);
    }

    public SiLACloudConnector.ObservableCommandResponse executeCommandResult(SiLACloudConnector.CommandGetResponse commandRequest) throws InvalidProtocolBufferException {


        ObservedCommandHandler handler = executionHandlers.get(commandRequest.getExecutionUUID());
        if (null == handler) {
            throw new IllegalArgumentException("No executing command handler found for executionUUID: " + commandRequest.getExecutionUUID().getValue());
        }

        ByteString commandResult = handler.handleCommandResult(commandRequest.getExecutionUUID());

        return SiLACloudConnector.ObservableCommandResponse.newBuilder()
                .setResult(commandResult)
                .build();
    }


    public void cancelExecutionInfoSubscription(String subscriptionId) {
        ObservedCommandHandler handler = subscriptionHandlers.remove(subscriptionId);
        if (handler != null) {
            handler.handleCommandExecutionInfoCancel();
        }
    }

    public void cancelIntermediateResponseSubscription(String subscriptionId) {
        ObservedCommandHandler handler = subscriptionHandlers.remove(subscriptionId);
        if (handler != null) {
            handler.handleCommandIntermediateResponseCancel();
        }
    }
}
