package sila_java.library.cloud.client;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
public class SILACloudClient {

    public static final Integer SILA_CLOUD_DEFAULT_PORT = 50308;

    private final Server gRpcServer;
    private final CloudClientEndpointService endpointService;

    private SILACloudClient(@NonNull final SILACloudClient.Builder builder) throws IOException {
        final Integer port = builder.port;

        endpointService = new CloudClientEndpointService();
        if (null != builder.serverConnectedListener) {
            endpointService.setOnServerConnectListener(builder.serverConnectedListener);
        }

        final ServerBuilder serverBuilder = ServerBuilder
                .forPort(port)
                .addService(endpointService);
        gRpcServer = serverBuilder.build();
        gRpcServer.start();
        log.info("started cloud client at port: " + port);
    }

    public void shutdown() {
        gRpcServer.shutdown();
    }

    public List<CloudConnectedServer> getConnectedServers() {
        return endpointService.getConnectedServers();
    }

    public static class Builder {
        /**
         * default port for cloud clients to listen for servers to connect
         */
        private Integer port = SILA_CLOUD_DEFAULT_PORT;
        private Consumer<CloudConnectedServer> serverConnectedListener;

        /**
         * Define Specific Port for the services
         *
         * @param port Port on which the gRCP service runs.
         */
        public Builder withPort(int port) {
            this.port = port;
            return this;
        }

        public Builder withServerConnectedListener(Consumer<CloudConnectedServer> serverConnectedListener) {
            this.serverConnectedListener = serverConnectedListener;
            return this;
        }

        public SILACloudClient start() throws IOException {
            return new SILACloudClient(this);
        }
    }
}
