package sila_java.library.cloud.server.property;

import com.google.protobuf.ByteString;

import java.util.function.Consumer;

/**
 * The observed property handler interface to handle observed property requests.
 */
public interface ObservedPropertyHandler {

    /**
     * Handles observed property subscription.
     *
     * @param parameters Property Parameters
     * @param consumer   consumes property values
     */
    void handlePropertySubscription(ByteString parameters, Consumer<ByteString> consumer);

    /**
     * Cancel the subscription associated to the requestUUID.
     *
     * @param requestUUID uuid of subscription request
     */
    void cancelSubscription(String requestUUID);

}

