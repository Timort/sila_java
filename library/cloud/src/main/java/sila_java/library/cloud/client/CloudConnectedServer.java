package sila_java.library.cloud.client;

import lombok.Data;
import sila_java.library.cloud.client.command.CommandExecutionService;
import sila_java.library.cloud.client.command.ObservableCommandExecutionService;
import sila_java.library.cloud.client.property.ObservablePropertyService;
import sila_java.library.cloud.client.property.UnobservablePropertyService;

/**
 * server interface for the client.
 */
@Data
public class CloudConnectedServer {
    private CommandExecutionService commandExecutionService;
    private ObservableCommandExecutionService observablecommandExecutionService;
    private ObservablePropertyService observablePropertyService;
    private UnobservablePropertyService unobservablePropertyService;

    public CloudConnectedServer(CommandExecutionService commandExecutionService, ObservableCommandExecutionService observablecommandExecutionService, ObservablePropertyService observablePropertyService, UnobservablePropertyService unobservablePropertyService) {
        this.commandExecutionService = commandExecutionService;
        this.observablecommandExecutionService = observablecommandExecutionService;
        this.observablePropertyService = observablePropertyService;
        this.unobservablePropertyService = unobservablePropertyService;
    }
}
