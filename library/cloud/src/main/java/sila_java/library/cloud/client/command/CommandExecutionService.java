package sila_java.library.cloud.client.command;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLACloudConnector.CommandResponse;
import sila2.org.silastandard.SiLAFramework;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO javadoc
 */
@Slf4j
public class CommandExecutionService {

    private final StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream;

    private final ConcurrentHashMap<String, UnobservableCommand> executions = new ConcurrentHashMap<>();

    public CommandExecutionService(StreamObserver<SiLACloudConnector.SILAClientMessage> clientMessageStream) {
        this.clientMessageStream = clientMessageStream;
    }

    public void executeCommand(UnobservableCommand command) {

        String requestUUID = UUID.randomUUID().toString();
        validateCommandId(command.getCommandId());

        SiLACloudConnector.CommandExecution commandRequest = SiLACloudConnector.CommandExecution.newBuilder()
                .setFullyQualifiedCommandId(command.getCommandId())
                .setCommandParameter(SiLACloudConnector.CommandParameter.newBuilder()
                        .setParameters(command.getPayload()))
                .build();

        SiLACloudConnector.SILAClientMessage clientMessage = SiLACloudConnector.SILAClientMessage.newBuilder()
                .setRequestUUID(requestUUID)
                .setCommandExecution(commandRequest)
                .build();

        executions.put(requestUUID, command);
        send(clientMessage);

        log.info("Command request sent. command:{} requestUUID:{} #executions:{}", command.getCommandId(), requestUUID, executions.size());
    }

    public void onCommandResponse(String requestUUID, CommandResponse commandResponse) {

        log.info("commandResponse. requestUUID: {}", requestUUID);

        getExecution(requestUUID)
                .ifPresent(execution -> onResponse(execution, commandResponse));
    }

    public void onError(String requestUUID, SiLAFramework.SiLAError silaError) {
        log.info("onError: {}", silaError);
        getExecution(requestUUID)
                .ifPresent(execution -> onError(execution, silaError));
    }

    public void onCompleted() {
        log.info("onCompleted. ");
        executions.clear();
    }

    private void onResponse(UnobservableCommand execution, CommandResponse response) {
        try {
            execution.handleCommandResponse(response);
        } catch (Exception ex) {
            log.error("error handling response {}", ex.getMessage(), ex);
        }
    }

    private void onError(UnobservableCommand execution, SiLAFramework.SiLAError error) {
        execution.handleCommandError(error);
    }

    private void validateCommandId(String commandId) {
        if (StringUtils.isEmpty(commandId))
            throw new IllegalArgumentException("Empty command id: " + commandId);
    }

    private void send(SiLACloudConnector.SILAClientMessage clientMessage) {
        clientMessageStream.onNext(clientMessage);
    }

    private Optional<UnobservableCommand> getExecution(String requestUUID) {

        Optional<UnobservableCommand> commandExecution = Optional.ofNullable(executions.get(requestUUID));

        if (!commandExecution.isPresent()) {
            log.warn("Received response for an execution that is not available: {}", requestUUID);
        } else {
            executions.remove(requestUUID);
        }
        return commandExecution;
    }

}
