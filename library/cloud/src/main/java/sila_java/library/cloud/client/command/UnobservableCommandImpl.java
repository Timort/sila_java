package sila_java.library.cloud.client.command;

import com.google.protobuf.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;

import java.util.function.Consumer;


/**
 * The base class for unobserved commands.
 *
 * @param <ParamType>  the parametrized command parameter type
 * @param <ResultType> the parametrized command result type
 */
@Getter
@AllArgsConstructor
public abstract class UnobservableCommandImpl<ParamType extends GeneratedMessageV3, ResultType extends MessageLite> implements UnobservableCommand {

    private final String commandId;
    private final ParamType parameters;
    private final Consumer<ResultType> resultCallback;
    private final Consumer<SiLAFramework.SiLAError> errorCallback;
    private final Parser<ResultType> parser;

    @Override
    public ByteString getPayload() {
        return parameters.toByteString();
    }

    public void handleCommandResponse(SiLACloudConnector.CommandResponse response) throws InvalidProtocolBufferException {

        ResultType responses = parser.parseFrom(response.getResult());
        onResponse(responses);

        if (null != resultCallback) {
            resultCallback.accept(responses);
        }
    }

    public void handleCommandError(SiLAFramework.SiLAError error) {
        onError(error);

        if (null != errorCallback) {
            errorCallback.accept(error);
        }
    }

    public abstract void onResponse(ResultType responses);

    public abstract void onError(SiLAFramework.SiLAError error);

}
