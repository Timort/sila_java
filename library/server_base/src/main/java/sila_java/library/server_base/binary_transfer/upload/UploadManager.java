package sila_java.library.server_base.binary_transfer.upload;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;

import java.io.IOException;
import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
class UploadManager {
    private final ExpiringMap<UUID, UploadCompletion> uploads = ExpiringMap
            .builder()
            .variableExpiration()
            .asyncExpirationListener(UploadManager::onEntryExpiration)
            .build();

    UploadCompletion removeUpload(@NonNull final UUID binaryId) {
        return this.uploads.remove(binaryId);
    }

    UploadCompletion getUpload(@NonNull final UUID binaryId) {
        return (this.uploads.get(binaryId));
    }

    UploadCompletion updateUpload(
            @NonNull final UUID binaryId,
            final int chunkIndex,
            @NonNull final byte[] data,
            @NonNull final Duration expiration
    ) throws IOException {
        final UploadCompletion upload = this.getUpload(binaryId);
        this.uploads.setExpiration(expiration.getSeconds(), TimeUnit.SECONDS);
        upload.addChunk(chunkIndex, data);
        return upload;
    }

    void addUpload(
            @NonNull final UUID binaryId,
            final long totalLength,
            final int nbChunks,
            @NonNull final Duration expiration
    ) {
        if (!this.uploads.containsKey(binaryId)) {
            this.uploads.put(binaryId,
                    new UploadCompletion(totalLength, nbChunks),
                    ExpirationPolicy.CREATED,
                    expiration.getSeconds(),
                    TimeUnit.SECONDS);
        } else {
            throw new IllegalStateException("Upload is already present in the upload manager");
        }
    }

    private static void onEntryExpiration(final UUID uploadId, final UploadCompletion uploadCompletion) {
        log.info("Client upload with id {} expired", uploadId);
        uploadCompletion.close();
    }
}
