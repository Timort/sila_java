package sila_java.library.server_base.binary_transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class BinaryInfo {
    private final UUID id;
    private final OffsetDateTime expiration;
    private final long length;
}
