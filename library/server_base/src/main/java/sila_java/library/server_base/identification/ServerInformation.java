package sila_java.library.server_base.identification;

import lombok.Value;

@Value
public class ServerInformation {
    private String type;
    private String description;
    private String vendorURL;
    private String version;
}