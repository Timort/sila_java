package sila_java.library.server_base.utils;

import lombok.NonNull;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class ArgumentHelperBuilder {
    public static ArgumentHelper build(
            final int port,
            final @NonNull String serverName,
            final @Nullable String configPath,
            final @Nullable String netInterface
    ) {
        return new ArgumentHelper(toArgs(port, configPath, netInterface), serverName);
    }

    public static String[] toArgs(
            final int port,
            final @Nullable String configPath,
            final @Nullable String netInterface
    ) {
        final ArrayList<String> args = new ArrayList<>();
        args.add("-p");
        args.add(Integer.toString(port));
        if (netInterface != null && !netInterface.isEmpty()) {
            args.add("-n");
            args.add(netInterface);
        }
        if (configPath != null) {
            args.add("-c");
            args.add(configPath);
        }
        return args.toArray(new String[0]);
    }
}
