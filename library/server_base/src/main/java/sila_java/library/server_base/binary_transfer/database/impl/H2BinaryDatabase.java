package sila_java.library.server_base.binary_transfer.database.impl;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.h2.jdbcx.JdbcDataSource;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public class H2BinaryDatabase implements BinaryDatabase {
    private static final String SQL_INIT = "CREATE TABLE IF NOT EXISTS BINARIES\n" +
            "(\n" +
            "    ID UUID NOT NULL PRIMARY KEY,\n" +
            "    EXPIRATION TIMESTAMP WITH TIME ZONE,\n" +
            "    DATA BLOB\n" +
            ");\n";
    private static final String DB_USER = "sa";
    private static final String DB_PASS = "sa";
    private static final int CLEANUP_CHECK_INTERVAL_SEC = 300;
    private static final Duration BINARY_EXTEND_DURATION = Duration.ofMinutes(10);
    private final ScheduledExecutorService cleaner = Executors.newSingleThreadScheduledExecutor();
    private final Connection connection;

    public H2BinaryDatabase(@NonNull final UUID serverId) throws SQLException {
        final JdbcDataSource ds = new JdbcDataSource();
        final String binaryDbPath = "jdbc:h2:~/.unitelabs/binary/" + serverId.toString() + ";DB_CLOSE_ON_EXIT=FALSE";
        ds.setURL(binaryDbPath);
        log.info("binary db path is: {}", binaryDbPath);
        ds.setUser(DB_USER);
        ds.setPassword(DB_PASS);
        this.connection = ds.getConnection();
        try {
            connection.prepareStatement(SQL_INIT).execute();
        } catch (SQLException e) {
            connection.close();
            throw e;
        }
        this.cleaner.scheduleAtFixedRate(() -> {
            try {
                purgeExpiredBinaries();
            } catch (BinaryDatabaseException e) {
                log.warn("Following exception occurred while purging expired binaries: {}", e.getMessage(), e);
            }
        }, 0, CLEANUP_CHECK_INTERVAL_SEC, TimeUnit.SECONDS);
    }

    @Override
    public void close() {
        this.cleaner.shutdownNow();
        try {
            this.removeAllBinaries();
        } catch (BinaryDatabaseException e) {
            log.warn("Error occurred while removing binaries from database: {}", e.getMessage(), e);
        }
        try {
            this.connection.close();
        } catch (SQLException e) {
            log.warn("Exception occurred while closing db connection", e);
        }
    }

    @Override
    public Binary getBinary(@NonNull final UUID binaryId) throws BinaryDatabaseException {
        try {
            final PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "SELECT ID, EXPIRATION, DATA, length(BINARIES.DATA) AS BYTE_SIZE FROM BINARIES WHERE ID = ?"
            );
            preparedStatement.setString(1, binaryId.toString());
            preparedStatement.execute();
            final ResultSet resultSet = preparedStatement.getResultSet();
            if (!resultSet.first()) {
                throw new BinaryDatabaseException("No binary found with id " + binaryId.toString());
            }
            return new Binary(resultSet.getBlob("DATA"), getBinaryInfo(resultSet));
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    @Override
    public BinaryInfo getBinaryInfo(@NonNull final UUID binaryId) throws BinaryDatabaseException {
        try {
            final PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "SELECT ID, EXPIRATION, length(BINARIES.DATA) AS BYTE_SIZE FROM BINARIES WHERE ID = ?"
            );
            preparedStatement.setString(1, binaryId.toString());
            preparedStatement.execute();
            final ResultSet resultSet = preparedStatement.getResultSet();
            if (!resultSet.first()) {
                throw new BinaryDatabaseException("No blob found with id " + binaryId.toString());
            }
            return getBinaryInfo(resultSet);
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    @Override
    public Duration addBinary(
            @NonNull final UUID binaryId,
            @NonNull final InputStream stream
    ) throws BinaryDatabaseException {
        try {
            final PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "INSERT INTO BINARIES VALUES (?, ?, ?)"
            );
            preparedStatement.setObject(1, binaryId);
            final OffsetDateTime expirationDate = OffsetDateTime.now().plus(BINARY_EXTEND_DURATION);
            preparedStatement.setObject(2, expirationDate);
            preparedStatement.setBinaryStream(3, stream);
            preparedStatement.execute();
            return Duration.between(OffsetDateTime.now(), expirationDate);
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                log.debug("Exception occurred while closing stream {}", e.getMessage(), e);
            }
        }
    }

    @Override
    public Duration extendBinaryExpiration(@NonNull final UUID binaryId) throws BinaryDatabaseException {
        try {
            final PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "UPDATE BINARIES SET EXPIRATION = ? WHERE ID = ?"
            );
            final OffsetDateTime newExpiration = getBinaryInfo(binaryId).getExpiration().plus(BINARY_EXTEND_DURATION);
            preparedStatement.setObject(1, newExpiration);
            preparedStatement.setObject(2, binaryId);
            preparedStatement.execute();
            return Duration.between(OffsetDateTime.now(), newExpiration);
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    @Override
    public void removeAllBinaries() throws BinaryDatabaseException {
        try {
            this.connection.prepareStatement("delete from BINARIES").execute();
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    @Override
    public void removeBinary(@NonNull final UUID binaryId) throws BinaryDatabaseException {
        try {
            final PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "delete from BINARIES where ID = ?"
            );
            preparedStatement.setObject(1, binaryId);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    @Override
    public void reserveBytes(final long length) throws BinaryDatabaseException {
        // todo check if there is enough space on the disk the save the temporary files
        //  and to store the merged temporary files into the database
        throw new NotImplementedException("Will be implemented in the future.");
    }

    private void purgeExpiredBinaries() throws BinaryDatabaseException {
        try {
            final PreparedStatement preparedStatement = this.connection.prepareStatement(
                    "delete from BINARIES where EXPIRATION < ?"
            );
            preparedStatement.setObject(1, OffsetDateTime.now());
            final int nbRemoved = preparedStatement.executeUpdate();
            if (nbRemoved > 0) {
                log.info("Purged {} expired binary.", nbRemoved);
            }
        } catch (SQLException e) {
            throw new BinaryDatabaseException(e);
        }
    }

    private static BinaryInfo getBinaryInfo(ResultSet resultSet) throws SQLException {
        final UUID id = UUID.fromString(resultSet.getString("ID"));
        final OffsetDateTime expiration = resultSet.getObject("EXPIRATION", OffsetDateTime.class);
        final long byteSize = resultSet.getLong("BYTE_SIZE");
        return new BinaryInfo(id, expiration, byteSize);
    }
}
