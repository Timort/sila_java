package sila_java.library.server_base.binary_transfer.upload;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;

@Slf4j
@Getter(value = AccessLevel.PACKAGE)
class UploadCompletion implements AutoCloseable {
    private static final String CHUNK_FILE_EXT = ".chunk";
    private final Map<Integer, File> chunks = new TreeMap<>(Comparator.naturalOrder());
    private final long totalLength;
    private final int totalChunks;

    UploadCompletion(final long totalLength, final int nbChunk) {
        this.totalLength = totalLength;
        this.totalChunks = nbChunk;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored") // ignore result of File.delete
    void addChunk(final int chunkIndex, @NonNull final byte[] data) throws IOException {
        if (this.isComplete()) {
            throw new IllegalStateException("Download is complete");
        }
        if (data.length == 0) {
            throw new IllegalArgumentException("Chunk is empty");
        }
        if (chunkIndex < 0 || chunkIndex >= this.totalChunks) {
            throw new IllegalArgumentException("Invalid chunk index `" + chunkIndex + "`");
        }
        final File previousEntry = this.chunks.put(chunkIndex, saveChunkToFile(data));
        if (previousEntry != null) {
            previousEntry.delete();
        }
    }

    boolean isComplete() {
        return this.chunks.size() == this.totalChunks && this.totalLength == getTotalChunksLength();
    }

    private long getTotalChunksLength() {
        return this.chunks.values().stream().mapToLong(File::length).sum();
    }

    InputStream chunksToStream() throws IOException {
        final List<InputStream> streams = new ArrayList<>(this.chunks.size());
        try {
            for (final Map.Entry<Integer, File> entry : this.chunks.entrySet()) {
                streams.add(entry.getKey(), new FileInputStream(entry.getValue()));
            }
        } catch (FileNotFoundException e) {
            for (final InputStream stream : streams) {
                try {
                    stream.close();
                } catch (IOException e1) {
                    log.debug("Exception occurred while closing inputstream: {}", e1.getMessage(), e1);
                }
            }
            throw new IOException(e);
        }
        return new SequenceInputStream(Collections.enumeration(streams));
    }

    @SuppressWarnings("ResultOfMethodCallIgnored") // ignore result of File.delete
    private static File saveChunkToFile(final byte[] data) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), CHUNK_FILE_EXT);
        tempFile.deleteOnExit();
        try {
            FileUtils.writeByteArrayToFile(tempFile, data);
        } catch (IOException e) {
            tempFile.delete();
            throw e;
        }
        return tempFile;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored") // ignore result of File.delete
    @Override
    public void close() {
        this.chunks.values().forEach(File::delete);
        this.chunks.clear();
    }
}