package sila_java.library.server_base.binary_transfer.database;

public class BinaryDatabaseException extends Exception {
    public BinaryDatabaseException() {
        super();
    }

    public BinaryDatabaseException(String message) {
        super(message);
    }

    public BinaryDatabaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BinaryDatabaseException(Throwable cause) {
        super(cause);
    }
}
