package sila_java.library.server_base.binary_transfer.upload;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.types.SiLADuration;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;

import java.io.IOException;
import java.time.Duration;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
public class UploadService extends BinaryUploadGrpc.BinaryUploadImplBase {
    private static final Duration MAX_UPLOAD_DURATION = Duration.ofMinutes(10);
    private final UploadManager uploadManager = new UploadManager();
    private final BinaryDatabase binaryDatabase;

    @Override
    public void createBinary(
            final SiLABinaryTransfer.CreateBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.CreateBinaryResponse> responseObserver
    ) {
        final UUID blobId = UUID.randomUUID();
        final Duration expiration = MAX_UPLOAD_DURATION;

        uploadManager.addUpload(blobId, request.getBinarySize(), request.getChunkCount(), expiration);
        responseObserver.onNext(
                SiLABinaryTransfer.CreateBinaryResponse
                        .newBuilder()
                        .setLifetimeOfBinary(SiLADuration.from(expiration))
                        .setBinaryTransferUUID(blobId.toString())
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunk(
            final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver
    ) {
        return new StreamObserver<SiLABinaryTransfer.UploadChunkRequest>() {
            @Override
            public void onNext(final SiLABinaryTransfer.UploadChunkRequest request) {
                uploadChunk(request, responseObserver);
            }

            @Override
            public void onError(final Throwable t) {
                log.warn("Upload chunk stream exception: {}", t.getMessage(), t);
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    private void uploadChunk(
            final SiLABinaryTransfer.UploadChunkRequest request,
            final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver
    ) {
        Duration expiration = MAX_UPLOAD_DURATION;
        final UUID blobId = UUID.fromString(request.getBinaryTransferUUID());

        try {
            final UploadCompletion uploadCompletion = this.uploadManager.updateUpload(
                    blobId,
                    request.getChunkIndex(),
                    request.getPayload().toByteArray(),
                    expiration
            );
            if (uploadCompletion.isComplete()) {
                log.info("Upload {} from client complete", blobId);
                // Close the upload, remove it and add to database
                this.uploadManager.removeUpload(blobId);
                expiration = this.binaryDatabase.addBinary(blobId, uploadCompletion.chunksToStream());
                uploadCompletion.close();
            }
        } catch (final IOException | BinaryDatabaseException e) {
            log.error("The following error occurred when attempting to save received chunk: {}", e.getMessage(), e);
            responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
            return;
        }
        responseObserver.onNext(
                SiLABinaryTransfer.UploadChunkResponse
                        .newBuilder()
                        .setBinaryTransferUUID(request.getBinaryTransferUUID())
                        .setChunkIndex(request.getChunkIndex())
                        .setLifetimeOfBinary(SiLADuration.from(expiration))
                        .build()
        );
    }

    @Override
    public void deleteBinary(
            final SiLABinaryTransfer.DeleteBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.DeleteBinaryResponse> responseObserver
    ) {
        this.uploadManager.removeUpload(UUID.fromString(request.getBinaryTransferUUID()));
        responseObserver.onNext(SiLABinaryTransfer.DeleteBinaryResponse.newBuilder().build());
        responseObserver.onCompleted();
    }
}