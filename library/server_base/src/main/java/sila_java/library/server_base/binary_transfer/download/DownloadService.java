package sila_java.library.server_base.binary_transfer.download;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.types.SiLADuration;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.UUID;

@Slf4j
@AllArgsConstructor
public class DownloadService extends BinaryDownloadGrpc.BinaryDownloadImplBase {
    private final BinaryDatabase binaryDatabase;

    @Override
    public void getBinaryInfo(
            final SiLABinaryTransfer.GetBinaryInfoRequest request,
            final StreamObserver<SiLABinaryTransfer.GetBinaryInfoResponse> responseObserver
    ) {
        final BinaryInfo binaryInfo;
        try {
            binaryInfo = this.binaryDatabase.getBinaryInfo(UUID.fromString(request.getBinaryTransferUUID()));
        } catch (BinaryDatabaseException e) {
            responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
            return;
        }
        responseObserver.onNext(
                SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder()
                        .setBinarySize(binaryInfo.getLength())
                        .setLifetimeOfBinary(
                                SiLADuration.from(Duration.between(OffsetDateTime.now(), binaryInfo.getExpiration()))
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<SiLABinaryTransfer.GetChunkRequest> getChunk(
            final StreamObserver<SiLABinaryTransfer.GetChunkResponse> responseObserver
    ) {
        return new StreamObserver<SiLABinaryTransfer.GetChunkRequest>() {
            @Override
            public void onNext(final SiLABinaryTransfer.GetChunkRequest request) {
                getChunk(request, responseObserver);
            }

            @Override
            public void onError(final Throwable t) {
                log.warn("Download chunk stream exception: {}", t.getMessage(), t);
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    private void getChunk(
            final SiLABinaryTransfer.GetChunkRequest request,
            final StreamObserver<SiLABinaryTransfer.GetChunkResponse> responseObserver
    ) {
        try {
            final UUID id = UUID.fromString(request.getBinaryTransferUUID());
            final Duration extendedExpiration = binaryDatabase.extendBinaryExpiration(id);
            final Binary binary = binaryDatabase.getBinary(id);
            responseObserver.onNext(
                    SiLABinaryTransfer.GetChunkResponse.newBuilder()
                            .setPayload(
                                    ByteString.readFrom(
                                            binary.getData().getBinaryStream(
                                                    request.getOffset() + 1L, // First byte is at index 1
                                                    request.getLength()
                                            )
                                    )
                            )
                            .setOffset(request.getOffset())
                            .setBinaryTransferUUID(request.getBinaryTransferUUID())
                            .setLifetimeOfBinary(SiLADuration.from(extendedExpiration))
                            .build()
            );
        } catch (Exception e) {
            responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
        }
    }

    @Override
    public void deleteBinary(
            final SiLABinaryTransfer.DeleteBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.DeleteBinaryResponse> responseObserver
    ) {
        try {
            this.binaryDatabase.removeBinary(UUID.fromString(request.getBinaryTransferUUID()));
        } catch (BinaryDatabaseException e) {
            responseObserver.onError(SiLAErrors.generateGenericExecutionError(e));
            return;
        }
        responseObserver.onNext(SiLABinaryTransfer.DeleteBinaryResponse.newBuilder().build());
        responseObserver.onCompleted();
    }
}