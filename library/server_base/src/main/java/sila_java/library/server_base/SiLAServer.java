package sila_java.library.server_base;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerServiceDefinition;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.discovery.SiLAServerRegistration;
import sila_java.library.core.encryption.EncryptionUtils;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.config.NonPersistentServerConfigWrapper;
import sila_java.library.server_base.config.PersistentServerConfigWrapper;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.binary_transfer.download.DownloadService;
import sila_java.library.server_base.binary_transfer.upload.UploadService;
import sila_java.library.server_base.standard_features.FeatureImplementation;
import sila_java.library.server_base.standard_features.SiLAServiceServer;
import sila_java.library.server_base.utils.TransmitThrowableInterceptor;

import java.io.IOException;
import java.nio.file.Path;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.utils.SocketUtils.getAvailablePortInRange;

@Slf4j
public class SiLAServer implements AutoCloseable {
    private final static int SHUTDOWN_TIMEOUT = 20; // [s]
    private final SiLAServerRegistration serverRegistration;
    private final Server server;
    private final BinaryDatabase binaryDatabaseImpl;

    /**
     * Create and start a Base Server to expose the SiLA Features
     *
     * @param builder The builder
     *
     * @implNote Instance can only withoutConfig one server at a time.
     */
    private SiLAServer(@NonNull final Builder builder) throws IOException {
        final SiLAServiceServer siLAServiceServer = new SiLAServiceServer(
                builder.serverConfig,
                builder.serverInformation,
                builder.featureDefinitions
        );
        log.info("Server registered on SiLAService with features={}", builder.featureDefinitions.keySet());

        final ServerBuilder serverBuilder = ServerBuilder
                .forPort(builder.port)
                .addService(siLAServiceServer.getService());

        this.binaryDatabaseImpl = builder.binaryDatabaseImpl;

        if (this.binaryDatabaseImpl == null) {
            log.warn("Server will not support binary transfer");
        } else {
            log.info("Server will support binary transfer");
            serverBuilder.addService(new DownloadService(this.binaryDatabaseImpl));
            serverBuilder.addService(new UploadService(this.binaryDatabaseImpl));
        }

        builder.bindableServices.forEach(serverBuilder::addService);
        builder.serverServices.forEach(serverBuilder::addService);

        // If cert and key we use transport security
        if (builder.certificate != null && builder.privateKey != null) {
            try {
                serverBuilder.useTransportSecurity(
                        EncryptionUtils.certificateToStream(builder.certificate),
                        EncryptionUtils.keyToStream(builder.privateKey)
                );
            } catch (final CertificateEncodingException e) {
                throw new IOException(e);
            }
            log.info("Server will use safe encrypted communication.");
        } else {
            log.warn("Server will use unsafe plain-text communication.");
        }

        this.server = serverBuilder.intercept(TransmitThrowableInterceptor.instance())
                .build()
                .start();

        log.info("Server started on port={}", builder.port);
        if (builder.interfaceName != null) {
            serverRegistration = new SiLAServerRegistration(
                    builder.serverConfig.getCacheConfig().getUuid(),
                    builder.interfaceName,
                    builder.port
            );
            log.info("Server registered with discovery.");
        } else {
            serverRegistration = null;
            log.warn("Server started without specifying a network interface, discovery is not enabled.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        log.info("[stop] stopping server...");
        if (this.serverRegistration != null) {
            this.serverRegistration.close();
        }

        // Stop server
        if (this.server != null && !this.server.isTerminated() && !this.server.isShutdown()) {
            try {
                log.info("[stop] stopping the server ...");
                this.server.shutdownNow().awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
                log.info("[stop] the server was stopped");
            } catch (InterruptedException e) {
                log.warn("[stop] could not shutdown the server within {} seconds", SHUTDOWN_TIMEOUT);
            }
            log.info("[stop] stopped");
        } else {
            log.info("[stop] server already stopped");
        }
        if (this.binaryDatabaseImpl != null) {
            try {
                this.binaryDatabaseImpl.close();
            } catch (Exception e) {
                log.warn("Error occurred while closing binary database {}", e.getMessage(), e);
            }
        }
    }

    public static class Builder {
        private static int[] defaultPortRange = {50052, 50052+256};

        // Mandatory Arguments
        private final ServerInformation serverInformation;
        private final IServerConfigWrapper serverConfig;
        private final Map<String, String> featureDefinitions = new HashMap<>();
        private final List<BindableService> bindableServices = new ArrayList<>();
        private final List<ServerServiceDefinition> serverServices = new ArrayList<>();

        // Optional Arguments
        private String interfaceName = null;
        private Integer port = null;
        private BinaryDatabase binaryDatabaseImpl = null;

        // TLS
        private X509Certificate certificate = null;
        private PrivateKey privateKey = null;

        /**
         * Enables Discovery on a certain interface
         * @param interfaceName  Name of network interface to use discovery
         */
        public Builder withDiscovery(@NonNull final String interfaceName) {
            this.interfaceName = interfaceName;
            return this;
        }

        /**
         * Define Specific Port for the services (otherwise a default range will be chosen)
         * @param port Port on which the server runs.
         */
        public Builder withPort(int port) {
            this.port = port;
            return this;
        }

        /**
         * Enable support for binary transfer
         */
        public Builder withBinaryTransferSupport(@NonNull final BinaryDatabase binarySupportImpl) {
            this.binaryDatabaseImpl = binarySupportImpl;
            return this;
        }

        /**
         * Enable support for binary transfer by creating and using the default H2 binary transfer database implementation
         * @see H2BinaryDatabase
         * @return the created binary database
         */
        @SneakyThrows
        public BinaryDatabase withBinaryTransferSupport() {
            try {
                if (binaryDatabaseImpl != null) {
                    binaryDatabaseImpl.close();
                    binaryDatabaseImpl = null;
                    log.warn("Duplicated call to withBinaryTransferSupport, closing previous database.");
                }
                binaryDatabaseImpl = new H2BinaryDatabase(this.serverConfig.getCacheConfig().getUuid());
            } catch (SQLException | IOException  e) {
                log.warn("Error while setting BinaryDatabase: {}", e.getMessage(), e);
                throw new RuntimeException(e);
            }
            return binaryDatabaseImpl;
        }

        /**
         * Add a Feature to be exposed
         * @param featureDescription Feature Description as String Content in XML
         * @param featureService Feature Service implemented with gRPC
         *
         * @implNote Unfortunately there is no straightforward way to validate the pure protobuf
         * descriptions and the generated gRPC descriptions.
         */
        @SneakyThrows({IOException.class, MalformedSiLAFeature.class})
        public Builder addFeature(
                @NonNull final String featureDescription,
                @NonNull final BindableService featureService
        ) {
            featureDefinitions.put(
                    FeatureGenerator.generateFullyQualifiedIdentifier(
                            FeatureGenerator.generateFeature(featureDescription)
                    ),
                    featureDescription
            );
            bindableServices.add(featureService);
            return this;
        }

        /**
         * Add a Feature to be exposed
         * @param featureImplementation Exposing both the description and implementation
         */
        @SneakyThrows({IOException.class, MalformedSiLAFeature.class})
        public Builder addFeature(@NonNull final FeatureImplementation featureImplementation) {
            featureDefinitions.put(
                    FeatureGenerator.generateFullyQualifiedIdentifier(
                            FeatureGenerator.generateFeature(featureImplementation.getFeatureDescription())
                    ),
                    featureImplementation.getFeatureDescription()
            );
            bindableServices.add(featureImplementation.getService());
            return this;
        }

        /**
         * Add a Feature to be exposed
         * @param featureDescription Feature Description as String Content in XML
         * @param serverServiceFeatureDefinition Server service definition feature implemented with gRPC
         *
         */
        @SneakyThrows({IOException.class, MalformedSiLAFeature.class})
        public Builder addFeature(
                @NonNull final String featureDescription,
                @NonNull final ServerServiceDefinition serverServiceFeatureDefinition
        ) {
            featureDefinitions.put(
                    FeatureGenerator.generateFullyQualifiedIdentifier(
                            FeatureGenerator.generateFeature(featureDescription)
                    ),
                    featureDescription
            );
            serverServices.add(serverServiceFeatureDefinition);
            return this;
        }

        /**
         * Starts and Creates the SiLA Server
         * @return SiLA Server
         */
        public SiLAServer start() throws IOException {
            if (this.port == null) {
                this.port = getAvailablePortInRange(defaultPortRange[0], defaultPortRange[1]);
            }

            return new SiLAServer(this);
        }

        /**
         * Use TLS certification
         * @param certChain InputStream certification
         * @param privateKey InputStream private key
         */
        public Builder withTLS(
                @NonNull final X509Certificate certChain,
                @NonNull final PrivateKey privateKey
        ) {
            this.certificate = certChain;
            this.privateKey = privateKey;
            return this;
        }

        /**
         * Use Plain-Text communication method
         */
        public Builder withPlainText() {
            this.certificate = null;
            this.privateKey = null;
            return this;
        }

        /**
         * Create and use a self signed certificate
         */
        public Builder withSelfSignedCertificate() throws SelfSignedCertificate.CertificateGenerationException {
            final SelfSignedCertificate selfSignedCertificate = SelfSignedCertificate.newBuilder().build();
            this.certificate = selfSignedCertificate.getCertificate();
            this.privateKey = selfSignedCertificate.getPrivateKey();
            return this;
        }

        /**
         * Create builder for a Server with a non-persistent configuration
         *
         * @param serverInformation     Meta server information defined by the server implementer
         */
        public static Builder withoutConfig(@NonNull final ServerInformation serverInformation) {
            log.debug("Server config is non-persistent");
            return new Builder(serverInformation, new NonPersistentServerConfigWrapper(serverInformation.getType()));
        }

        /**
         * Create builder for a Server with a persistent configuration file
         *
         * @param configurationFile     The file persisting the server name and UUID data for that server instance
         * @param serverInformation     Meta server information defined by the server implementer
         */
        public static Builder withConfig(
                @NonNull final Path configurationFile,
                @NonNull final ServerInformation serverInformation
        ) throws IOException {
            log.debug("Server config is persistent");
            return new Builder(
                    serverInformation,
                    new PersistentServerConfigWrapper(configurationFile, serverInformation.getType())
            );
        }

        private Builder(
                @NonNull final ServerInformation serverInformation,
                @NonNull final IServerConfigWrapper serverConfig
        ) {
            this.serverInformation = serverInformation;
            this.serverConfig = serverConfig;
        }
    }
}
