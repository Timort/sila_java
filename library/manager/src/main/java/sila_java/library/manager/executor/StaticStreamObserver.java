package sila_java.library.manager.executor;

import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.ClientCall;
import lombok.NonNull;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Stream Observer for custom calls
 */
public final class StaticStreamObserver implements io.grpc.stub.StreamObserver<Object> {
    private final ConcurrentLinkedDeque<String> resultsList = new ConcurrentLinkedDeque<>();
    private final CompletableFuture<List<String>> future = new CompletableFuture<>();
    private final ClientCall<Object, Object> clientCall;
    private final StreamCallback callback;

    /**
     * Constructor
     * @param clientCall The call
     * @param callback Callback to be executed on every element of the Stream
     */
    public StaticStreamObserver(
            @NonNull final ClientCall<Object, Object> clientCall,
            @Nullable StreamCallback callback
    ) {
        this.clientCall = clientCall;
        this.callback = callback;
    }

    public CompletableFuture<List<String>> getFuture() {
        return future;
    }

    @Override
    public void onNext(Object message) {
        final String json;
        try {
            json = ProtoMapper.serializeToJson((DynamicMessage)message);
            resultsList.add(json);
            if (this.callback != null && !this.callback.onNext(json)) {
                clientCall.cancel(null, new StreamCancellationException());
            }
        } catch (InvalidProtocolBufferException e) {
            this.onError(new IllegalArgumentException(e.getMessage()));
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable.getCause() instanceof StreamCancellationException)
            future.complete(new ArrayList<>(resultsList));
        else
            future.completeExceptionally(throwable);
    }

    @Override
    public void onCompleted() {
        future.complete(new ArrayList<>(resultsList));
    }

    /**
     * A simple exception class thrown when we manually cancel the stream call
     */
    private static final class StreamCancellationException extends RuntimeException { }

    public interface StreamCallback {
        /**
         * Called when the a new message is received
         * @param message The message
         * @return False to cancel the stream, true to continue reading
         */
        boolean onNext(String message);
    }
}
