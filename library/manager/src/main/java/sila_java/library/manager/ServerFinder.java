package sila_java.library.manager;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.asynchronous.MethodPoller;
import sila_java.library.manager.models.Server;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class used to find and retrieve servers that match specific filters
 */
@Slf4j
public class ServerFinder {
    private final ServerManager serverManager;
    private final Predicate<Server> filterPredicate;

    /**
     * Class that describe a filter to apply on the servers
     */
    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Filter {
        private final Type type;
        private final Predicate<Server> predicate;

        private enum Type {
            NAME,
            TYPE,
            HOST,
            PORT,
            STATUS,
            CUSTOM,
            UUID
        }

        /**
         * Create a server UUID filter
         * @param uuid The server UUID to filter
         * @return a new Filter instance
         */
        public static Filter uuid(@NonNull final UUID uuid) {
            return new Filter(Type.UUID, server -> server.getConfiguration().getUuid().equals(uuid));
        }

        /**
         * Create a server type filter
         * @param type The server type to filter
         * @return a new Filter instance
         */
        public static Filter type(@NonNull final String type) {
            return new Filter(Type.TYPE, server -> server.getInformation().getType().equals(type));
        }

        /**
         * Create a server name filter
         * @param name The server name to filter
         * @return a new Filter instance
         */
        public static Filter name(@NonNull final String name) {
            return new Filter(Type.NAME, server -> server.getConfiguration().getName().equals(name));
        }

        /**
         * Create a server host filter
         * @param host The server host to filter
         * @return a new Filter instance
         */
        public static Filter host(@NonNull final String host) {
            return new Filter(Type.HOST, server -> server.getHost().equals(host));
        }

        /**
         * Create a server port filter
         * @param port The server port to filter
         * @return a new Filter instance
         */
        public static Filter port(final int port) {
            return new Filter(Type.PORT, server -> server.getPort() == port);
        }

        /**
         * Create a server status filter
         * @param status the server status to filter
         * @return a new Filter instance
         */
        public static Filter status(@NonNull final Server.Status status) {
            return new Filter(Type.STATUS, server -> server.getStatus().equals(status));
        }

        /**
         * Create a custom filter
         * @param customPredicate the custom predicate to apply
         * @return a new Filter instance
         */
        public static Filter custom(@NonNull final Predicate<Server> customPredicate) {
            return new Filter(Type.CUSTOM, customPredicate);
        }
    }

    public static ServerFinder filterBy(final Filter ... filters) {
        return new ServerFinder(filters);
    }

    /**
     * Find and return all the servers that match the filters
     * @return the servers that match the filters
     */
    public List<Server> find() {
        return matchingServers().collect(Collectors.toList());
    }

    /**
     * Attempt to find one server that match the filters
     * @return an optional server
     */
    public Optional<Server> findOne() {
        return matchingServers().findAny();
    }

    /**
     * Attempt to find one server that match the filters while scanning the network for new servers
     * @param timeout The maximum time to find a server
     * @return an optional server
     */
    public Optional<Server> scanAndFindOne(@NonNull final Duration timeout) {
        try {
            return Optional.of(
                    MethodPoller
                            .await()
                            .atMost(timeout)
                            .withInterval(Duration.ZERO)
                            .untilPresent(() -> {
                                final Optional<Server> optionalServer = findOne();

                                if (optionalServer.isPresent()) {
                                    return optionalServer;
                                } else {
                                    serverManager.getDiscovery().scanNetwork(1);
                                    return Optional.empty();
                                }
                            })
            );
        } catch (ExecutionException | TimeoutException e) {
            return Optional.empty();
        }
    }

    /**
     * Stream the servers that match the filters
     * @return A stream of server
     */
    private Stream<Server> matchingServers() {
        return serverManager.getServers().values().stream().filter(filterPredicate);
    }

    /**
     * Private utility function to make sure that there is no duplicate filter type
     * @param filters The filters
     * @return True if there is duplicate, false otherwise
     */
    private static boolean hasDuplicateFilter(@NonNull final Collection<Filter> filters) {
        return filters.stream().map(Filter::getType).collect(Collectors.toSet()).size() != filters.size();
    }

    /**
     * Private constructor
     * @param filters The filters
     */
    private ServerFinder(final Filter ... filters) {
        this.serverManager = ServerManager.getInstance();

        if (filters.length == 0) {
            this.filterPredicate = x -> true;
        } else {
            final List<Filter> filterList = Arrays.asList(filters);
            if (hasDuplicateFilter(filterList)) {
                throw new RuntimeException("Duplicate filters found, there can only be one type of filter");
            }
            this.filterPredicate = filterList
                    .stream()
                    .map(filter -> filter.predicate)
                    .reduce(Predicate::and)
                    .orElse(x -> true);
        }
    }
}
