package sila_java.library.manager.executor;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.*;
import io.grpc.stub.ClientCalls;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.ExceptionGeneration;
import sila_java.library.core.sila.mapping.grpc.GrpcNameMapper;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.manager.grpc.DynamicMessageMarshaller;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.SiLACall;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallInProgress;
import sila_java.library.manager.server_management.Connection;

import javax.annotation.Nullable;
import java.security.KeyException;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * Executor to handle all possible SiLA Calls
 */
@Slf4j
public class ServerCallExecutor {
    private static final Duration DEFAULT_CALL_TIMEOUT = Duration.ofMillis(Long.MAX_VALUE); // Infinite timeout
    private static final ExecutorService executor =  new ThreadPoolExecutor(
            1,
            Runtime.getRuntime().availableProcessors(),
            0L,
            TimeUnit.MILLISECONDS,
            new SynchronousQueue<>(), // don't queue task
            new ThreadFactoryBuilder().setNameFormat("sila-call-exec-%d").setDaemon(true).build()
    );
    private final Connection connection;
    private final SiLACall baseCall;
    private final Descriptors.ServiceDescriptor feature;
    private final Duration timeout;
    private final CommandCallListener commandCallListener;

    public static class Builder {
        private final Connection connection;
        private final SiLACall baseCall;
        private final Descriptors.ServiceDescriptor feature;
        private Duration timeout = DEFAULT_CALL_TIMEOUT;
        private CommandCallListener commandCallListener = new CommandCallListener() {};

        private Builder(@NonNull final Connection connection, @NonNull final SiLACall baseCall) {
            this.connection = connection;
            this.baseCall = baseCall;
            try {
                this.feature = this.connection.getFeatureService(baseCall.getFeatureId());
            } catch (KeyException e) {
                throw new RuntimeException(e);
            }
        }

        public ServerCallExecutor build() {
            return new ServerCallExecutor(this);
        }

        /**
         * Set the timeout when performing a call.
         * Default value is {@link ServerCallExecutor#DEFAULT_CALL_TIMEOUT}
         *
         * @param timeout The timeout to set
         * @return the builder
         */
        public Builder withTimeout(@NonNull final Duration timeout) {
            this.timeout = timeout;
            return this;
        }

        public Builder withCommandCallListener(@NonNull final CommandCallListener commandCallListener) {
            this.commandCallListener = commandCallListener;
            return this;
        }
    }

    public static Builder newBuilder(@NonNull final Connection connection, @NonNull final SiLACall baseCall) {
        return new Builder(connection, baseCall);
    }

    private ServerCallExecutor(@NonNull final Builder builder) {
        this.connection = builder.connection;
        this.baseCall = builder.baseCall;
        this.feature = builder.feature;
        this.timeout = builder.timeout;
        this.commandCallListener = builder.commandCallListener;
        if (timeout.isZero() || timeout.isNegative()) {
            throw new IllegalArgumentException("Timeout must be greater than 0");
        }
    }

    public String execute() {
        String result = "";
        switch (this.baseCall.getType()) {
        case UNOBSERVABLE_COMMAND:
            result = executeCommandWithProgression(this::executeUnobservableCommand);
            break;
        case OBSERVABLE_COMMAND:
            result = executeCommandWithProgression(this::executeObservableCommand);
            break;
        case UNOBSERVABLE_PROPERTY:
            result = getUnobservableProperty();
            break;
        case OBSERVABLE_PROPERTY:
            result = getObservableProperty();
            break;
        }

        return result;
    }

    @FunctionalInterface
    private interface CallExecutor {
        String execute();
    }

    private String executeCommandWithProgression(@NonNull final CallExecutor callExecutor) {
        final CallInProgress callInProgress = new CallInProgress(OffsetDateTime.now(), this.timeout, this.baseCall);
        final Future<String> completableFuture = executor.submit(callExecutor::execute);

        commandCallListener.onProgress(callInProgress, () -> completableFuture.cancel(true));
        log.info("Call {} started", this.baseCall.getCallId());
        final String result = callFuture(completableFuture, errorMessage -> {
            final CallErrored callErrored = new CallErrored(
                    callInProgress.getStartDate(),
                    OffsetDateTime.now(),
                    errorMessage,
                    this.baseCall
            );
            log.info("Call {} ended with error", callInProgress.getSiLACall().getCallId());
            commandCallListener.onError(callErrored);
        });
        final CallCompleted callCompleted = new CallCompleted(
                callInProgress.getStartDate(), OffsetDateTime.now(), result, this.baseCall
        );
        log.info("Call {} ended successfully", callInProgress.getSiLACall().getCallId());
        commandCallListener.onComplete(callCompleted);
        return result;
    }

    private String executeUnobservableCommand() {
        return (this.executeCall(this.baseCall.getCallId(), this.baseCall.getParameters()));
    }

    private String executeObservableCommand() {
        final SiLAFramework.CommandConfirmation.Builder command = SiLAFramework.CommandConfirmation.newBuilder();

        String commandId;
        try {
            JsonFormat.parser().merge(this.executeCall(this.baseCall.getCallId(), this.baseCall.getParameters()), command);
            commandId = ProtoMapper.serializeToJson(command.getCommandExecutionUUID());
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException("Received a malformed message");
        }
        final AtomicInteger commandStatus = new AtomicInteger(SiLAFramework.ExecutionInfo.CommandStatus.waiting_VALUE);
        final CompletableFuture<List<String>> future = this.executeStream(
                GrpcNameMapper.getStateCommand(this.baseCall.getCallId()),
                commandId,
                message -> {
                    try {
                        final SiLAFramework.ExecutionInfo.Builder stateBuilder = SiLAFramework.ExecutionInfo.newBuilder();
                        JsonFormat.parser().merge(message, stateBuilder);
                        log.info("Received status for call " + this.baseCall.getCallId());
                        log.info(stateBuilder.toString());

                        commandStatus.set(stateBuilder.getCommandStatus().getNumber());

                        return (
                                commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.running_VALUE ||
                                commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.waiting_VALUE
                        );

                    } catch (final InvalidProtocolBufferException e) {
                        log.warn("Received a malformed message: ", e);
                        return (false);
                    }
                }
        );
        callFuture(future, null);

        if (commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully_VALUE) {
            return (this.executeCall(GrpcNameMapper.getResult(this.baseCall.getCallId()), commandId));
        } else {
            // An exception should be raised with the cause of the error
            this.executeCall(GrpcNameMapper.getResult(this.baseCall.getCallId()), commandId);

            // If no exception is raised or is not a valid SiLA Error, raise one
            throw new RuntimeException("Command finished with an error without further information!");
        }
    }

    private String getUnobservableProperty() {
        return (this.executeCall(GrpcNameMapper.getUnobservableProperty(this.baseCall.getCallId()), this.baseCall.getParameters()));
    }

    private String getObservableProperty() {
        CompletableFuture<List<String>> future = this.executeStream(
                GrpcNameMapper.getObservableProperty(this.baseCall.getCallId()),
                this.baseCall.getParameters(),
                message -> false
        );
        List<String> results = callFuture(future, null);
        if (results.isEmpty()) {
            throw new RuntimeException("No result");
        } else {
            return (results.get(results.size() - 1));
        }
    }

    /**
     * Private utility to call a future and employ the proper error handling
     */
    private <T> T callFuture(
            @NonNull final Future<T> future,
            @Nullable final Consumer<String> errorConsumer
    ) {
        try {
            return future.get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } catch (Throwable e) {
            // Completable future has the cause wrapped
            Throwable relevantThrowable = e;
            if (relevantThrowable.getCause() != null) {
                relevantThrowable = relevantThrowable.getCause();
            }

            final String errorMessage = ExceptionGeneration.generateMessage(relevantThrowable, this.timeout);

            if (errorConsumer != null) {
                errorConsumer.accept(errorMessage);
            }

            throw new RuntimeException(errorMessage);
        }
    }

    /**
     * Simple Unary gRPC Call with given Service Id and parameters
     * @param callId Service Name provided in the proto definition
     * @param params Protobuf Parameters in JSON Format
     * @return Protobuf Response in JSON Format
     */
    private String executeCall(@NonNull final String callId, @NonNull final String params) {
        try {
            final Descriptors.MethodDescriptor method = this.feature.findMethodByName(callId);
            if (method == null) {
                throw new RuntimeException("Server " + this.baseCall.getServerId() + " doesn't expose call to " + callId);
            }
            final DynamicMessage request = ServerCallExecutor.getRequestMessage(method, params);
            final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);

            final DynamicMessage unaryCall = (DynamicMessage) ClientCalls.blockingUnaryCall(
                    connection.getManagedChannel().newCall(
                            methodDescriptor,
                            CallOptions.DEFAULT.withDeadlineAfter(timeout.getSeconds(), TimeUnit.SECONDS)
                    ),
                    request
            );

            return ProtoMapper.serializeToJson(unaryCall);
        } catch (Throwable e) {
            throw new RuntimeException(ExceptionGeneration.generateMessage(e, this.timeout));
        }
    }

    /**
     * Calling Server Side Streaming
     * @param callId Service Name provided in the proto definition
     * @param params Protobuf Parameters in JSON Format
     * @param callback Callback to consume Streaming messages
     * @return Future to get a list of JSON Responses
     */
    private CompletableFuture<List<String>> executeStream(
            @NonNull final String callId,
            @NonNull final String params,
            @Nullable final StaticStreamObserver.StreamCallback callback
    ) {
        final Descriptors.MethodDescriptor method = this.feature.findMethodByName(callId);
        if (method == null) {
            throw new RuntimeException("Server " + this.baseCall.getServerId() + " doesn't expose call to " + callId);
        }

        final DynamicMessage request = ServerCallExecutor.getRequestMessage(method, params);
        final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);
        final ClientCall<Object, Object> clientCall = this.connection.getManagedChannel().newCall(
                methodDescriptor,
                CallOptions.DEFAULT.withDeadlineAfter(timeout.getSeconds(), TimeUnit.SECONDS)
        );

        final StaticStreamObserver propertyObserver = new StaticStreamObserver(clientCall, callback);

        ClientCalls.asyncServerStreamingCall(clientCall, request, propertyObserver);
        return (propertyObserver.getFuture());
    }

    // Private Helpers to retrieve dynamic protobuf constructs
    private static MethodDescriptor<Object, Object> getMethodDescriptor(
            @NonNull final Descriptors.MethodDescriptor method
    ) {
        return (MethodDescriptor
                .newBuilder()
                .setType(MethodDescriptor.MethodType.UNARY)
                .setFullMethodName(getFullMethodName(method))
                .setRequestMarshaller(new DynamicMessageMarshaller(method.getInputType()))
                .setResponseMarshaller(new DynamicMessageMarshaller(method.getOutputType()))
                .build()
        );
    }

    private static String getFullMethodName(@NonNull final Descriptors.MethodDescriptor method) {
        return (MethodDescriptor.generateFullMethodName(
                method.getService().getFullName(),
                method.getName())
        );
    }

    private static DynamicMessage getRequestMessage(
            @NonNull final Descriptors.MethodDescriptor method,
            @NonNull final String params
    ) {
        final DynamicMessage.Builder parBuilder = DynamicMessage.newBuilder(method.getInputType());

        try {
            JsonFormat.parser().merge(params, parBuilder);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        return (parBuilder.build());
    }
}
