package sila_java.library.manager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Duration;
import java.time.OffsetDateTime;

@Getter
@AllArgsConstructor
public class CallInProgress {
    private final OffsetDateTime startDate;
    private final Duration timeout;
    private final SiLACall siLACall;
}
