package sila_java.library.manager;

import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.core.discovery.SiLADiscovery;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.executor.CommandCallListener;
import sila_java.library.manager.executor.ServerCallExecutor;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.manager.server_management.Connection;
import sila_java.library.manager.server_management.ServerConnectionException;
import sila_java.library.manager.server_management.ServerLoading;

import javax.annotation.Nullable;
import java.security.KeyException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;

import static sila_java.library.manager.server_management.ServerLoading.*;

/**
 * Singleton Manager to manage SiLA Servers
 *
 * This Manager will keep a cache of all servers added either through discovery or via invoking
 * the add function by other means (e.g. manually). It will also keep track of their Status with a
 * Heartbeat.
 */
@Slf4j
public class ServerManager implements AutoCloseable {
    private static final int MAX_SERVICE_TIMEOUT = 3; // [s]
    private static ServerManager instance;
    @Getter
    private final SiLADiscovery discovery = new SiLADiscovery();
    private final Map<UUID, Connection> connections = new ConcurrentHashMap<>();
    private final Map<UUID, Server> servers = new ConcurrentHashMap<>();
    private final List<ServerListener> serverListenerList = new CopyOnWriteArrayList<>();

    /**
     * Create a SiLAManager instance if not already present and returns it.
     * @throws RuntimeException if unable to build SslContext during first call
     * @return The single manager instance
     */
    public static ServerManager getInstance() {
        synchronized (ServerManager.class) {
            if (instance == null) {
                instance = new ServerManager();
            }
        }
        return instance;
    }

    private ServerManager() {
        discovery.addListener((instanceId, host, port) -> {
            try {
                final Server server = this.servers.get(instanceId);
                if (server != null && (server.getStatus() == Server.Status.ONLINE)) {
                    log.debug("Discovery found server with uuid: " + instanceId);
                } else {
                    ServerManager.getInstance().addServer(host, port);
                }
            } catch (final ServerAdditionException e) {
                log.warn(
                        "Server with instance id: '{}' was not added because the following error occurred: '{}'",
                        instanceId,
                        e.getMessage()
                );
            }
        });
    }

    /**
     * Initialise SiLA Manager with pre-defined SiLA Servers, e.g. from a persistence layer
     * It will also set the servers status in the map to offline
     * @param silaServers Map containing all predefined SiLAServers
     */
    public void initialize(@NonNull final Map<UUID, Server> silaServers) {
        if (!this.connections.isEmpty() || !this.servers.isEmpty()) {
            throw new IllegalStateException("SiLA Manager can only be initialised in an empty state");
        }
        log.info("Initializing SiLA Manager with " + silaServers.size() + " servers");

        silaServers.forEach((uuid, server) -> {
            server.setStatus(Server.Status.OFFLINE);
            final ManagedChannel managedChannel =
                    server.getNegotiationType().equals(Server.NegotiationType.TLS) ?
                            ChannelFactory.withEncryption(server.getHost(), server.getPort()) :
                            ChannelFactory.withoutEncryption(server.getHost(), server.getPort());
            final Connection connection;
            try {
                connection = new Connection(server, managedChannel);
            } catch (final MalformedSiLAFeature e) {
                managedChannel.shutdownNow();
                return;
            }
            this.servers.put(uuid, server);
            this.connections.put(uuid, connection);
            connection.attachAndTriggerListener(this::updateServerState);
        });
        log.info("Initialization complete");
    }

    /**
     * Clear any servers from manager
     */
    public void clear() {
        this.servers.keySet().forEach(this::removeServer);
        this.discovery.clearCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        this.discovery.close();
        this.connections.values().forEach(Connection::close);
        this.connections.clear();
        this.servers.clear();
        this.serverListenerList.clear();
        instance = null;
    }

    /**
     * Removing Server manually
     *
     * @param id Server Id used for referencing to the Server
     */
    public synchronized void removeServer(@NonNull final UUID id) {
        final Connection connection = connections.get(id);
        final Server server = servers.get(id);

        if (connection == null || server == null) {
            log.warn("Server with id: " + id + " already removed from cache.");
            return;
        }

        // Inform listeners of server removal
        serverListenerList.forEach(listener -> listener.onServerRemoved(id, server));

        // First close connection to stop channel updates
        connections.remove(id);
        connection.close();
        servers.remove(id);

        log.info("[removeServer] removed serverId={}", id);
    }

    /**
     * Add Server to the manager
     *
     * @param host Host on which SiLA Server is exposed
     * @param port Port on which SiLA Server is exposed
     * @throws ServerAdditionException if unable to add the server
     */
    public synchronized void addServer(@NonNull final String host, final int port) throws ServerAdditionException {
        // Create Server
        final Server server = new Server();
        server.setJoined(new Date());
        server.setHost(host);
        server.setPort(port);
        server.setStatus(Server.Status.ONLINE);

        try {
            addServer(server);
        } catch (final ServerAdditionException e) {
            serverListenerList.forEach(listener -> listener.onServerAdditionFail(host, port, e.getMessage()));
            throw e;
        }
    }

    /**
     * Add Server
     *
     * @param server the server to add
     * @throws ServerAdditionException if unable to add the server
     */
    private void addServer(@NonNull final Server server) throws ServerAdditionException {
        // Establish Connection
        final ManagedChannel managedChannel;
        // Load SiLA Server
        try {
            managedChannel = ServerLoading.attemptConnectionWithServer(server);
        } catch (final ServerConnectionException e) {
            throw new ServerAdditionException(server, e.getMessage());
        }
        try {
            loadServer(server, managedChannel);
        } catch (final ServerLoadingException e) {
            managedChannel.shutdownNow();
            throw new ServerAdditionException(server, e.getMessage());
        }

        log.info(
                "[addServer] Resolved SiLA Server serverName={} on {}:{}",
                server.getConfiguration().getName(),
                server.getHost(),
                server.getPort()
        );

        // Protect against re-adding when Online
        final UUID uuid = server.getConfiguration().getUuid();
        final Server serverInManager = servers.get(uuid);
        final boolean isNewServer = (serverInManager == null);
        if (!isNewServer && (serverInManager.getStatus() == Server.Status.ONLINE)) {
            log.warn("Server with id `{}` is already present in the manager and online! ", uuid);
            managedChannel.shutdownNow();
            throw new ServerAdditionException(
                    server,
                    "Server with id [" + uuid + "] is already in the manager and online"
            );
        }

        // Add Server and notify when successful resolving
        final Connection connection;
        try {
            connection = new Connection(server, managedChannel);
        } catch (final MalformedSiLAFeature e) {
            managedChannel.shutdownNow();
            throw new ServerAdditionException(server, e.getMessage());
        }

        servers.put(uuid, server);
        final Connection previousConnection = connections.put(uuid, connection);
        if (previousConnection != null) {
            previousConnection.close();
        }
        if (isNewServer) {
            serverListenerList.forEach(listener -> listener.onServerAdded(uuid, server));
        } else {
            serverListenerList.forEach(listener -> listener.onServerChange(uuid, server));
        }
        connection.attachAndTriggerListener(this::updateServerState);
    }

    /**
     * Private Utility to check and update the state of the server
     * Attempt to execute a call to retrieve the UUID of the server.
     * If the call succeed and the UUID match, the status of server will be set to online, offline otherwise.
     *
     * @param serverId The server unique identifier
     * @param channel The server channel
     */
    private void updateServerState(@NonNull final UUID serverId, @NonNull final ManagedChannel channel) {
        final Server server = this.servers.get(serverId);
        if (server == null) {
            log.warn("Unable to update server state because server {} does not exist", serverId);
            return;
        }
        final ConnectivityState state = channel.getState(false);
        if (state == ConnectivityState.IDLE) {
            CompletableFuture.runAsync(() -> {
                Server.Status status = Server.Status.OFFLINE;
                try {
                    final UUID uuid = getServerId(SiLAServiceGrpc.newBlockingStub(channel));
                    if (server.getConfiguration().getUuid().equals(uuid)) {
                        status = Server.Status.ONLINE;
                    } else {
                        log.debug(
                                "GetServerId returned by server UUID: {} differ from the one in manager {}",
                                uuid,
                                server.getConfiguration().getUuid()
                        );
                    }
                } catch (final Exception e) {
                    log.debug("Failed UUID Retrieval {}", e.getMessage());
                }
                this.setServerStatus(serverId, status);
            });
        }
    }

    /**
     * Set the Status of a Server
     * @param id Server Id
     * @param status Server Status
     */
    private void setServerStatus(@NonNull final UUID id, @NonNull final Server.Status status) {
        final Connection siLAConnection = this.connections.get(id);

        if (siLAConnection == null) {
            log.warn("Server with id " + id + " doesn't exist.");
            return;
        }

        final Server server = this.servers.get(id);

        // Only Notify Change, if Change happened
        if (!server.getStatus().equals(status)) {
            server.setStatus(status);
            log.info("[changeServerStatus] change {} status to {}", id, status.toString());
            // also update listeners
            serverListenerList.forEach(listener -> listener.onServerChange(id, server));
        }
    }

    /**
     * Set the Status of a Server
     *
     * @implNote serverNameResponse has no response at the moment, as such
     * we proceed assuming the setServerName was successful and catching RcpException at application level
     * alternatively, wrap this function call with try {} catch(RpcException e) {}
     *
     * @param id Server Id
     * @param newServerName String Server Name
     */
    public void setServerName(@NonNull final UUID id, @NonNull final String newServerName) {
        final Connection connection = this.connections.get(id);

        if (connection == null) {
            log.warn("Server with id " + id + " doesn't exit.");
            return;
        }

        final Server server = this.servers.get(id);
        if (!server.getConfiguration().getName().equals(newServerName)) {
            log.info("[setServerName] server name changed to {}", newServerName);
            @NonNull final ManagedChannel managedChannel = connection.getManagedChannel();
            @NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAServiceBlockingStub = SiLAServiceGrpc
                    .newBlockingStub(managedChannel);

            siLAServiceBlockingStub
                    .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .setServerName(SiLAServiceOuterClass.SetServerName_Parameters.newBuilder().setServerName(
                            SiLAString.from(newServerName)).build()
                    );
            // check for updated name on server
            final String serverName = getServerName(siLAServiceBlockingStub);
            if (serverName.equals(newServerName)) {
                log.info("[setServerName] server name changed to {}", newServerName);
                // update server prior to updating through the listener
                server.setConfiguration(server.getConfiguration().withName(newServerName));
                serverListenerList.forEach(listener -> listener.onServerChange(id, server));
            } else {
                throw new IllegalStateException("Server Name was not updated on SiLA Server.");
            }
        }
    }

    /**
     * Get the map of servers
     *
     * @implNote Synchronised call because servers might be in removal state
     *
     * @return A read only map of servers
     */
    public synchronized Map<UUID, Server> getServers() {
        return Collections.unmodifiableMap(this.servers);
    }

    /**
     * Add additional listener to retrieve SiLA Server information in-process
     */
    public void addServerListener(@NonNull final ServerListener siLAServerListener) {
        serverListenerList.add(siLAServerListener);
    }

    public void removeServerListener(@NonNull final ServerListener siLAServerListener) {
        serverListenerList.remove(siLAServerListener);
    }

    /**
     * Create a Server Call Executor with default options
     */
    public ServerCallExecutor newCallExecutor(@NonNull final SiLACall siLACall) {
        return ServerCallExecutor.newBuilder(connections.get(siLACall.getServerId()), siLACall).build();
    }

    /**
     * Create a Server Call Executor Builder from a SiLA Call defining it
     */
    public ServerCallExecutor.Builder newCallExecutorBuilder(@NonNull final SiLACall siLACall) {
        return ServerCallExecutor.newBuilder(connections.get(siLACall.getServerId()), siLACall);
    }
}
