package sila_java.library.core.encryption;

import io.grpc.netty.GrpcSslContexts;
import io.netty.handler.ssl.SslContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.net.ssl.SSLException;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.Base64;

import static io.netty.handler.ssl.util.InsecureTrustManagerFactory.INSTANCE;

/**
 * Internal utility functions.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class EncryptionUtils {
    private static final String CERTIFICATE_BEGIN = "-----BEGIN CERTIFICATE-----";
    private static final String CERTIFICATE_END = "-----END CERTIFICATE-----";
    private static final String RSA_PRIVATE_KEY_BEGIN = "-----BEGIN RSA PRIVATE KEY-----";
    private static final String RSA_PRIVATE_KEY_END = "-----END RSA PRIVATE KEY-----";

    private static final TrustManagerFactory insecureTrustManager = INSTANCE;

    /**
     * Create SSL Context for gRPC Channels to accept all certificates
     */

    public static SslContext buildTLSContextNoCert() throws SSLException {
        return GrpcSslContexts.forClient().trustManager(insecureTrustManager).build();
    }

    /**
     * Provides full content of a crt like file.
     */
    public static InputStream certificateToStream(@NonNull final X509Certificate certificate)
            throws CertificateEncodingException {
        final String cert = new String(Base64.getEncoder().encode(certificate.getEncoded()));
        return new ByteArrayInputStream((CERTIFICATE_BEGIN + '\n' + cert + '\n' + CERTIFICATE_END).getBytes());
    }

    /**
     * Provides full content of a pem/key like file
     */
    public static InputStream keyToStream(@NonNull final PrivateKey privateKey) {
        final String key = new String(Base64.getEncoder().encode(privateKey.getEncoded()));
        return new ByteArrayInputStream((RSA_PRIVATE_KEY_BEGIN + '\n' + key + '\n' + RSA_PRIVATE_KEY_END).getBytes());
    }
}
