package sila_java.library.core.discovery.networking.dns.records;

import lombok.Getter;
import lombok.NonNull;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 * The record A specifies IP address (IPv4) for given host.
 * A records are used for conversion of domain names to corresponding IP addresses.
 */
@Getter
public class ARecord extends Record {
    private final InetAddress address;

    public ARecord(
            @NonNull final ByteBuffer buffer,
            @NonNull final String name,
            final long ttl
    ) throws UnknownHostException {
        super(name, ttl);
        final byte[] addressBytes = new byte[4];
        buffer.get(addressBytes);
        address = InetAddress.getByAddress(addressBytes);
    }

    @Override
    public String toString() {
        return "ARecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", address=" + address +
                '}';
    }
}
