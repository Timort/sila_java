package sila_java.library.core.asynchronous;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.*;

/**
 * Simple MethodPoller Utility to wait until a condition is met
 *
 * @implNote Default is 1 Second Interval forever
 */
@Slf4j
public class MethodPoller {
    private final static ExecutorService executor = Executors.newCachedThreadPool(r -> {
        final Thread t = Executors.defaultThreadFactory().newThread(r);
        t.setDaemon(true);
        return t;
    });
    private Duration pollInterval = Duration.ofSeconds(1);
    private Duration maxDuration = Duration.ofSeconds(Long.MAX_VALUE);

    public static MethodPoller await() {
        return new MethodPoller();
    }

    public MethodPoller withInterval(@NonNull Duration pollInterval) {
        this.pollInterval = pollInterval;
        return this;
    }

    public MethodPoller atMost(@NonNull Duration maxDuration) {
        this.maxDuration = maxDuration;
        return this;
    }

    public void until(@NonNull Callable<Boolean> conditionEvaluator) throws ExecutionException, TimeoutException {
        this.execute(
                executor.submit(
                        ()->{
                            while(!conditionEvaluator.call()) {
                                Thread.sleep(pollInterval.toMillis());
                                log.trace("Checking condition evaluator...");
                            }
                            return null;
                        }
                )
        );
    }

    public <T> T untilPresent(@NonNull Callable<Optional<T>> valueProvider) throws ExecutionException, TimeoutException {
        return this.execute(
                executor.submit(
                        ()->{
                            Optional<T> value = Optional.empty();

                            while(!value.isPresent()) {
                                value = valueProvider.call();
                                Thread.sleep(pollInterval.toMillis());
                                log.trace("Checking value provider...");
                            }

                            return value.get();
                        }
                )
        );
    }

    private <T> T execute(@NonNull Future<T> future) throws ExecutionException, TimeoutException {
        try {
            return future.get(maxDuration.toMillis(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } catch (TimeoutException e) {
            future.cancel(true);
            if (!future.isCancelled()) {
                throw new ExecutionException("Condition Evaluator can not be cancelled", e.getCause());
            }
            throw e;
        }
    }
}
