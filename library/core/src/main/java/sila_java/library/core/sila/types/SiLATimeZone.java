package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.ZoneOffset;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLATimeZone {
    public static SiLAFramework.Timezone from(final ZoneOffset zoneOffset) {
        final int totalOffsetSeconds = zoneOffset.getTotalSeconds();
        final int offsetMinutes = (totalOffsetSeconds / 60) % 60;
        final int offsetHours = (totalOffsetSeconds / 3600);
        return SiLAFramework.Timezone.newBuilder().setHours(offsetHours).setMinutes(offsetMinutes).build();
    }
}
