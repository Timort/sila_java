package sila_java.library.core.sila.clients;

import io.grpc.ManagedChannel;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * Custom Channel Factory utility
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ChannelFactory {
    /**
     * Build a new gRPC Channel instance related to a SiLA Server
     *
     * @param host the SiLA Server host
     * @param port the SiLA Server port
     * @return A new channel connected to Server
     */
    public static ManagedChannel withoutEncryption(@NonNull final String host, final int port) {
        return ChannelBuilder.builderWithoutEncryption(host, port).build();
    }

    /**
     * Build a new gRPC (Netty) Channel instance with TLS related to a SiLA Server
     * @param host the SiLA Server host
     * @param port the SiLA Server port
     * @return A new channel connected to Server
     *
     * @implNote Accepts untrusted certificates from the server
     */
    public static ManagedChannel withEncryption(@NonNull final String host, final int port) {
        return ChannelBuilder.builderWithEncryption(host, port).build();
    }
}
