package sila_java.library.core.discovery.networking.dns.records;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
public abstract class Record {
    public final static int USHORT_MASK = 0xFFFF;
    private final static long UINT_MASK = 0xFFFFFFFFL;
    private final static String NAME_CHARSET = "UTF-8";
    protected final String name;
    protected final long ttl;

    private enum Type {
        UNSUPPORTED(0),
        A(1),
        NS(2),
        CNAME(5),
        SOA(6),
        NULL(10),
        WKS(11),
        PTR(12),
        HINFO(13),
        MINFO(14),
        MX(15),
        TXT(16),
        AAAA(28),
        SRV(33);

        private final int value;

        public static Type fromInt(final int val) {
            for (final Type type : values()) {
                if (type.value == val) {
                    return type;
                }
            }
            return UNSUPPORTED;
        }

        Type(final int value) {
            this.value = value;
        }
    }

    protected Record(@NonNull final String name, final long ttl) {
        this.name = name;
        this.ttl = ttl;
    }

    /**
     * Create DNS Record from Bytebuffer
     *
     * @implNote Currently supported records: A, AAAA, PTR, SRV, TXT
     *
     * @param buffer BytBuffer containing type, buffer, class and record specific information
     *
     * @return Specific Record, UnknownRecord if not supported
     */
    public static Record fromBuffer(@NonNull final ByteBuffer buffer) {
        final String name = readNameFromBuffer(buffer);
        final Type type = Type.fromInt(buffer.getShort() & USHORT_MASK);

        // Note: We don't use DNS Classes for now
        buffer.getShort();

        final long ttl = buffer.getInt() & UINT_MASK;
        final int rdLength = buffer.getShort() & USHORT_MASK;

        switch (type) {
            case A:
                try {
                    return new ARecord(buffer, name, ttl);
                } catch (UnknownHostException e) {
                    throw new IllegalArgumentException("Buffer does not represent a valid A record");
                }
            case AAAA:
                try {
                    return new AaaaRecord(buffer, name, ttl);
                } catch (UnknownHostException e) {
                    throw new IllegalArgumentException("Buffer does not represent a valid AAAA record");
                }
            case PTR:
                return new PtrRecord(buffer, name, ttl, rdLength);
            case SRV:
                return new SrvRecord(buffer, name, ttl);
            case TXT:
                return new TxtRecord(buffer, name, ttl, rdLength);
            default:
                return new UnknownRecord(name, ttl);
        }
    }

    public static String readNameFromBuffer(@NonNull final ByteBuffer buffer) {
        final List<String> labels = new ArrayList<>();
        int labelLength;
        int continueFrom = -1;

        try {
            do {
                buffer.mark();
                labelLength = buffer.get() & 0xFF;
                if (isPointer(labelLength)) {
                    buffer.reset();
                    int offset = buffer.getShort() & 0x3FFF;
                    if (continueFrom < 0) {
                        continueFrom = buffer.position();
                    }
                    buffer.position(offset);
                } else {
                    String label = readLabel(buffer, labelLength);
                    labels.add(label);
                }
            } while (labelLength != 0);
        } catch (final BufferUnderflowException | IllegalArgumentException e) {
            log.debug(e.getMessage());
        }
        if (continueFrom >= 0) {
            buffer.position(continueFrom);
        }

        return String.join(".", labels);
    }

    public static List<String> readStringsFromBuffer(@NonNull final ByteBuffer buffer, @NonNull final int length) {
        final List<String> strings = new ArrayList<>();
        int bytesRead = 0;
        do {
            int stringLength = buffer.get() & 0xFF;
            String label = readLabel(buffer, stringLength);
            bytesRead += label.length() + 1;
            strings.add(label);
        } while (bytesRead < length);
        return strings;
    }

    private static boolean isPointer(final int octet) {
        return (octet & 0xC0) == 0xC0;
    }

    private static String readLabel(@NonNull final ByteBuffer buffer, @NonNull final int length) {
        if (length > 0) {
            final byte[] labelBuffer = new byte[length];
            buffer.get(labelBuffer);
            try {
                return new String(labelBuffer, NAME_CHARSET);
            } catch (final UnsupportedEncodingException e) {
                log.warn("UnsupportedEncoding: " + e);
            }
        }
        return "";
    }

    @Override
    public String toString() {
        return "Record{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                '}';
    }
}
