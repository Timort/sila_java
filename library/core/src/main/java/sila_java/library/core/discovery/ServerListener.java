package sila_java.library.core.discovery;


import java.util.UUID;

/**
 * Listener interface that gets notified when a SiLA Server is added to mdns.
 *
 * @implNote The query of the server actually being active has to be done on a higher level.
 */
public interface ServerListener {
    /**
     * Server added, note that the same Server might be added several times
     * @param instanceId Service Instance ID, SiLA Server UUID for SiLA Servers
     * @param host Host of Service
     * @param port Port of service
     */
    void serverAdded(UUID instanceId, String host, int port);
}
