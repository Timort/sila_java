package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.OffsetDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLADate {
    public static SiLAFramework.Date from(final OffsetDateTime offsetDateTime) {
        return SiLAFramework.Date
                .newBuilder()
                .setTimezone(SiLATimeZone.from(offsetDateTime.getOffset()))
                .setYear(offsetDateTime.getYear())
                .setMonth(offsetDateTime.getMonthValue())
                .setDay(offsetDateTime.getDayOfMonth())
                .build();
    }
}
