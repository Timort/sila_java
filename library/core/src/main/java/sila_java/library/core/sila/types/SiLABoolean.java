package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLABoolean {
    public static SiLAFramework.Boolean from(final boolean bool) {
        return SiLAFramework.Boolean.newBuilder().setValue(bool).build();
    }
}
