package sila_java.library.core.encryption;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pqc.jcajce.spec.SPHINCS256KeyGenParameterSpec;
import org.bouncycastle.pqc.jcajce.spec.XMSSMTParameterSpec;
import org.bouncycastle.pqc.jcajce.spec.XMSSParameterSpec;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Date;

@Slf4j
@Getter
public class SelfSignedCertificate {
    private static final String CN = "0.0.0.0"; // ip or domain certified

    private final X509Certificate certificate;
    private final PrivateKey privateKey;

    public enum EncryptionAlgorithm {
        XMSS, XMSSMT, SPHINCS256, RSA
    }

    public enum DigestAlgorithm {
        SHA256, SHA512
    }

    public enum KeySize {
        SIZE_1024(1024),
        SIZE_4096(4096);

        public final int value;

        KeySize(final int value) {
            this.value = value;
        }
    }

    /**
     * Used to encapsulate another exception linked to the generation of a self signed certificate
     */
    public static class CertificateGenerationException extends Exception {

        /**
         * Constructor
         * @param cause The exception to encapsulate
         */
        public CertificateGenerationException(@NonNull final Throwable cause) {
            super(cause);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private EncryptionAlgorithm encryptionAlgorithm = EncryptionAlgorithm.RSA;
        private DigestAlgorithm digestAlgorithm = DigestAlgorithm.SHA512;
        private KeySize keySize = KeySize.SIZE_4096;
        private int days = 365;
        private String payload = "SiLA Standard now allows an encryption layer.";

        /**
         * Set the encryption algorithm to use
         * @param encryptionAlgorithm The encryption algorithm
         * @return The builder instance
         */
        public Builder withEncryptionAlgorithm(@NonNull final EncryptionAlgorithm encryptionAlgorithm) {
            this.encryptionAlgorithm = encryptionAlgorithm;
            return this;
        }

        /**
         * Set the digest algorithm to use
         * @param digestAlgorithm The digest algorithm
         * @return The builder instance
         */
        public Builder withDigestAlgorithm(@NonNull final DigestAlgorithm digestAlgorithm) {
            this.digestAlgorithm = digestAlgorithm;
            return this;
        }

        /**
         * Set the size of the key
         * @param keySize The key size
         * @return The builder instance
         */
        public Builder withKeySize(@NonNull final KeySize keySize) {
            this.keySize = keySize;
            return this;
        }

        /**
         * Set the number of days the certificate will be valid
         * @param days The number of days
         * @return The builder instance
         */
        public Builder withDays(final int days) {
            this.days = days;
            return this;
        }

        /**
         * Set the certificate signature payload
         * @param payload The payload
         * @return The builder instance
         */
        public Builder withPayload(@NonNull final String payload) {
            this.payload = payload;
            return this;
        }

        /**
         * Build a new SelfSignedCertificate instance
         * @return A new SelfSignedCertificate instance
         * @throws CertificateGenerationException if an exception during the creation of the certificate
         */
        public SelfSignedCertificate build() throws CertificateGenerationException {
            return new SelfSignedCertificate(
                    this.encryptionAlgorithm,
                    this.digestAlgorithm,
                    this.keySize,
                    this.days,
                    this.payload
            );
        }
    }

    private static void initialize(
            @NonNull final KeyPairGenerator kpg,
            @NonNull final EncryptionAlgorithm alg,
            @NonNull final DigestAlgorithm digest,
            @NonNull final KeySize keySize
    ) throws InvalidAlgorithmParameterException {
        switch (alg) {
            case XMSS:
                kpg.initialize(new XMSSParameterSpec(4, digest.toString()));
                break;
            case XMSSMT:
                kpg.initialize(new XMSSMTParameterSpec(4, 2, digest.toString()));
                break;
            case SPHINCS256:
                kpg.initialize(new SPHINCS256KeyGenParameterSpec());
                break;
            case RSA:
                kpg.initialize(new RSAKeyGenParameterSpec(keySize.value, new BigInteger("5")));
                break;
            default:
                throw new InvalidAlgorithmParameterException("Cannot initialize with specified algorithm: " + alg);
        }
    }

    private static void sign(
            @NonNull final PrivateKey pk,
            @NonNull final String sigAlg,
            @NonNull final String provider,
            @NonNull final String payload
    ) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        final Signature signer = Signature.getInstance(sigAlg, provider);
        signer.initSign(pk);
        signer.update(payload.getBytes());
        signer.sign();
        log.info("Successfully signed");
    }

    private static X509Certificate generate(
            @NonNull final PrivateKey privKey,
            @NonNull final PublicKey pubKey,
            @NonNull final String sigAlg,
            final int days,
            final boolean isSelfSigned
    ) throws OperatorCreationException, CertificateException, NoSuchProviderException, NoSuchAlgorithmException,
            InvalidKeyException, SignatureException
    {
        final Provider bouncyCastle = new BouncyCastleProvider();
        // distinguished name table.
        final X500NameBuilder subjectBuilder = createSubjectBuilder();
        // create the certificate
        final ContentSigner sigGen = new JcaContentSignerBuilder(sigAlg).build(privKey);
        final X509v3CertificateBuilder certGen = new JcaX509v3CertificateBuilder(
                new X500Name("cn=" + CN), // Issuer
                BigInteger.ONE,
                new Date(System.currentTimeMillis() - 50000), // Valid from
                new Date((System.currentTimeMillis() + days * 24 * 60 * 60)), // Valid to
                subjectBuilder.build(), // Subject
                pubKey // PublicKey
        );
        final X509Certificate cert = new JcaX509CertificateConverter()
                .setProvider(bouncyCastle)
                .getCertificate(certGen.build(sigGen));
        cert.checkValidity(new Date());
        if (isSelfSigned) {
            // check verifies in general
            cert.verify(pubKey);
            // check verifies with contained key
            cert.verify(cert.getPublicKey());
        }
        final ByteArrayInputStream bIn = new ByteArrayInputStream(cert.getEncoded());
        final CertificateFactory fact = CertificateFactory.getInstance("X.509", bouncyCastle);
        log.info("Certificate constructed and valid for " + days + " days.");
        return (X509Certificate) fact.generateCertificate(bIn);
    }

    private static X500NameBuilder createSubjectBuilder() {
        final X500NameBuilder builder = new X500NameBuilder(RFC4519Style.INSTANCE);
        builder.addRDN(RFC4519Style.c, "CH") // Country
                .addRDN(RFC4519Style.o, "SiLA Standard") // Organisation
                .addRDN(RFC4519Style.l, "BS") // Location
                .addRDN(RFC4519Style.st, "Basel"); // State
        return builder;
    }

    /**
     * Create a new self signed certificate
     *
     * @param alg The encryption algorithm to use
     * @param digest The digest algorithm to use
     * @param keySize The size of the key
     * @param days The days of validity of the certificate
     * @param payload The signature payload
     *
     * @throws CertificateGenerationException if one of the following exception occur:
     * CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException,
     * InvalidAlgorithmParameterException, OperatorCreationException, NoSuchProviderException
     */
    private SelfSignedCertificate(
            @NonNull final EncryptionAlgorithm alg,
            @NonNull final DigestAlgorithm digest,
            @NonNull final KeySize keySize,
            final int days,
            @NonNull final String payload
    ) throws CertificateGenerationException {
        final String sigAlg = digest + "with" + alg;
        final String provider = "BC";
        // add providers
        Security.addProvider(new BouncyCastleProvider()); // because PROVIDER = "BC"

        try {
            // create keypair
            final KeyPairGenerator kpg = KeyPairGenerator.getInstance(alg.toString(), provider);
            initialize(kpg, alg, digest, keySize);
            final KeyPair myKp = kpg.generateKeyPair();

            // sign
            sign(myKp.getPrivate(), sigAlg, provider, payload);

            this.privateKey = myKp.getPrivate();
            final PublicKey publicKey = myKp.getPublic();
            this.certificate = generate(this.privateKey, publicKey, sigAlg, days, true);
        } catch (final CertificateException
                | NoSuchAlgorithmException
                | InvalidKeyException
                | SignatureException
                | InvalidAlgorithmParameterException
                | OperatorCreationException
                | NoSuchProviderException e
        ) {
            throw new CertificateGenerationException(e);
        }
    }
}