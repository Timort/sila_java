package sila_java.library.core.sila.errors;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila2.org.silastandard.SiLAFramework;

import javax.annotation.Nonnull;
import javax.xml.bind.DatatypeConverter;
import java.util.Optional;

/**
 * Utility Functions to handle SiLA Errors
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAErrors {
    /**
     * Catching a generic Java Throwable and generating a SiLA conforming Execution Error
     */
    public static StatusRuntimeException generateGenericExecutionError(@Nonnull final Throwable e) {
        final SiLAFramework.UndefinedExecutionError.Builder builder = SiLAFramework.UndefinedExecutionError
                .newBuilder()
                .setMessage(
                        e.getClass().getName() + ": " + ((e.getMessage() == null) ? "Unknown reason" : e.getMessage())
                );
        return generateGRPCError(SiLAFramework.SiLAError.newBuilder().setUndefinedExecutionError(builder).build());
    }

    /**
     * Generating gRPC Error from built SiLA Error
     *
     * @param siLAError SiLAError proto message
     * @return StatusRuntime according to SiLA
     */
    private static StatusRuntimeException generateGRPCError(@Nonnull final SiLAFramework.SiLAError siLAError) {
        return new StatusRuntimeException(
                Status.ABORTED.withDescription(DatatypeConverter.printBase64Binary(siLAError.toByteArray()))
        );
    }

    /**
     * Generate a SiLA conforming Validation Error
     * @param parameter The field concerned by the error
     * @param message The error message
     * @return StatusRuntime according to SiLA
     */
    public static StatusRuntimeException generateValidationError(
            @NonNull final String parameter,
            @NonNull final String message
    ) {
        return generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                .setValidationError(SiLAFramework.ValidationError.newBuilder()
                        .setParameter(parameter)
                        .setMessage(message)
                        .build())
                .build());
    }

    /**
     * Generate a SiLA conforming Execution Error
     * @param errorIdentifier The error identifier
     * @param message The error message
     * @return StatusRuntime according to SiLA
     */
    public static StatusRuntimeException generateDefinedExecutionError(
            @NonNull final String errorIdentifier,
            @NonNull final String message
    ) {
        return generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                .setDefinedExecutionError(SiLAFramework.DefinedExecutionError.newBuilder()
                        .setErrorIdentifier(errorIdentifier)
                        .setMessage(message)
                        .build())
                .build());
    }

    /**
     * Generate a SiLA conforming Execution Error
     * @param message The error message
     * @return StatusRuntime according to SiLA
     */
    public static StatusRuntimeException generateUndefinedExecutionError(@NonNull final String message) {
        return generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                .setUndefinedExecutionError(SiLAFramework.UndefinedExecutionError.newBuilder()
                        .setMessage(message)
                        .build())
                .build());
    }

    /**
     * Generate a SiLA conforming Framework Error
     * @param errorType the framework error type
     * @return StatusRuntime according to SiLA
     */
    public static StatusRuntimeException generateFrameworkError(
            @NonNull final SiLAFramework.FrameworkError.ErrorType errorType
    ) {
        return generateGRPCError(SiLAFramework.SiLAError.newBuilder()
                .setFrameworkError(SiLAFramework.FrameworkError
                        .newBuilder()
                        .setErrorType(errorType))
                .build());
    }

    /**
     * Retrieve SiLAError from gRPC Exception
     *
     * @param statusRuntimeException gRPC Exception occuring during gRPC call
     * @return Optional SiLAError if well formed
     */
    public static Optional<SiLAFramework.SiLAError> retrieveSiLAError(
            @Nonnull final StatusRuntimeException statusRuntimeException
    ) {
        final Status status = statusRuntimeException.getStatus();

        if (!status.getCode().equals(Status.Code.ABORTED)) {
            return Optional.empty();
        }

        try {
            final String description = (status.getDescription() == null) ? ("No description") : (status.getDescription());
            return Optional.of(SiLAFramework.SiLAError.parseFrom(DatatypeConverter.parseBase64Binary(description)));
        } catch (InvalidProtocolBufferException e) {
            return Optional.empty();
        }
    }
}
