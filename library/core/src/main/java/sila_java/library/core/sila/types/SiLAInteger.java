package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAInteger {
    public static SiLAFramework.Integer from(final long integer) {
        return SiLAFramework.Integer.newBuilder().setValue(integer).build();
    }
}
