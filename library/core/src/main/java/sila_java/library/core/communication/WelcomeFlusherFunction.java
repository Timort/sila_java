package sila_java.library.core.communication;

import java.io.IOException;

@FunctionalInterface
public interface WelcomeFlusherFunction<E extends IOException> {
    void apply(SynchronousCommunication communication) throws E;
}
