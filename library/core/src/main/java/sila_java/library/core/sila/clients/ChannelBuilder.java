package sila_java.library.core.sila.clients;

import io.grpc.netty.NettyChannelBuilder;
import io.netty.handler.ssl.SslContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.concurrent.TimeUnit;


/**
 * Custom Channel Builder utility
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ChannelBuilder {
    private static final int CHANNEL_IDLE_TIMEOUT_SEC = 5;

    /**
     * Preconfigure a channel builder without encryption
     * @param host the SiLA Server host
     * @param port the SiLA Server port
     * @return a pre configured channel builder without encryption
     */
    public static NettyChannelBuilder builderWithoutEncryption(@NonNull final String host, final int port) {
        return getChannelBuilder(host, port, Optional.empty());
    }

    /**
     * Preconfigure a channel builder with encryption
     * @param host the SiLA Server host
     * @param port the SiLA Server port
     * @return a pre configured channel builder with encryption
     */
    public static NettyChannelBuilder builderWithEncryption(@NonNull final String host, final int port) {
        return getChannelBuilder(host, port, Optional.of(SSLContextFactory.getInstance()));
    }

    private static NettyChannelBuilder getChannelBuilder(
            @NonNull final String host,
            final int port,
            final Optional<SslContext> sslContext
    ) {
        final NettyChannelBuilder channelBuilder =  NettyChannelBuilder
                .forAddress(host, port)
                .idleTimeout(CHANNEL_IDLE_TIMEOUT_SEC, TimeUnit.SECONDS);
        if (sslContext.isPresent()) {
            channelBuilder
                    .sslContext(sslContext.get())
                    .useTransportSecurity();
        } else {
            channelBuilder.usePlaintext();
        }
        return channelBuilder;
    }
}
