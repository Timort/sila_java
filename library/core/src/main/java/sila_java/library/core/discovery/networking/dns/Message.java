package sila_java.library.core.discovery.networking.dns;

import java.nio.ByteBuffer;

import static sila_java.library.core.discovery.networking.dns.records.Record.USHORT_MASK;

public abstract class Message {
    private final static int MAX_LENGTH = 9000; // max size of mDNS packets, in bytes
    protected final ByteBuffer buffer;

    protected Message() {
        buffer = ByteBuffer.allocate(MAX_LENGTH);
    }

    protected int readUnsignedShort() {
        try {
            return buffer.getShort() & USHORT_MASK;
        } catch (final Exception e) {
            return 0;
        }
    }
}
