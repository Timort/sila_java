package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAReal {
    public static SiLAFramework.Real from(final float real) {
        return SiLAReal.from((double)real);
    }

    public static SiLAFramework.Real from(final double real) {
        return SiLAFramework.Real.newBuilder().setValue(real).build();
    }
}
