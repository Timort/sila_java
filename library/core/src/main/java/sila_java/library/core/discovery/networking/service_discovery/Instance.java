package sila_java.library.core.discovery.networking.service_discovery;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.discovery.networking.dns.records.*;

import java.net.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Instance
 */
@Slf4j
@Getter
public class Instance {
    private final String name;
    private final String hostAddress;
    private final int port;
    private final Map<String, String> attributes;
    private final long ttl;

    private Instance(
            @NonNull final String name,
            @NonNull final InetAddress address,
            final int port,
            @NonNull final Map<String, String> attributes,
            final long ttl) {
        this.name = name;
        this.ttl = ttl;
        this.hostAddress = address.getHostAddress();
        this.port = port;
        this.attributes = attributes;
    }

    /**
     * Create Service Instance from a Set of Records
     * @param ptr PTR Record
     * @param records Set of All Records
     * @return an Instance Object
     */
    public static Optional<Instance> createFromRecords(@NonNull final PtrRecord ptr, @NonNull final Set<Record> records) {
        final String name = ptr.getUserVisibleName();
        final int port;
        long ttl;
        List<InetAddress> addresses = new ArrayList<>();
        Map<String, String> attributes = Collections.emptyMap();

        // Check SRV Record and populate values
        final Optional<SrvRecord> srv = records.stream()
                .filter(r -> r instanceof SrvRecord && r.getName().equals(ptr.getPtrName()))
                .map(r -> (SrvRecord) r).findFirst();
        if (srv.isPresent()) {
            ttl = srv.get().getTtl();
            port = srv.get().getPort();
        } else {
            log.debug("Cannot create Instance when no SRV record is available");
            return Optional.empty();
        }

        final Optional<TxtRecord> txt = records.stream()
                .filter(r -> r instanceof TxtRecord && r.getName().equals(ptr.getPtrName()))
                .map(r -> (TxtRecord) r).findFirst();
        if (txt.isPresent()) {
            attributes = txt.get().getAttributes();
            ttl = srv.get().getTtl();
        }

        final Optional<ARecord> aRecord = records.stream()
                .filter(r -> r instanceof ARecord && r.getName().equals(srv.get().getTarget()))
                .map(r -> (ARecord) r).findFirst();
        final Optional<AaaaRecord> aaaRecord = records.stream()
                .filter(r -> r instanceof AaaaRecord && r.getName().equals(srv.get().getTarget()))
                .map(r -> (AaaaRecord) r).findFirst();

        if (aRecord.isPresent()) {
            addresses.addAll(records.stream().filter(r -> r instanceof ARecord)
                    .filter(r -> r.getName().equals(srv.get().getTarget())).map(r -> ((ARecord) r).getAddress())
                    .collect(Collectors.toList()));
        } else if (aaaRecord.isPresent()) {
            addresses.addAll(records.stream().filter(r -> r instanceof AaaaRecord)
                    .filter(r -> r.getName().equals(srv.get().getTarget())).map(r -> ((AaaaRecord) r).getAddress())
                    .collect(Collectors.toList()));
        } else {
            return Optional.empty();
        }
        return Optional.of(new Instance(name, addresses.get(0), port, attributes, ttl));
    }
}
