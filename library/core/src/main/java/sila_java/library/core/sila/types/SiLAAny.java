package sila_java.library.core.sila.types;

import com.google.protobuf.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.utils.XMLMarshaller;

import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAAny {
    /**
     * Generates a proto message according to the received xml datatype and the payload from the any type
     * @param any the any type
     * @return The proto message
     */
    public static DynamicMessage toMessage(SiLAFramework.Any any)
            throws MalformedSiLAFeature, IOException {
        final DataTypeType dataTypeType = XMLMarshaller.convertFromXML(DataTypeType.class, any.getType());
        final Descriptors.Descriptor descriptor = ProtoMapper.dataTypeToDescriptor(dataTypeType);
        return DynamicMessage.parseFrom(descriptor, any.getPayload());
    }

    public static SiLAFramework.Any from(final String xml, final Message silaTypeValue) {
        return SiLAFramework.Any
                .newBuilder()
                .setType(xml)
                .setPayload(silaTypeValue.toByteString())
                .build();
    }

    public static SiLAFramework.Any from(final DataTypeType dataTypeType, final Message silaTypeValue) {
        return from(XMLMarshaller.convertToXML(dataTypeType), silaTypeValue);
    }
}
