package sila_java.library.core.discovery;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.core.discovery.networking.dns.Question;
import sila_java.library.core.discovery.networking.service_discovery.Instance;
import sila_java.library.core.discovery.networking.service_discovery.InvalidInstanceNameException;
import sila_java.library.core.discovery.networking.service_discovery.ResponseListener;
import sila_java.library.core.discovery.networking.service_discovery.SenderReceiver;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class SiLADiscovery implements AutoCloseable, ResponseListener.InstanceListener {
    static final String SILA_MDNS_DOMAIN = "local.";
    static final String SILA_MDNS_TYPE = "_sila._tcp.";
    private static final int QUERY_RATE = 1000; // [ms]
    private static final int DEFAULT_ITERATIONS = 3;
    private final static Pattern GUID_PATTERN = Pattern.compile(
            "([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE
    );
    private final Set<UUID> uuidCache = ConcurrentHashMap.newKeySet();
    private final Set<ServerListener> serverListeners = ConcurrentHashMap.newKeySet();
    private final SenderReceiver senderReceiver;
    private final Question queryQuestion;

    /**
     * Discovery Class to get DNS-SD records via mDNS
     */
    public SiLADiscovery() {
        final String dnsQueryName = (SILA_MDNS_TYPE + SILA_MDNS_DOMAIN).toLowerCase();
        this.queryQuestion = new Question(dnsQueryName, Question.QType.PTR, Question.QClass.IN);
        this.senderReceiver = new SenderReceiver(new ResponseListener(this, SILA_MDNS_TYPE));
        this.senderReceiver.enable();
    }

    @Override
    public void close() {
        this.senderReceiver.close();
        this.serverListeners.clear();
    }

    @Override
    public void instanceAdded(Instance instance) {
        log.debug("Trying to add instance: {}", instance.getName());

        final String instanceName = instance.getName();
        if (instance.getTtl() != 0) {
            log.debug("Instance added: {} -> {}", instanceName, instance.toString());
            UUID serverUUID;
            try {
                serverUUID = parseUUID(instanceName);
            } catch (final InvalidInstanceNameException e) {
                log.warn("Failed to add instance because: {}", e.getMessage());
                return;
            }

            // Only add new UUIDs
            if (uuidCache.contains(serverUUID)) {
                return;
            }
            log.info(
                    "[serverAdded] Found Service Instance {} on {}:{}",
                    serverUUID,
                    instance.getHostAddress(),
                    instance.getPort()
            );
            uuidCache.add(serverUUID);
            serverListeners.forEach(listener ->
                    listener.serverAdded(serverUUID, instance.getHostAddress(), instance.getPort())
            );
        }
    }

    /**
     * Refresh the discovery by cleaning all the elements from the cache
     */
    public void clearCache() {
        uuidCache.clear();
    }

    /**
     * Scan the network for several iterations
     *
     * @implNote Clears the cache so it re-adds server previously found for all listeners
     */
    public void scanNetwork() {
        scanNetwork(DEFAULT_ITERATIONS);
    }

    /**
     * Scan the network for x iterations
     * The number of iteration must be greater than or equal to 1
     * This call is blocking for: (iteration * (iteration + 1) / 2) * QUERY_RATE milliseconds
     * @param iteration The number of scan iteration to perform
     */
    public synchronized void scanNetwork(final int iteration) {
        clearCache();
        log.debug("Scanning network...");
        int timeToWaitMs = QUERY_RATE;
        for (int i = 0; i < iteration; ++i) {
            try {
                this.query();
                Thread.sleep(timeToWaitMs);
                timeToWaitMs *= 2;
            } catch (final InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        log.debug("End of network scan.");
    }

    /**
     * Add Listener
     */
    public void addListener(@NonNull final ServerListener serverListener) {
        this.serverListeners.add(serverListener);
    }

    /**
     * Remove Listener
     */
    public void removeListener(@NonNull final ServerListener serverListener) {
        this.serverListeners.remove(serverListener);
    }

    /**
     * Issue a query on all specified Network Interfaces
     */
    private void query() {
        this.senderReceiver.send(queryQuestion);
    }

    /**
     * Parse UUID from the service instance name
     *
     * @param instanceName instance name of discovered service
     * @return unique identifier of that service instance
     */
    private static UUID parseUUID(@NonNull final String instanceName) throws InvalidInstanceNameException {
        final Matcher matcher = GUID_PATTERN.matcher(instanceName);
        if (!matcher.matches()) {
            throw new InvalidInstanceNameException(
                    "The service instance: " + instanceName + " does not follow the pattern of an UUID."
            );
        }
        return UUID.fromString(matcher.group(1));
    }
}
