package sila_java.library.core.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Utility Functions for Files including Resources and Configurations
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class FileUtils {
    /**
     * Get File content converted to a String using the default Charset without BOM
     *
     * @param absolutePath Absolute Path on the File System
     */
    public static String getFileContent(@NonNull final String absolutePath) throws IOException {
        return getFileContent(Files.newInputStream(Paths.get(absolutePath)));
    }

    /**
     * Get File content converted to a String using the default Charset without BOM
     *
     * @param fileStream File as Input Stream
     */
    public static String getFileContent(@NonNull final InputStream fileStream) throws IOException {
        return IOUtils.toString(new BOMInputStream(fileStream), Charset.defaultCharset());
    }

    /**
     * Get resource content converted to a String using the default Charset without BOM
     * @param resourcePath Absolute path in a designated resource directory
     */
    public static String getResourceContent(@NonNull final String resourcePath) throws IOException {
        return getFileContent(Thread
                .currentThread()
                .getContextClassLoader()
                .getResourceAsStream(resourcePath)
        );
    }

    /**
     * Copy Resource into a specified File Object
     * @param resourcePath Absolute path in a designated resource directory
     * @param file File will be overwritten with
     */
    public static void copyResourceFile(
            @NonNull String resourcePath,
            @NonNull File file) throws IOException{
        final URL resourceURL = Thread.currentThread().getContextClassLoader().getResource(resourcePath);
        if (resourceURL == null) {
            throw new IllegalStateException(resourcePath + " could not be found with Class loader");
        }
        org.apache.commons.io.FileUtils.copyURLToFile(resourceURL, file);
    }

    /**
     * Get Configuration File in current Class Context
     *
     * @param fileName Name of properties file
     * @return Configurations class
     */
    public static Configuration getLocalConfiguration(String fileName) {
        return getConfiguration(Thread.currentThread().getContextClassLoader(), fileName);
    }

    /**
     * Get Configuration File with given Class Loader
     *
     * @param classLoader Loader that can access the property file
     * @param fileName Name of properties file
     * @return Configurations class
     */
    public static Configuration getConfiguration(ClassLoader classLoader, String fileName) {
        final URL fileURL = classLoader.getResource(fileName);
        if (fileURL == null) {
            final String errMsg = fileName + " couldn't be found!";
            throw new IllegalStateException(errMsg);
        }

        try {
            return new Configurations().properties(fileURL);
        } catch (ConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }
}
