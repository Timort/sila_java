package sila_java.library.core.sila.clients;

import io.netty.handler.ssl.SslContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.SSLException;

import static sila_java.library.core.encryption.EncryptionUtils.buildTLSContextNoCert;

/**
 * SSL Context Factory utility
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SSLContextFactory {
    private static SslContext sslContext = null;

    /**
     * Get an existing instance or create one
     * @return a SSL Context
     */
    public static SslContext getInstance() {
        synchronized (SSLContextFactory.class) {
            if (sslContext == null) {
                sslContext = getNewInstance();
            }
        }
        return sslContext;
    }

    /**
     * Create a new SSL Context
     * @return a SSL Context
     */
    public static SslContext getNewInstance() {
        try {
            return buildTLSContextNoCert();
        } catch (SSLException e) {
            log.error("Unable to build TLS Context", e);
            throw new RuntimeException(e);
        }
    }
}
