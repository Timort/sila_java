package sila_java.library.core.discovery.networking.dns.records;

import lombok.NonNull;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The text record can hold arbitrary non-formatted text string.
 */
public class TxtRecord extends Record {
    private final Map<String, String> attributes;

    public TxtRecord(@NonNull final ByteBuffer buffer, @NonNull final String name, final long ttl, final int length) {
        super(name, ttl);
        final List<String> strings = readStringsFromBuffer(buffer, length);
        attributes = parseDataStrings(strings);
    }

    private Map<String, String> parseDataStrings(@NonNull final List<String> strings) {
        final Map<String, String> pairs = new HashMap<>();
        strings.forEach(s -> {
            final String[] parts = s.split("=");
            if (parts.length > 1) {
                pairs.put(parts[0], parts[1]);
            } else {
                pairs.put(parts[0], "");
            }
        });
        return pairs;
    }

    public Map<String, String> getAttributes() {
        return Collections.unmodifiableMap(attributes);
    }

    @Override
    public String toString() {
        return "TxtRecord{" +
                "name='" + name + '\'' +
                ", ttl=" + ttl +
                ", attributes=" + attributes +
                '}';
    }
}
