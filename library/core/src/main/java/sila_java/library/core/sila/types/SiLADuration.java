package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.Duration;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLADuration {
    public static SiLAFramework.Duration from(final Duration duration) {
        return SiLAFramework.Duration
                .newBuilder()
                .setNanos(duration.getNano())
                .setSeconds(duration.getSeconds())
                .build();
    }
}
