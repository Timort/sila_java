package sila_java.library.core.sila.types;

import com.google.protobuf.ByteString;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLABinary {
    public static SiLAFramework.Binary fromBytes(final byte[] bytes) {
        return SiLABinary.fromBytes(ByteString.copyFrom(bytes));
    }

    public static SiLAFramework.Binary fromBytes(final ByteString byteString) {
        return SiLAFramework.Binary.newBuilder().setValue(byteString).build();
    }

    public static SiLAFramework.Binary fromBinaryTransferUUID(final String binaryTransferUUID) {
        return SiLAFramework.Binary.newBuilder().setBinaryTransferUUID(binaryTransferUUID).build();
    }

    public static SiLAFramework.Binary fromBinaryTransferUUID(final UUID binaryTransferUUID) {
        return SiLAFramework.Binary.newBuilder().setBinaryTransferUUID(binaryTransferUUID.toString()).build();
    }
}
