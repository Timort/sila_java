package sila_java.integration_test;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila_java.integration_test.utils.ServerInfo;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;
import sila_java.examples.test_server.TestServer;
import sila_java.integration_test.utils.TestServerHelper;
import sila_java.examples.test_server.impl.UnobservableCommand;
import sila_java.examples.test_server.impl.UnobservableProperty;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static sila_java.integration_test.utils.ScanUtils.scanAndWaitServerState;
import static sila_java.integration_test.utils.ScanUtils.waitServerState;

/**
 * Call Executor Test
 */
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CallExecutorTest {
    private static final int TIMEOUT = 60;
    private static final ServerInfo SERVER = new ServerInfo(Paths.get("./tmp_server3.cfg"), 50060);
    private TestServer testServer;
    private UUID testServerUUID;

    @BeforeAll
    void beforeAll() throws IOException {
        log.info("Setting up test server");
        testServer = TestServerHelper.build(SERVER.getPort(), SERVER.getConfigFile().getPath(), "local");
        testServerUUID = SERVER.getUUID(TIMEOUT);
        scanAndWaitServerState(testServerUUID, Server.Status.ONLINE, TIMEOUT);
        log.info("Set up test server");
    }

    @AfterAll
    void cleanup() throws TimeoutException, ExecutionException {
        log.info("Closing and cleaning up");
        testServer.close();
        waitServerState(testServerUUID, Server.Status.OFFLINE, TIMEOUT);
        ServerManager.getInstance().close();
    }

    @Test
    void testUnobservableCommand() throws InvalidProtocolBufferException {
        final SiLACall siLACall = new SiLACall(
                testServerUUID,
                "UnobservableCommandTest",
                "MakeCoffee",
                SiLACall.Type.UNOBSERVABLE_COMMAND
        );
        final String result = ServerManager.getInstance().newCallExecutor(siLACall).execute();
        final UnobservableCommandTestOuterClass.MakeCoffee_Responses.Builder builder = UnobservableCommandTestOuterClass.MakeCoffee_Responses.newBuilder();
        JsonFormat.parser().merge(result, builder);
        final UnobservableCommandTestOuterClass.MakeCoffee_Responses response = builder.build();

        log.info("Unobservable command call result {}", response.toString());
        Assertions.assertEquals(UnobservableCommand.MAKE_COFFEE_RESPONSE, response);
    }

    @Test
    void testUnobservableProperty() throws InvalidProtocolBufferException {
        final SiLACall siLACall = new SiLACall(
                testServerUUID,
                "UnobservablePropertyTest",
                "ListString",
                SiLACall.Type.UNOBSERVABLE_PROPERTY
        );
        final String result = ServerManager.getInstance().newCallExecutor(siLACall).execute();
        final UnobservablePropertyTestOuterClass.Get_ListString_Responses.Builder builder = UnobservablePropertyTestOuterClass.Get_ListString_Responses.newBuilder();
        JsonFormat.parser().merge(result, builder);
        final UnobservablePropertyTestOuterClass.Get_ListString_Responses response = builder.build();
        log.info("Unobservable property call result {}", response.toString());
        Assertions.assertEquals(UnobservableProperty.LIST_STRING_RESPONSE, response);
    }

    @Test
    void testObservableCommand() throws InvalidProtocolBufferException {
        final SiLACall siLACall = new SiLACall(
                testServerUUID,
                "ObservableCommandTest",
                "RestartDevice",
                SiLACall.Type.OBSERVABLE_COMMAND
        );
        final String result = ServerManager.getInstance().newCallExecutor(siLACall).execute();
        final ObservableCommandTestOuterClass.RestartDevice_Responses.Builder builder = ObservableCommandTestOuterClass.RestartDevice_Responses.newBuilder();
        JsonFormat.parser().merge(result, builder);
        final ObservableCommandTestOuterClass.RestartDevice_Responses response = builder.build();
        log.info("Observable command call result {}", response.toString());
    }

    @Test
    void testObservableProperty() throws InvalidProtocolBufferException {
        final SiLACall siLACall = new SiLACall(
                testServerUUID,
                "ObservablePropertyTest",
                "ListString",
                SiLACall.Type.OBSERVABLE_PROPERTY
        );
        final String result = ServerManager.getInstance().newCallExecutor(siLACall).execute();
        final ObservablePropertyTestOuterClass.Subscribe_ListString_Responses.Builder builder = ObservablePropertyTestOuterClass.Subscribe_ListString_Responses.newBuilder();
        JsonFormat.parser().merge(result, builder);
        final ObservablePropertyTestOuterClass.Subscribe_ListString_Responses response = builder.build();
        log.info("Observable property call result {}", response.toString());
        Assertions.assertEquals(4, response.getListStringList().size());
        for (int i = 0; i < response.getListStringList().size(); ++i) {
            Assertions.assertEquals("Element " + i, response.getListStringList().get(i).getValue());
        }
    }
}
